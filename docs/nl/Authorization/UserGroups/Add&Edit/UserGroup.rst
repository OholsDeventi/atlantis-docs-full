.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Gebruikersgroep: **\ De gebruikersgroep is een groep mensen met
dezelfde interesses en doelen betreffende het systeem. Zij hebben
allemaal dezelfde rechten. De lijst met mensen binnen de groep is te
zien in het meest linker deelvenster.

 

.. raw:: html

   </div>

·         \ **Samenvouwen deelvenster-knop:**\  Klik op de **linker
pijl-**\ knop om het deelvenster samen te vouwen. Zodra het deelvenster
is samengevouwen, zal diezelfde pijl van richting veranderen naar
rechts. Klik op de **rechterpijl** om het deelvenster weer uit te
vouwen.

 

·         \ **“weergeef-knop”: **\ De weergeef-knop is het **oog**
pictogram in het deelvenster van de gebruikersgroep. Met het pictogram
worden alle geregistreerde gebruikers in het systeem weergegeven. Door
te klikken op de gebruikersgroep, worden alle gebruikers die zijn
toegewezen aan de gebruikersnaam-groepsnaam, vermeld.

 

·         \ **Toevoeg knop: **\ Click op de **knop +** om
gebruikersgroep documenten toe te voegen.

 

·         \ **Zoekknop: **\ de gebruiker kan voor een gebruikersgroep
zoeken in de zoekruimte. ** **

****

 

·         \ **Verwijder knop: **\ de **prullenbak** pictogram is de
verwijderknop. Door te klikken op het prullenbakpictogram, wordt het
bevestigingsvenster weergegeven.

§  \ **Ja**\ : door te klikken op de **JA**-knop, zal de verwijderactie
worden uitgevoerd.

§  \ **Nee**\ : door te klikken op de **NEE-knop,** zal de
verwijderactie worden beëindigd. 

 

·         \ **Bewerkknop: **\ Klik op de potlood-pictogram om
gebruikersgroep documenten te bewerken.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:28.35pt;margin-right:0in">

·         \ **R-pictogram: De**\  R-pictogram staat voor de **rechten**
knop.

.. raw:: html

   </div>

 

Hoe kan ik een gebruikersgroep toevoegen?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**\ 

.. raw:: html

   </div>

·         Klik op de **+** pictogram om toevoegacties uit te voeren.

·         \ **Naam:**\  Geef de naam van de gebruikersgroep in het
tekstveld.

·         \ **Betekenis:**\  Beschrijf het doel van de gebruikersgroep
in het tekstveld. 

·         \ **Nieuwe gebruiker:**\  Het keuzevakje informeert of nieuw
geregistreerde gebruikers standaard aan deze groep worden toegewezen. 

·         \ **Herstel actie**\ 

§  Door te klikken op de **Reset**-knop, wordt het bevestigingsvenster
weergegeven. 

§  \ **Ja**\ : Door te klikken op de **Ja-**\ knop, keert u terug naar
de laatst opgeslagen positie.

§  \ **Annuleren**\ : Door te klikken op de **annuleer**\ knop, wordt de
betreffende actie beëindigd.

 

·         \ **Opslaan**\ 

§  Door te klikken op de **opslaan-knop**, worden de gemaakte
veranderingen opgeslagen.

 

·         \ **Afsluit-knop: **\ Door te klikken op het
**afsluitpictogram**, wordt het bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op **Ja**, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleer: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>

 

 

 

 

 

 

.. raw:: html

   </div>
