.. raw:: html

   <div class="WordSection1">

Waar staat de R-pictogram voor?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

De **R-pictogram** staat voor de verschillende **rechten** van een
gebruiker of een gebruikersgroep. Dit toont de verschillende rechten die
beschikbaar zijn voor de gebruikersgroep. De gebruiker kan rechten voor
gebruikers of groepen selecteren of deselecteren.

 

 

.. raw:: html

   </div>

·         \ **Zoek: **\ De gebruiker kan ook voor specifieke rechten
zoeken in de zoekbalk. ****

****

 

·         \ **Afsluit-knop: **\ Door te klikken op de
**afsluit-**\ pictogram, verschijnt het bevestigingsvenster. ****

§  \ **Ja: **\ Door te klikken op de ja-knop, wordt de pagina
afgesloten.

§  \ **Annuleer: **\ door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

 

·         \ **Herstelactie:**\ 

Door de klikken op de **herstel-knop**, wordt het bevestigingsvenster
weergegeven. 

§  \ **Ja**\ : Door te klikken op de ja-knop, keert u terug naar de
laatst opgeslagen positie.

§  \ **Annuleer**\ : Door op de annuleerknop te drukken, wordt de
betreffende actie beëindigd.

 

·         \ **Opslaan:**\ 

§  Door te klikken op de **opslaan-**\ knop, worden de gemaakte
veranderingen opgeslagen. ****

****

 

·         \ **Afsluit-knop: **\ Door te klikken op de afsluit-pictogram,
wordt er een bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op de ja-knop, wordt de pagina
afgesloten. ****

§  \ **Annuleer: **\ Door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

****

 

 

.. raw:: html

   </div>
