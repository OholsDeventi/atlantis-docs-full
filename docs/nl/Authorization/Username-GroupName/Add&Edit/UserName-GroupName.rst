.. raw:: html

   <div class="WordSection1">

` <>`__\ ` <>`__

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Gebruikersnaam – groepsnaam: **\ De lijst met de kop “gebruikersnaam –
groepsnaam” geeft de lijst met individuele gebruikers samen met de
groepsnaam weer, als zij aan een groep zijn toegevoegd.

.. raw:: html

   </div>

 

·         \ **Minimaliseer deelvenster-knop:** Klik op de **linker
pijl-knop** om het deelvenster samen te vouwen. Zodra het deelvenster
samengevouwen is, zal dezelfde pijl naar rechts draaien. Klik op de
**rechterpijl** om het venster weer uit te breiden. 

 

·         \ **Toevoeg-knop: **Klik op het **+ icoon** om gebruikersgroep
bestanden toe te voegen.

 

 

·         \ **Zoekknop: **Een gebruiker kan voor een gebruikersnaam
zoeken in de zoekruimte.

 

·         \ **Verwijder-knop: **de **prullenbak-pictogram** is de
verwijder-knop. Door te klikken op de verwijderknop, wordt het
bevestigingsvenster weergegeven. 

§  \ **Ja**: Door te klikken op de **ja-knop,** wordt de verwijder actie
uitgevoerd.

§  \ **Nee**: Door te klikken op de **nee-knop,** wordt de
verwijderactie beëindigd.

**Bewerk-knop:**\  Klik op het **potlood-icoon** om de gebruikersnaam te
bewerken.

 

Hoe kan ik individuele gebruikers toevoegen en deze met verschillende
rechten te machtigen?

`**** <>`__

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

 

.. raw:: html

   </div>

-  De gebruiker kan op de **toevoeg+**\ ** **\ pictogram klikken om
   nieuwe gebruikers aan het systeem toe te voegen.

-  **Gebruikersnaam\*:**\  Dit veld vereist een duidelijk naam van de
   gebruiker om verwarring te voorkomen. Het veld is uniek en verplicht.

-  **Titel:**\  In dit veld kan de gebruiker een groetwoord typen.

-  **Naam:**\  Dit veld vereist de achternaam van de gebruiker.

-  **Voornaam:**\  Dit veld vereist de voornaam van de gebruiker.

-  **Adres:**\  Dit veld vereist het gehele adres van de gebruiker.

-  **Land:**\  Voeg de landnaam van het adres in het tekstveld in.

-  **Wachtwoord\*:**\  Voeg hier het wachtwoord van het
   gebruikersaccount toe. Dit veld is uniek en verplicht.

-  **Wachtwoord bevestigen:**\  Vul hier nogmaals het bovengenoemde
   wachtwoord in.

-  **Postcode:**\  Vul hier de postcode van de gebruiker in, die
   gekoppeld is aan boven vermelde adres.

-  **Woonplaats:**\  Voeg hier de woonplaats van de gebruiker in.

-  **Telefoonnummer:**\  Vul hier het gewenste nummer van de gebruiker
   in.

-  **Emailadres\*:**\  Vul hier het gewenste emailadres van de gebruiker
   in. Het emailadres moet voldoen aan abc@xyz formaat. Dit tekstveld is
   verplicht. 

`KEUZE VAKJES: <>`__\ ****

-  **Actief:**\  De gebruiker is actief of niet.

-  **In het account: **\ Gebruiker kan een *kopie* bestellen en ontvangt
   een factuur in plaats van een directe betaling.

-  **Niet geregistreerd:**\  Voor de gebruikersaccounts van degenen die
   toegang hebben tot het systeem zonder login. ****

-  **Versturen van de login gegevens:**\  Deel de logingegevens met de
   betreffende gebruiker, ingesteld door de beheerder, via het
   verstrekte emailadres.

om segmenten en gebruikersgroepen toe te voegen, klik op de +
toevoegknop.

-  **Segmenten:**\  Selecteer uit de keuzelijst het gewenste segment
   voor de gebruiker, wat de reikwijdte van de toegang van de gebruiker
   afbakent. De gebruiker kan meerdere segmenten selecteren, welke al
   een andere chip zullen worden weergegeven.

·         \ **Gebruikersgroep: **Selecteer de gebruikersgroep al een
gebruiker in een groep moet worden toegevoegd. De gebruiker kan maar en
groep van de keuzelijst selecteren. **Opslaan**  \ ****

§  Door te klikken op de **opslaan-knop**, worden alle gemaakte
wijzigingen opgeslagen. ****

§  Zodra het bestand is opgeslagen, wordt het **prullenbak-icoontje**
weergegeven.  \ ****

****

 

·         \ **Verwijderknop: **De **vuilnisbak**-pictogram is de
verwijder-knop. Door te kikken op de vuilnisbak-pictogram zal het
toegevoegde segment verwijderd worden.

·         \ **Afsluitknop**: Door te klikken op de **afsluit-**\ knop,
wordt ere en bevestigingsvenster weergegeven. ****

§  \ **Ja: **Door te klikken op de ja-knop, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.75in;margin-right:
   0in">

§  \ **Annuleer: **Door te klikken op de annuleerknop, blijft u op
dezelfde pagina. \ ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
