.. raw:: html

   <div class="WordSection1">

**Machtiging **

Op het Atlantis Dashboard, is de miniatuur “Autorisatie” zichtbaar in de
kleur blauw.
Een systeembeheerder stelt gebruikers in staat om toegang te krijgen tot
het systeem en biedt hen de juiste rechten.

Dit scherm toont twee zijden: 1). Gebruikersgroepen en 2). Gebruikers,
respectievelijk. Autorisatie bestaat uit het toekennen van vooraf
gedefinieerde gebruikersrechten aan gebruikersgroepen. Door gebruikers
toe te kennen aan gebruikersgroepen, krijgen de gebruikers de
respectieve gebruikersrechten.

 

**Gereedschaps-tip: **\ De gebruiker kan voor meer informatie met de
muis over de knoppen zweven. Hierdoor verschijnt de naam van de knop.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Gebruikersgroep: **\ de gebruikersgroep is een groep mensen die
soortgelijke interesses en doelen hebben betreffende het systeem. Zij
hebben dezelfde rechten. De lijst van gebruikersgroepen wordt in het
linker deelvenster weergegeven.

 

.. raw:: html

   </div>

·         \ `**Pinvast-knop:
** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\ ** **\ zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s te vast
te pinnen. De vastgepinde pagina’s worden opgeslagen in lijsten, die
onder de kop van “Atlantis Dashboard” worden weergegeven.

 

·         \ **Samenvouw-knop: **\ met deze knop kan de gebruiker het
venster in elkaar vouwen. Om het paneel in elkaar te vouwen, klikt u op
de linker pijlknop. Zodra het paneel in elkaar gevouwen is, verandert
dezelfde pijlknop van richting, d.w.z., de pijl draait naar rechts. Klik
op de rechterpijl om de het deelvenster weer uit te vouwen.

-  **Voorbeeld knop:**\  de voorbeeldknop is het **oog** pictogram in
   het venster van de gebruikersgroep. Met de oog pictogram worden alle
   geregistreerde gebruikers in het systeem weergegeven. Als u op een
   gebruikersgroep klikt, worden alle gebruikers die aan de
   gebruikersnaam-groepsnaam zijn toegewezen, vermeld.

 

·         \ **Toevoeg knop: **\ Klik op de **knop +** om gebruikersgroep
records toe te voegen.

 

·         \ **Zoekknop: **\ de gebruikers kan voor een gebruikersgroep
zoeken in de zoekruimte. ** **

****

 

****

 

·         \ **Verwijder knop: **\ de **prullenbak** pictogram is de
verwijderknop. Door te klikken op het prullenbakpictogram, wordt het
bevestigingsvenster weergegeven.

§  \ **Ja**\ : door te klikken op de **JA**-knop, zal de verwijderactie
worden uitgevoerd.

§  \ **Nee**\ : door te klikken op de **NEE-knop,** zal de
verwijderactie worden beëindigd.  

 

·         \ **Bewerkknop: **\ Klik op het potlood-pictogram om de
gebruikersgroep documenten te bewerken.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:28.35pt;margin-right:0in">

·         \ **R-pictogram: De**\  R-pictogram staat voor de **rechten**
knop.

.. raw:: html

   </div>

 

 

Hoe kan ik een gebruikersgroep toevoegen?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**\ 

.. raw:: html

   </div>

·         Klik op de **+** pictogram om toevoegacties uit te voeren.

·         \ **Naam:**\  Geef de naam van de gebruikersgroep in het
tekstveld.

·         \ **Betekenis:**\  Beschrijf het doel van de gebruikersgroep
in het tekstveld.  

·         \ **Nieuwe gebruiker:**\  Het keuzevakje informeert of nieuw
geregistreerde gebruikers standaard aan deze groep worden toegewezen.  

·         \ **Reset**\ 

§  Door te klikken op de **Reset**-knop, wordt het bevestigingsvenster
weergegeven.  

§  \ **Ja**\ : Door te klikken op de **Ja-**\ knop, keert u terug naar
de laatst opgeslagen positie.

§  \ **Annuleren**\ : Door te klikken op de **annuleer**\ knop, wordt de
betreffende actie beëindigd.

 

·         \ **Opslaan**\ 

§  Door te klikken op de **opslaan-knop**, worden de gemaakte
veranderingen opgeslagen.

 

·         \ **Afsluit-knop: **\ Door te klikken op het
**afsluitpictogram**, wordt het bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op **Ja**, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleer: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>

 

 

` <>`__

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Gebruikersnaam – groepsnaam: **\ De lijst met de kop “gebruikersnaam –
groepsnaam” geeft de lijst met individuele gebruikers samen met de
groepsnaam weer, als zij aan een groep zijn toegevoegd.

.. raw:: html

   </div>

 

·         \ **Samenvouwen deelvenster-knop:** Klik op de **linker
pijl-knop** om het deelvenster samen te vouwen. Zodra het deelvenster
samengevouwen is, zal dezelfde pijl naar rechts draaien. Klik op de
**rechterpijl** om het venster weer uit te breiden.  

 

·         \ **Toevoeg-knop: **Klik op het **+ icoon** om gebruikersgroep
bestanden toe te voegen.

 

 

·         \ **Zoekknop: **Een gebruiker kan voor een gebruikersnaam
zoeken in de zoekruimte.

 

·         \ **Verwijder-knop: **de **prullenbak-pictogram** is de
verwijder-knop. Door te klikken op de verwijderknop, wordt het
bevestigingsvenster weergegeven.  

§  \ **Ja**: Door te klikken op de **ja-knop,** wordt de verwijder actie
uitgevoerd.

§  \ **Nee**: Door te klikken op de **nee-knop,** wordt de
verwijderactie beëindigd.

**Bewerk-knop:**\  Klik op het **potlood-icoon** om de gebruikersnaam te
bewerken.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**\ 

.. raw:: html

   </div>

·         Klik op het + icoon om aanvullende acties uit te voeren.

·         \ **Naam:**\  Geef de naam van de gebruikersgroep in het
tekstveld.

·         \ **Definitie:**\  Beschrijf het doel van de gebruikersgroep
in het tekstveld.

·         \ **Nieuwe gebruiker:**\  Het selectievakje informeert of
nieuw geregistreerde gebruikers automatisch aan de groep worden
toegewezen.

·         \ **Herstel actie**\ 

§  Door te klikken op de **Reset**-knop, wordt het bevestigingsvenster
weergegeven

§  \ **Ja**\ : Door te klikken op de **Ja-**\ knop, keert u terug naar
de laatst opgeslagen positie

§  \ **Annuleren**\ : Door op de annuleerknop te drukken, wordt de
betreffende actie beëindigd.  

 

·         \ **Opslaan actie**\ 

·         Deze knop slaat alle nieuwe of niet opgeslagen veranderingen
op.

 

·         \ **Afsluit-knop: **\ Door te klikken op de **afsluit-icoon,**
verschijnt het bevestigingsvenster. ** **

§  \ **Ja: **\ Door te klikken op de Ja-knop, wordt de pagina
afgesloten. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

.. raw:: html

   </div>

 

`Waar staat de R-pictogram voor?  <>`__

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**R-pictogram: **\ de R-pictogram staat voor de verschillende
**rechten** van een gebruiker of een groep gebruikers. Dit toont de
verschillende rechten die beschikbaar zijn voor de groep gebruikers. De
gebruiker kan rechten voor gebruikers of groepen selecteren of
deselecteren.

 

 

.. raw:: html

   </div>

·         \ **Zoek: **De gebruiker kan ook voor specifieke rechten
zoeken in de zoekbalk. ****

****

 

·         \ **Afsluit-knop: **Door te klikken op de
**afsluit-**\ pictogram, verschijnt het bevestigingsvenster. ****

§  \ **Ja: **Door te klikken op de ja-knop, wordt de pagina afgesloten.

§  \ **Annuleer: **door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

 

·         \ **Herstelactie:**

Door de klikken op de herstel-knop, wordt het bevestigingsvenster
weergegeven. 

§  \ **Ja**: Door te klikken op de ja-knop, keert u terug naar de laatst
opgeslagen positie.

§  \ **Annuleer**: Door op de annuleerknop te drukken, wordt de
betreffende actie beëindigd.

·         \ **Opslaan:**

§  Door te klikken op de **opslaan-**\ knop, worden de gemaakte
veranderingen opgeslagen. ****

****

 

·         \ **Afsluit-knop: **Door te klikken op de afsluit-pictogram,
wordt er een bevestigingsvenster weergegeven. ****

§  \ **Ja: **Door te klikken op de ja-knop, wordt de pagina afgesloten.
****

§  \ **Annuleer: **Door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

§ 

 

`Hoe kan ik individuele gebruikers toevoegen en deze met verschillende
rechten te machtigen?
 <>`__\ 

`**** <>`__

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

 

.. raw:: html

   </div>

-  De gebruiker kan op de **toevoeg+**\ ** **\ pictogram klikken om
   nieuwe gebruikers aan het systeem toe te voegen.

-  **Gebruikersnaam\*:**\  Dit veld vereist een duidelijk naam van de
   gebruiker om verwarring te voorkomen. Het veld is uniek en verplicht.

-  **Titel:**\  Hier kan de gebruiker een groetwoord invoeren.

-  **Naam:**\  Dit veld vereist de achternaam van de gebruiker.

-  **Voornaam:**\  Dit veld vereist de voornaam van de gebruiker.

-  **Adres:**\  Dit veld vereist het gehele adres van de gebruiker.

-  **Land:**\  Voer de landnaam van het adres in het tekstveld in.

-  **Wachtwoord\*:**\  Voeg hier het wachtwoord van het
   gebruikersaccount toe. Dit veld is uniek en verplicht.

-  **Wachtwoord bevestigen:**\  Vul hier nogmaals het bovengenoemde
   wachtwoord in.

-  **Postcode:**\  Vul hier de postcode van de gebruiker in, die
   gekoppeld is aan boven vermelde adres.

-  **Woonplaats:**\  Voeg hier de woonplaats van de gebruiker in.

-  **Telefoonnummer:**\  Vul hier het gewenste nummer van de gebruiker
   in.

-  **Emailadres\*:**\  Vul hier het gewenste emailadres van de gebruiker
   in. Het emailadres moet voldoen aan abc@xyz formaat. Dit tekstveld is
   verplicht.  

`KEUZE VAKJES: <>`__\ ****

-  **Actief:**\  De gebruiker is actief of niet.

-  **In het account: **\ Gebruiker kan een *kopie* bestellen en ontvangt
   een factuur in plaats van een directe betaling.

-  **Niet geregistreerd:**\  Voor de gebruikersaccounts van degenen die
   toegang hebben tot het systeem zonder login. ****

-  **Versturen van de login gegevens:**\  Deel de logingegevens met de
   betreffende gebruiker, ingesteld door de beheerder, via het
   verstrekte emailadres.

om segmenten en gebruikersgroepen toe te voegen, klik op de +
toevoegknop.

-  **Segmenten:**\  Selecteer uit de keuzelijst het gewenste segment
   voor de gebruiker, wat de reikwijdte van de toegang van de gebruiker
   afbakent. De gebruiker kan meerdere segmenten selecteren, welke al
   een andere chip zullen worden weergegeven.

·         \ **Gebruikersgroep: **Selecteer de gebruikersgroep al een
gebruiker in een groep moet worden toegevoegd. De gebruiker kan maar en
groep van de keuzelijst selecteren. **Opslaan**  \ ****

§  Door te klikken op de **opslaan-knop**, worden alle gemaakte
wijzigingen opgeslagen. ****

§  Zodra het bestand is opgeslagen, wordt het **prullenbak-icoontje**
weergegeven.  \ ****

****

 

·         \ **Verwijderknop: **De **vuilnisbak**-pictogram is de
verwijder-knop. Door te kikken op de vuilnisbak-pictogram zal het
toegevoegde segment verwijderd worden.

·         \ **Afsluitknop**: Door te klikken op de **afsluit-**\ knop,
wordt ere en bevestigingsvenster weergegeven. ****

§  \ **Ja: **Door te klikken op de ja-knop, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.75in;margin-right:
   0in">

§  \ **Annuleer: **Door te klikken op de annuleerknop, blijft u op
dezelfde pagina. \ ****

.. raw:: html

   </div>

****

 

**Global Herstellen en opslaan **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Herstellen **\ 

.. raw:: html

   </div>

§  Door te klikken op de **herstel-**\ knop, wordt er een
bevestigingsvenster weergegeven.

§  \ **Ja**\ : Door te klikken op de ja-knop, keert u terug naar de
laatst opgeslagen positie.

§  \ **Annuleer**\ : Door op de annuleerknop te drukken, wordt de
betreffende actie beëindigd.

 

·         \ **Opslaan **

§  Door te klikken op de **opslaan-**\ knop, worden de gemaakte
veranderingen opgeslagen. ****

**Afsluit-knop  **

§  Door te klikken op het **annuleer-**\ pictogram, verschijnt ere en
bevestigingsvenster. ****

§  \ **Ja: **\ Door te klikken op de ja kop, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op de annuleerknop, blijft u op
dezelfde pagina.

.. raw:: html

   </div>

.. raw:: html

   </div>
