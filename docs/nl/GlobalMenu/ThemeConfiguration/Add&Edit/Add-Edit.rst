.. raw:: html

   <div class="WordSection1">

****

 

**Hoe kan ik thema’s toevoegen of bewerken? **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen (+) of bewerken (Potlood)**

.. raw:: html

   </div>

•        \ **Naam:**\  Voer hier de naam van het thema in

•        \ **Logo:**\  Door te klikken op dit veld, kan de gebruiker een
pictogram toevoegen van de lokale computer.

 

 

**Kleur**

•        \ **Primaire kleur:**\  Een selectie in dit veld verandert de
kleur van de kop en de knoppen.

•        \ **Secundaire kleur:**\  Een selectie in dit veld veranderd de
kleur van de broodkruimels / breadcrumbs.

•        \ **Tertiaire kleur:**\  Een selectie in dit veld verandert de
kleur van de kolommen.

•        \ **Pictogramkleur:**\  Een selectie in dit veld verandert de
kleur van de pictogrammen.

•        \ **Tekstkleur: **\ Een selectie in dit veld verandert de
tekstkleur.

.. rubric:: Typografie
   :name: typografie

•        \ **Primaire lettertype:**\  Selecteer hier het lettertype uit
de vervolgkeuzelijst. De stijl van de kop en knoppen wordt hiermee
gewijzigd.

•        \ **Primaire lettergrootte:**\  Selecteer hier het lettertype
uit de vervolgkeuzelijst. De grootte van de letters van de kop en
knoppen worden hiermee gewijzigd.

•        \ **Secundaire lettergrootte:**\  Selecteer hier de
lettergrootte uit de vervolgkeuzelijst. De grootte van het lettertype
van de broodkruimels / breadcrumbs wordt hiermee gewijzigd.

•        \ **Tertiaire lettergrootte:**\  Selecteer hier de
lettergrootte uit de vervolgkeuzelijst. De grootte van de letters in de
kolommen worden hiermee gewijzigd.

•        \ **Algemene tekst lettergrootte:**\  Selecteer hier de
lettergrootte uit de vervolgkeuzelijst. De grootte van de letters van de
algemene tekst in het systeem worden hiermee gewijzigd.

**Reset **

Door te klikken op **reset**, wordt er een bevestigingsvenster geopend

Dit bevestigingsvenster geeft de volgende opties weer: ****

·         \ **Ja:**\  door op ja te klikken, wordt u teruggebracht naar
de laatst opgeslagen positie ****

·         \ **Annuleren**\ : Door op annuleren te klikken, wordt de
betreffende actie beëindigd.\ ****

****

 

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Deze knop slaat alle nieuwe en niet-opgeslagen wijzigingen
op.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
