.. raw:: html

   <div class="WordSection1">

**Thema’s **

Zoals de naam doet vermoeden, kan de gebruiker met deze functie de look
& feel van het systeem veranderen. De gebruikers kunnen het thema
configureren door naar deze schermen te navigeren vanuit het Global
Menu. De sjablonen zijn vooraf gedefinieerd en kunnen niet door de
gebruiker worden verwijderd, hoewel de gebruiker het voorgedefinieërde
thema kan herhalen en handmatig kan bewerken. Alle voorgeconfigureerde
thema's worden weergegeven in het linkerdeel van het scherm. Het thema
dat in groen is gemarkeerd, geeft het huidige thema weer. De gebruiker
kan het thema alleen toepassen door op de knop ‘’toepassen’’ te klikken.
De gebruiker kan ook de lettergrootte en het lettertype van het systeem
wijzigen.

 

**Gereedschaps-tip: **\ Om clickables makkelijker te begrijpen, kan de
gebruiker met de muis over elke knop zweven. Hiermee wordt de naam van
de knop weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pinvast-knop**\ : Zoals de naam suggereert, wordt de
pinvast-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in de kop van
Atlantis Dashboard.\ ****

.. raw:: html

   </div>

 

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker een
venster samenvouwen door op de linker pijl te klikken. Als het venster
samengevouwen is, verandert de pijl van richting (naar rechts). Door op
de rechterpijl te klikken, kan de gebruiker het venster weer uitvouwen.

·         \ **Toevoegen:**\  Door op **+** te drukken, kan de gebruiker
thema’s toevoegen.

 

·         \ **Zoeken:**\  In dit veld kan de gebruiker zoeken en
selecteren op thema’s uit de lijst.

 

·         \ **Bewerken:**\  Door op het **potlood-**\ pictogram te
drukken, kan de gebruiker themabestanden bewerken.\ ****

·         \ **Verwijderen: **\ Door op het **prullenbak-**\ pictogram te
drukken, kan de gebruiker thema’s verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie beëindigd. ****

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Om thema’s te kunnen bewerken, moeten alle velden
verplicht worden ingevuld.  **\ 

.. raw:: html

   </div>

.. raw:: html

   </div>
