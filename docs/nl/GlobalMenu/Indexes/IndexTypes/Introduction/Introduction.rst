.. raw:: html

   <div class="WordSection1">

**Indextypes**

Indexen zijn secundaire toegang tot archieven. Via indexen wordt toegang
tot archieven gegeven door een ander punt, zoals personen, adressen,
beschrijvingen van handelingen en registers.

Elk indextype heeft een aantal vaste velden, zoals identificatie, datum,
personen, locaties en publiciteitsbeperkingen. Aan elk indextype kunnen
20 nieuwe velden toegevoegd worden, zoals Nummer, Datum, Ja / Nee of
Tekst. De gebruiker kan het indextype via het Global Menu bereiken. Het
pictogram van het Global Menu staat in de rechterhoek van het scherm
(knop rechts van de profielfoto).

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Vastpin-knop: ** <../Pin%20Button.docx>`__\  Zoals de
naam suggereert, kan de gebruiker met deze knop pagina’s vastpinnen. De
vastgepinde pagina’s worden in een lijst opgeslagen, die te vinden is in
de kop van Atlantis Dashboard. \ ****

.. raw:: html

   </div>

 

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker het
menu samenvouwen. Om het menu samen te vouwen, moet de gebruiker op de
linkpijl-toets drukken. Als het menu is samengevouwen, verandert de pijl
van richting (naar rechts). Door op de rechterpijl te klikken, wordt het
menu weer uitgevouwen.

 

·         \ **Zoeken:**\  Met dit veld kan de gebruiker zoeken en
selecteren op indextypes uit de lijst\ ****

****

 

·         \ **Toevoegen: **\ Door op + te klikken, kan de gebruiker
indextype-bestanden toevoegen ****

****

 

·         \ **Verwijderen:**\  Het **prullenbak-**\ pictogram is de
verwijderknop. Door te klikken op het **prullenbak-**\ pictogram,
verschijnt er een bevestigingsvenster. ****

§  \ **Ja**\ : Door op ja te klikken, wordt het betreffende bestand
verwijderd.

§  \ **Nee**\ : Door op nee te klikken, wordt de betreffende actie
beëindigd.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

·         \ **Bewerken: **\ Door op het **potlood-**\ pictogram te
drukken, kan de gebruiker indextypes bewerken. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
