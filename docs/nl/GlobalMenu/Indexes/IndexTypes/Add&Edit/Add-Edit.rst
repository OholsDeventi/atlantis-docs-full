.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen of bewerken **

.. raw:: html

   </div>

·         Een lijst met vooraf geconfigureerde indextypes zijn te vinden
in de linkerkant van het scherm

·         Klik op ofwel de + knop of op het **potlood-**\ pictogram om
de gewenste actie uit te voeren \*Alle velden zijn verplicht.

·         \ **Metagegevens-model:**\  Selecteer de metagegevens-model
uit de vervolgkeuze lijst.

·         \ **Omschrijving:**\  Voeg de omschrijving van het indextype
toe

·         \ **Standaard typen: **\ selecteer de publiciteitsbeperking in
de vervolgkeuzelijst.

·         \ **Aanvraagbaar**\ : Markeer indien documenten uit dit
indextype kunnen worden aangevraagd.

 

·         \ **Bestelbaar**\ : Markeer als een weergave van de documenten
uit dit indextype besteld kan worden.

·         \ **Veldnummer:**\  De gebruiker kan hier 20 velden uit de
vervolgkeuzelijst kiezen

·         \ **Veldnaam:**\  Voer hier de veldnaam in het tekstveld in

·         \ **Veldtype:**\  Hier kan het veldtype worden opgeschreven,
zoals een nummer, datum, ja/nee of een tekst

·         \ **+ pictogram: **\ Door te klikken op het + pictogram aan
het eind van iedere rij, kan de gebruiker een nieuwe rij toevoegen. Met
het + pictogram aan het einde van een leeg veld, kan er een vrij veld
worden toegevoegd, met een maximum van 20 nieuwe velden. ****

·         \ **Verwijderknop:**\  Door op het **prullenbak-**\ pictogram
te klikken, kan een rij worden verwijderd. Met het prullenbak-pictogram,
kan een vrij veld worden verwijderd. ****

**Reset **

·         Door te klikken op **reset,** wordt een bevestigingsvenster
weergegeven\ ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
uw laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Opslaan**

·         Met deze optie worden alle nieuwe en niet opgeslagen
wijzigingen opgeslagen ****

****

 

**Afsluiten**

•        Door te klikken op het **afsluit-**\ pictogram, wordt er een
bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
