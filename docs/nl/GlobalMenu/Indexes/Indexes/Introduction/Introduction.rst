.. raw:: html

   <div class="WordSection1">

**Index**

Dit is een gedeelte worden de indexen van de bestanden weergegeven. Elk
bestand in de index volgt het formaat van het toegewezen indextype. Om
de details hiervan weer te geven, moet de gebruiker op de index klikken.
Deze laat dan een lijst aan de linkerkant van het scherm zijn. De
gebruiker kan het indextype bereiken middels \ `**Global
Menu** <file:///C:\Users\thera\Desktop\Help%20Folder%20Structure-%20Revised-V.02%20(2)\Help%20Folder%20Structure-%20Revised-V.02\GlobalMenu\Indexes\Indexes\Global%20Menu.docx>`__\ **.**\ 
Het pictogram van \ `**Global
Menu** <file:///C:\Users\thera\Desktop\Help%20Folder%20Structure-%20Revised-V.02%20(2)\Help%20Folder%20Structure-%20Revised-V.02\GlobalMenu\Indexes\Indexes\Global%20Menu.docx>`__\ **
**\ is in de meest rechterhoek van het scherm te vinden (de knop rechts
naast de profielfoto).

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Pin%20Button.docx>`__\  Zoals
de naam doet vermoeden, kan de gebruiker met deze knop pagina’s
vastpinnen. De vastgepinde pagina’s worden opgeslagen in een lijst, die
te vinden is onder de kop van Atlantis Dashboard. \ ****

.. raw:: html

   </div>

 

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker het
venster samenvouwen. Om het venster samen te vouwen, moet de gebruiker
op de linkpijl-knop drukken. Zodra het venster is samengevouwen,
verandert de richting van de pijl naar rechts. Om het venster weer uit
te vouwen, moet de gebruiker op de rechterpijl-knop drukken.

 

·         \ **Toevoegen:**\  Klik op + om index bestanden toe te voegen.

****

 

·         \ **Zoeken:**\  Hiermee kan de gebruiker indexen uit de lijst
zoeken. ****

·         \ **Verwijderen**\ **: **

Klik op het **prullenbak-**\ pictogram om de bestanden van indexen te
verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de betreffende
actie beëindigd. ****

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Bewerken: **\ Door op het **potlood-**\ pictogram te
klikken, kan de gebruiker de index-bestanden bewerken.

.. raw:: html

   </div>

.. raw:: html

   </div>
