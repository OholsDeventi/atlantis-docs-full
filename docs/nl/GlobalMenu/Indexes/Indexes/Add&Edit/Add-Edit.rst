.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Toevoegen of bewerken **

.. raw:: html

   </div>

·         Klik op de toevoegknop (+) of op het **potlood-**\ pictogram
voor de gewenste actie.

·         \ **\*Alle velden zijn verplicht.**

·         \ **Titel:**\  In dit veld moet de gebruiker de titel van de
index invullen.

·         \ **Plaats:**\  In dit veld moet de gebruiker de plaats van de
bestanden van de index invullen.

·         \ **Soort:**\  De vervolgkeuzelijst staat gebruikers toe om te
selecteren op een indextype voor de desbetreffende index.

·         \ **Startdatum:**\  Selecteer de startdatum van de bestanden
uit de kalender.

·         \ **Einddatum:**\  Selecteer de einddatum van de bestanden uit
de kalender.

****

 

**      Reset**

·         Door te klikken op **Reset,** verschijnt er een
bevestigingsvenster. ****

·         \ **Ja:**\  Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

****

 

**      Opslaan**

·         Deze knop slaat alle nieuwe en niet-opgeslagen wijzigingen op.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

****

 

**Afsluiten **

•        Door te klikken op het **afsluit-**\ pictogram, verschijnt er
een bevestigingsvenster. ****

§  \ **Ja: **\ door te klikken op ja, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ door te klikken op annuleren, wordt de betreffende
actie beëindigd.

.. raw:: html

   </div>
