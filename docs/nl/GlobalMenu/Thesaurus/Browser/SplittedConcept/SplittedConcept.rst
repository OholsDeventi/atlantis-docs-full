.. raw:: html

   <div class="WordSection1">

**Splitsen: **\ Door op splitsen te drukken, wordt het
splitsingsconceptvenster geopend. \*alle velden die onder de
bewerksectie vallen. ****

§  \ **Toevoegen:**\  Door te klikken op +, kan de gebruiker de relatie
van de knooppunten toevoegen. ****

§  \ **Semantiek:**\  Selecteer segmenten uit de vervolgkeuzelijst. ****

§  \ **Relatie:**\  Selecteer de relatie uit de vervolgkeuzelijst. ****

§  \ **Samenvoeg-knop:**\  Door te klikken op de “functie-knop” worden
de gerelateerde knooppunten uitgebreid. Selecteer een knooppunt uit de
lijst. ****

§  \ **Reset:**\  Door te klikken op **Reset,** word teen
bevestigingsvenster weergegeven. Dit bevestigingsvenster bestaat uit de
volgende opties: ****

o   \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie. ****

o   \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd.  

§  \ **Opslaan: **\ Met deze knop kan de gebruiker alle nieuwe en
niet-opgeslagen wijzigingen opslaan.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

 

.. raw:: html

   </div>

.. raw:: html

   </div>
