.. raw:: html

   <div class="WordSection1">

**Thesaurus Browser**

Op de pagina 'Thesaurus Browser' ziet de gebruiker een lijst van
'Domein', 'Conceptgroep' en 'Concept'. Als de gebruiker op '+' klikt,
wordt het menu uitgebreid en toont het een lijst- of boom van concepten
en markeringen (items die zijn weergegeven met een checkbox-icoon). Elk
item met een '+' teken, toont sub-items zien die gekoppeld zijn aan het
ouder item.

Het linkerdeelvenster toont een lijst met alle knooppunten die in het
systeem aanwezig zijn. De legende die bovenaan het knooppunt wordt
weergegeven, wordt gebruikt om een bepaald knooppunt toe te wijzen. Elk
item dat met '+' staat aangegeven, is een ouderknooppunt en een
subonderdeel daar binnen zijn kinderknooppunten:

**Blauw:**\  Conceptgroep

**Rood:**\  Domein

**Zwart:**\  Concept

**Gereedschaps-tip: **\ Een gebruiker kan met de muis over elke knop
zweven, waardoor aanvullende informatie van de knop wordt weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s vast te
pinnen. De vastgepinde pagina’s worden opgeslagen in een lijst die onder
de \ `**Atlantis
Dashboard** <http://dev-deventit.staging.easternenterprise.com/#/dashboard>`__\ 
te vinden is. ****

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  Door te klikken op de
samenvoeg-knop (weergegeven in de boomstructuur) kunnen knooppunten
worden samengevoegd. ****

·         \ **Voeg oorspronkelijke knooppunten toe: **\ Door te klikken
op +, kunnen knooppunten worden toegevoegd. (Let op: dezelfde pagina is
beschikbaar vanaf onderstaande acties/velden tijdens het samenvoegen van
een knooppunt.) ****

·         \ **Vervolgkeuzelijst:**\  Door te klikken op de
vervolgkeuzelijst, kan de gebruiker de voorkeurstaal aanklikken. ****

·         \ **Zoeken: **\ De gebruiker kan zoeken naar een specifiek
knooppunt-item of sub-item van de lijst. ****

·         \ **Verwijderen: **\ Klik op het **prullenbak-**\ pictogram om
auteursrechthouder-bestanden te verwijderen. ****

§  \ **Ja: **\ Door te klikken op ja, wordt het bestand verwijderd. ****

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de betreffende
actie beëindigd. ****

·         \ **Afsluiten: **\ Door te klikken op afsluiten, wordt ere en
bevestigingsvenster weergegeven.  \ ****

§  \ **Ja: **\ Door te klikken op ja, wordt de huidige pagina
afgesloten\ ****

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.\ ****

 

.. raw:: html

   </div>
