.. raw:: html

   <div class="WordSection1">

**Thesaurus Browser**

 

**Knooppunten samenvoegen **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Om de knooppunten te kunnen samenvoegen, kan de gebruiker op
de **Samenvoegen**-knop bovenaan de pagina klikken.\ ****

.. raw:: html

   </div>

·         Door op de **Samenvoegen**-knop te drukken wordt een tweede
lijst met de knooppunten geopend aan de rechterzijde van het scherm.
****

·         Het **Knooppunten Samenvoegen Formulier** is zichtbaar tussen
de lijsten van knooppunten.\ ****

·         Vervolgens kan de gebruiker in de beide lijsten een knooppunt
selecteren die in het formulier samengevoegd kunnen worden.\ ****

·         De data van de geselecteerde knooppunten wordt ingevuld in het
samengevoegde knooppunten formulier. ****

·         \ **Toevoegen: **\ Klik op de + knop om de keuze knoppen uit
te vouwen zodat er gekozen kan worden tussen Semantiek en de Relatie.
****

·         \ **Semantiek: **\ De gebruiker kan Semantisch kiezen uit de
keuzelijst om knooppunten met een semantische relatie toe te wijzen of
om knooppunten samen te voegen.\ ****

·         \ **Relatie: **\ De gebruiker kan meerdere artikelen aan
elkaar linken met deze functie. Selecteer de relatie uit de keuzelijst.
****

·         \ **Reset: **\ Klik op de Reset-knop om terug te gaan naar de
laatst opgeslagen instellingen. Let wel dat dit niet hetzelfde is als
‘Terugkeren naar Fabrieksinstellingen’.\ ****

·         \ **Opslaan: **\ De Opslaan-knop is niet klikbaar totdat de
semantiek en relatie tussen twee knooppunten is geselecteerd. Na het
selecteren kunnen met de Opslaan-knop de wijzigingen worden
opgeslagen.\ ****

·         \ **Bewerken: **\ Zodra de wijzigingen zijn opgeslagen
verschijnt de **Bewerken**-knop (**Potlood** icoon).  \ ****

·         \ **Prullenbak: **\ Zodra de veranderingen zijn opgeslagen
verschijnt de **Prullenbak-**\ knop. Door hier op te klikken worden de
samengevoegde knooppunten verwijderd. ****

·         \ **Samenvoegen: **\ Zodra de eerdere veranderingen gemaakt
zijn verschijnt de **Samenvoegen**-knop. Door hier op te klikken worden
knooppunten samengevoegd.\ ****

·         \ **Wanneer 2 knooppunten van een synoniemenlijst samen zijn
gevoegd, dienen de relaties door de gebruiker worden toegevoegd. Anders
werkt het niet. **

****

 

**Na het samenvoegen van de knooppunten verschijnt een nieuw
formulier.**

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Toevoegen: **\ Klik op de **+** knop om een
knooppunt-onderdeel toe te voegen. ****

.. raw:: html

   </div>

·         \ **Taal: **\ Klik op de keuzelijst om de gewenste taal te
selecteren.\ ****

·         \ **Benaming: **\ Voer een benaming in bij het tekstveld. Dit
veld is niet verplicht. ****

·         \ **Voorkeur: **\ Een vinkje geeft aan dat dit het
voorkeursknooppunt-onderdeel is.\ ****

·         \ **Standaard: **\ Een vinkje geeft aan dat dit het standaard
knooppunt-onderdeel is.\ ****

·         \ **Rangorde: **\ Geef in het tekstveld de rangorde aan. ****

·         \ **Annuleren: **\ Klik op Annuleren om de handeling af te
breken.\ ****

·         \ **Toevoegen: **\ De knop kan enkel aangeklikt worden als
alle velden zijn ingevuld. Door op de knop te drukken voegt men een
knooppunt-onderdeel toe.\ ****

·         \ **Bewerken:**\  Wanneer een knooppunt-onderdeel is
toegevoegd, verschijnt er in de rij een **Potlood**-knop om deze aan te
passen. ****

o   \ **Opslaan: **\ Klik hier om de wijzigingen op te slaan. ****

·         \ **Verwijderen: **\ Klik op het **Prullenbak-**\ icoon om een
knooppunt-onderdeel rij te verwijderen.\ ****

§  \ **Ja: **\ Klik op Ja om het geselecteerde onderdeel te verwijderen.
****

§  \ **Annuleren**\ : Klik op Annuleren om de huidige handeling af te
breken.

                  Na een knooppunt-onderdeel toe te hebben gevoegd, kan
de gebruiker onderaan de pagina een nieuw formulier vinden.

·         \ **Toevoegen:**\  Klik op de **+** knop om verdere details
toe te voegen aan het knooppunt dat de gebruiker aan het maken is.

·         \ **Taal:**\  Klik op de keuzelijst-knop om de taal van
voorkeur te kiezen.

·         \ **Notitie:**\  Voeg een notitie toe in het tekstveld.

·         \ **Opslaan:**\  Klik hier om de wijzigingen op te slaan.

·         \ **Bewerken:**\  Klik op de **Potlood**-knop om de notitie
aan te passen.

·         \ **Verwijderen:**\  Klik op het **Prullenbak**-icoon om de
notitie te verwijderen.

·         \ **Knooppunt type:**\  Klik op de keuzelijst om het type te
selecteren. De opties zijn: Domein, Concept groep of Concept. 

·         \ **Rangorde Nummer:**\  Voeg een rangorde nummer van het
knooppunt toe in het tekstveld.

·         \ **Concept identificatie:**\  Voeg een concept identificatie
van het knooppunt toe.

·         \ **Details:**\  Details van het knoppunt verschijnen hier als
deze zijn toegevoegd. Is dit niet het geval, dan verschijnt er “Geen
Details verbonden”.

·         \ **Segmenten:**\  Selecteer de segmenten uit de keuzelijst.
De segmenten worden geselecteerd als chip, welke verwijdert kan worden
door op **Sluiten** te drukken op de chip zelf.

o   \ **Chip: Wanneer een gebruiker op een Segment-veld klikt, dan
verschijnt er een lijst met voor geconfigureerde segmenten. Na het
selecteren van een van de segmenten uit de lijst, verschijnt er een
‘chip’.**\ 

 

**Een ander scherm formulier kan worden uitgevouwen na het toevoegen van
een knooppunt.**\ 

·         \ **Toevoegen:**\  Klik op de **+** knop om een relatie tussen
knooppunten toe te voegen.

·         \ **Semantisch:**\  Selecteer de segmenten uit de keuzelijst.

·         \ **Relatie:**\  Selecteer de relatie uit de keuzelijst.

·         \ **Samenvoegen:**\  Klik op de functietoets om de
gerelateerde knooppunten uit te vouwen. Selecteer het knooppunt in de
lijst. \ ****

**Reset**

Door op de **Reset**-knop te klikken verschijnt een bevestigingsscherm.
Het bevestigingsscherm laat de volgende opties zien:

·         \ **Ja**\ : Door op **Ja** te klikken gaat de gebruiker terug
naar de laatst opgeslagen instellingen.\ ****

·         \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken.\ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop worden de wijzigingen opgeslagen. ****

.. raw:: html

   </div>

 

 

Alle knooppunten in het linker panel behouden ook hun individuele
functies. Klik met de rechtermuisknop op een knooppunt.

**     Bewerken: **\ Klik op de **Potlood-**\ knop om een knooppunt te
bewerken. De velden zijn hetzelfde als hierboven al is aangegeven.

Na het samenvoegen van de knooppunten verschijnt een nieuw formulier.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Toevoegen: **\ Klik op de **+** knop om een
knooppunt-onderdeel toe te voegen. ****

.. raw:: html

   </div>

·         \ **Taal: **\ Klik op de keuzelijst om de gewenste taal te
selecteren.\ ****

·         \ **Benaming: **\ Voer een benaming in bij het tekstveld. Dit
veld is niet verplicht. ****

·         \ **Voorkeur: **\ Een vinkje geeft aan dat dit het
voorkeursknooppunt-onderdeel is.\ ****

·         \ **Standaard: **\ Een vinkje geeft aan dat dit het standaard
knooppunt-onderdeel is.\ ****

·         \ **Rangorde: **\ Geef in het tekstveld de rangorde aan. ****

·         \ **Annuleren: **\ Klik op Annuleren om de handeling af te
breken.\ ****

·         \ **Toevoegen: **\ De knop kan enkel aangeklikt worden als
alle velden zijn ingevuld. Door op de knop te drukken voegt men een
knooppunt-onderdeel toe.\ ****

·         \ **Bewerken:**\  Wanneer een knooppunt-onderdeel is
toegevoegd, verschijnt er in de rij een **Potlood**-knop om deze aan te
passen. ****

o   \ **Opslaan: **\ Klik hier om de wijzigingen op te slaan. ****

·         \ **Verwijderen: **\ Klik op het **Prullenbak-**\ icoon om een
knooppunt-onderdeel rij te verwijderen.\ ****

§  \ **Ja: **\ Klik op Ja om het geselecteerde onderdeel te verwijderen.
****

§  \ **Annuleren**\ : Klik op Annuleren om de huidige handeling af te
breken.

 

                  Na het toevoegen van een node kan de gebruiker
onderstaand formulier zien:\ ****

·         \ **Toevoegen:**\  Klik op de **+** knop om verdere details
toe te voegen aan het knooppunt dat de gebruiker aan het maken is.

·         \ **Taal:**\  Klik op de keuzelijst-knop om de taal van
voorkeur te kiezen.

·         \ **Notitie:**\  Voeg een notitie toe in het tekstveld.

·         \ **Opslaan:**\  Klik hier om de wijzigingen op te slaan.

·         \ **Bewerken:**\  Klik op de **Potlood**-knop om de notitie
aan te passen.

·         \ **Verwijderen:**\  Klik op het **Prullenbak**-icoon om de
notitie te verwijderen.

·         \ **Knooppunt type:**\  Klik op de keuzelijst om het type te
selecteren. De opties zijn: Domein, Concept groep of Concept. 

·         \ **Rangorde Nummer:**\  Voeg een rangorde nummer van het
knooppunt toe in het tekstveld.

·         \ **Concept identificatie:**\  Voeg een concept identificatie
van het knooppunt toe.

·         \ **Details:**\  Details van het knoppunt verschijnen hier als
deze zijn toegevoegd. Is dit niet het geval, dan verschijnt er “Geen
Details verbonden”.

·         \ **Segmenten:**\  Selecteer de segmenten uit de keuzelijst.
De segmenten worden geselecteerd als chip, welke verwijdert kan worden
door op **Sluiten** te drukken op de chip zelf.

o   \ **Chip: Wanneer een gebruiker op een Segment-veld klikt, dan
verschijnt er een lijst met voor geconfigureerde segmenten. Na het
selecteren van een van de segmenten uit de lijst, verschijnt er een
‘chip’.**\ 

 

**Een nieuw venster formulier kan uitgebreid worden na het toevoegen van
een node.           **

·         \ **Toevoegen:**\  Klik op de **+** knop om een relatie tussen
knooppunten toe te voegen.

·         \ **Semantisch:**\  Selecteer de segmenten uit de keuzelijst.

·         \ **Relatie:**\  Selecteer de relatie uit de keuzelijst.\ ****

·         \ **Samenvoegen:**\  Klik op de functietoets om de
gerelateerde knooppunten uit te vouwen. Selecteer het knooppunt in de
lijst.\ ****

**Reset**

Door op de **Reset**-knop te klikken verschijnt een bevestigingsscherm.
Het bevestigingsscherm laat de volgende opties zien:

·         \ **Ja**\ : Door op **Ja** te klikken gaat de gebruiker terug
naar de laatst opgeslagen instellingen.\ ****

·         \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken.\ ****

****

 

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop worden de wijzigingen opgeslagen. ****

.. raw:: html

   </div>

****

 

 

Na het samenvoegen van de knooppunten verschijnt een nieuw formulier.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Toevoegen: **\ Klik op de **+** knop om een
knooppunt-onderdeel toe te voegen. ****

.. raw:: html

   </div>

·         \ **Taal: **\ Klik op de keuzelijst om de gewenste taal te
selecteren.\ ****

·         \ **Benaming: **\ Voer een benaming in bij het tekstveld. Dit
veld is niet verplicht. ****

·         \ **Voorkeur: **\ Een vinkje geeft aan dat dit het
voorkeursknooppunt-onderdeel is.\ ****

·         \ **Standaard: **\ Een vinkje geeft aan dat dit het standaard
knooppunt-onderdeel is.\ ****

·         \ **Rangorde: **\ Geef in het tekstveld de rangorde aan. ****

·         \ **Annuleren: **\ Klik op Annuleren om de handeling af te
breken.\ ****

·         \ **Toevoegen: **\ De knop kan enkel aangeklikt worden als
alle velden zijn ingevuld. Door op de knop te drukken voegt men een
knooppunt-onderdeel toe.\ ****

·         \ **Bewerken:**\  Wanneer een knooppunt-onderdeel is
toegevoegd, verschijnt er in de rij een **Potlood**-knop om deze aan te
passen. ****

o   \ **Opslaan: **\ Klik hier om de wijzigingen op te slaan. ****

·         \ **Verwijderen: **\ Klik op het **Prullenbak-**\ icoon om een
knooppunt-onderdeel rij te verwijderen.\ ****

§  \ **Ja: **\ Klik op Ja om het geselecteerde onderdeel te verwijderen.
****

§  \ **Annuleren**\ : Klik op Annuleren om de huidige handeling af te
breken.

             Na het toevoegen van een knooppunt-onderdeel, kan de
gebruiker het onderstaande formulier zien:

·         \ **Toevoegen:**\  Klik op de **+** knop om verdere details
toe te voegen aan het knooppunt dat de gebruiker aan het maken is.

·         \ **Taal:**\  Klik op de keuzelijst-knop om de taal van
voorkeur te kiezen.

·         \ **Notitie:**\  Voeg een notitie toe in het tekstveld.

·         \ **Opslaan:**\  Klik hier om de wijzigingen op te slaan.

·         \ **Bewerken:**\  Klik op de **Potlood**-knop om de notitie
aan te passen.

·         \ **Verwijderen:**\  Klik op het **Prullenbak**-icoon om de
notitie te verwijderen.

·         \ **Knooppunt type:**\  Klik op de keuzelijst om het type te
selecteren. De opties zijn: Domein, Concept groep of Concept. 

·         \ **Rangorde Nummer:**\  Voeg een rangorde nummer van het
knooppunt toe in het tekstveld.

·         \ **Concept identificatie:**\  Voeg een concept identificatie
van het knooppunt toe.

·         \ **Details:**\  Details van het knoppunt verschijnen hier als
deze zijn toegevoegd. Is dit niet het geval, dan verschijnt er “Geen
Details verbonden”.

·         \ **Segmenten:**\  Selecteer de segmenten uit de keuzelijst.
De segmenten worden geselecteerd als chip, welke verwijdert kan worden
door op **Sluiten** te drukken op de chip zelf.

o   \ **Chip: Wanneer een gebruiker op een Segment-veld klikt, dan
verschijnt er een lijst met voor geconfigureerde segmenten. Na het
selecteren van een van de segmenten uit de lijst, verschijnt er een
‘chip’.**\ 

 

 

**Een nieuw venster formulier kan uitgebreid worden na het toevoegen van
een node. **\ 

·         \ **Toevoegen:**\  Klik op de **+** knop om een relatie tussen
knooppunten toe te voegen.

·         \ **Semantisch:**\  Selecteer de segmenten uit de keuzelijst.

·         \ **Relatie:**\  Selecteer de relatie uit de keuzelijst.\ ****

·         \ **Samenvoegen:**\  Klik op de functietoets om de
gerelateerde knooppunten uit te vouwen. Selecteer het knooppunt in de
lijst.\ ****

****

 

**Reset**

Door op de **Reset**-knop te klikken verschijnt een bevestigingsscherm.
Het bevestigingsscherm laat de volgende opties zien:

·         \ **Ja**\ : Door op **Ja** te klikken gaat de gebruiker terug
naar de laatst opgeslagen instellingen.\ ****

·         \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken.\ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop worden de wijzigingen opgeslagen. ****

.. raw:: html

   </div>

 

 

**Alfabetisch sorteren:**\  Door op deze knop te drukken worden de
knooppunten op alfabetische volgorde gerangschikt. ****

 

****

 

.. raw:: html

   </div>
