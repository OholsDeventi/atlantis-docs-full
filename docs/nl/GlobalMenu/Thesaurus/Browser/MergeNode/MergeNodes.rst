.. raw:: html

   <div class="WordSection1">

 

**Knooppunten samenvoegen**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Om knooppunten samen te voegen, moet de gebruiker op de
samenvoeg-knop bovenaan de pagina klikken. ****

.. raw:: html

   </div>

·         Door te klikken op samenvoegen, wordt er een dubbele lijst van
de knooppunten in het rechterdeel van het scherm geopend. ****

·         Het formulier om knooppunten samen te voegen, is te zien
tussen beide knooppuntlijsten. ****

·         Vervolgens kan de gebruiker de knooppunten van beide lijsten
selecteren, zodat de knooppunten in het formulier worden samengevoegd.
****

·         De gegevens van de geselecteerde knooppunten worden ingevuld
op het samenvoeg-formulier. ****

·         \ **Toevoegen: **\ Door te klikken op de + knop, wordt er een
vervolgkeuzemenu weergegeven, waar de gebruiker semantiek en relatie kan
toevoegen. ****

·         \ **Semantiek: **\ De gebruiker kan semantiek van de
vervolgkeuzelijst toewijzen aan de knooppunten, zodat knooppunten kunnen
worden samengevoegd. ****

·         \ **Relaties: **\ De gebruiker kan hiermee de verschillende
items aan elkaar linken. Selecteer de relatie van de vervolgkeuzelijst.
****

·         \ **Reset: **\ Door te klikken op de reset-knop, wordt de
gebruiker teruggebracht naar de laatst geconfigureerde instellingen. Let
op: met deze knop kan de gebruiker niet terug naar “standaard
fabrieksinstellingen”. ****

·         \ **Opslaan: **\ De opslaan-knop kan de gebruiker nog niet
aanklikken, totdat de gebruiker semantiek en relatie tussen twee
knooppunten heeft aangegeven. Nadat dit gebeurd is, kan de gebruiker de
opslaan-knop wel gebruiken. ****

·         \ **Bewerken: **\ Zodra de wijzigingen zijn opgeslagen, wordt
de bewerkknop (het **potlood-**\ pictogram) weergegeven. ****

·         \ **Prullenbak-pictogram: **\ Zodra de wijzigingen opgeslagen
zijn, wordt ook het **prullenbak-**\ pictogram zichtbaar. Door hierop te
klikken, worden de samengevoegde knooppunten verwijderd. ****

·         \ **Samenvoegen: **\ Als de vorige wijzigingen zijn
aangebracht, wordt ook de samenvoeg-knop zichtbaar. Door hierop te
klikken, kan de gebruiker knooppunten samenvoegen. ****

·         \ **Als twee knooppunten van een thesaurus worden
samengevoegd, moet de gebruiker de relatie tussen deze twee knooppunten
toevoegen, anders werkt het niet. **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

****

 

 

.. raw:: html

   </div>
