.. raw:: html

   <div class="WordSection1">

**Onderliggende knooppunten toevoegen:**\  Klik met de rechtermuisknop
op een ouderknooppunt in het bovenste menu, klik op deze knop om een
kinderknooppunt toe te voegen. Deze actie zal een nieuw venster
openen.\ ****

§  \ **Knooppunt-type: **\ Selecteer hier het knooppunt-type van
vervolgkeuzelijst. ****

§  \ **Relatietype:**\  Selecteer het relatietype van de
vervolgkeuzelijst. ****

§  \ **Bestelnummer:**\  Voer hier bestelnummer in

§  \ **Concept ID:**\  Voer hier concept ID van het knooppunt toe

§  \ **Details:**\  Details van de knooppunten verschijnen hier, als
deze zijn toegevoegd. Als niet, wordt hier “geen details verbonden”
weergegeven.

§  \ **Segmenten:**\  Selecteer de segmenten die in de vervolgkeuzelijst
staan. De segmenten worden geselecteerd als een chip, die verwijderd kan
worden door op de knop “afsluiten” te drukken, op de chip zelf.

§  \ **Een ander venster kan worden geopend als er een knooppunt is
toegevoegd.**\ 

§  \ **Toevoegen:**\  Klik op + om de relaties tussen knooppunten toe te
voegen.

§  \ **Semantiek:**\  Selecteer de segmenten uit de vervolgkeuzelijst.

§  \ **Relatie:**\  Selecteer de relatie uit de vervolgkeuzelijst.  

§  \ **Samenvoeg-knop:**\  Door te klikken op de “functie-knop”, worden
de gerelateerde knooppunten laten zien. Selecteer een knooppunt uit deze
lijst.

§  \ **Reset:**\  Door te klikken op **Reset,** wordt ere en
bevestigingsvenster weergegeven. Dit bevestigingsvenster bestaat uit de
volgende opties:

**Ja**\ : Door op ja te klikken, wordt u teruggebracht naar de laatst
opgeslagen positie.

**Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie beëindigd.

§  \ **Opslaan: **\ Door op deze knop te drukken worden alle nieuwe en
niet-opgeslagen wijzigingen opgeslagen. ****

 

.. raw:: html

   </div>
