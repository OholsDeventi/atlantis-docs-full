.. raw:: html

   <div class="WordSection1">

**Kopieerlijst: **\ 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Door te klikken op de samenvoeg-knop, wordt een gedupliceerde
lijst van de knooppunten aan de rechterkant van het scherm weergegeven.
****

.. raw:: html

   </div>

·         Vervolgens kan de gebruiker het knooppunt van beide lijsten
selecteren, zodat knooppunten in het formulier kunnen worden
samengevoegd. ****

·         \ **Toevoegen: **\ Door op het + pictogram te klikken, worden
knooppunten samengevoegd.\ ****

·         \ **Semantiek: **\ De gebruiker kan semantiek in de
vervolgkeuzelijst selecteren om de knooppunten met een semantische
relatie toe te wijzen of om knooppunten samen te voegen. ****

·         \ **Relatie: **\ De gebruiker kan met behulp van deze functie
meerdere items koppelen die vermeld staan. Selecteer de relatie uit de
vervolgkeuzelijst. ****

·         \ **Reset: **\ Door te klikken op reset, keert de gebruiker
terug naar de laatst opgeslagen geconfigureerde instellingen. Let op dat
dit niet gebruikt moet worden om het systeem naar “standaard
fabrieksinstellingen” te brengen. ****

·         \ **Opslaan: **\ De opslaan-knop zal niet klikbaar zijn,
totdat u de semantiek en de relatie heeft aangegeven tussen twee
knooppunten. Na deze selectie, kunt u het wel opslaan door op opslaan te
drukken. ****

·         \ **Bewerken: **\ Zodra de veranderingen opgeslagen zijn, zal
de bewerk-knop (het **potlood-**\ pictogram) worden weergegeven. ****

·         \ **Verwijderen: **\ Zodra de veranderingen zijn opgeslagen,
zal het **prullenbak-**\ pictogram worden weergegeven. Door hierop te
klikken, worden de samengevoegde knooppunten verwijderd. ****

·         \ **Samenvoegen: **\ Zodra de vorige wijzigingen zijn
aangebracht, wordt de samenvoegknop ingeschakeld. Als u op de knop
Samenvoegen klikt, worden de knooppunten samengevoegd.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
