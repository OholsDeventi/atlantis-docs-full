.. raw:: html

   <div class="WordSection1">

**Thesaurus configuratie **

The Thesaurus Configuratie zorgt ervoor dat thesaurusknooppunten
gekoppeld worden aan velden in de database. Het topconcept kan zowel in
de thesaurus in Atlantis geselecteerd worden, als ook in een externe
thesaurus. Om databasevelden te koppelen, zijn tabel- en kolomnamen
toegewezen en kan de gebruiker ook een metagegevens-model toewijzen.

****

 

**Gereedschaps-tip: **\ Om opties makkelijker te begrijpen, kan de
gebruiker met de muis over de knoppen zweven. Hiermee wordt de naam van
de knoppen weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·        
\ `**Pinvast-button** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\ **:
**\  Zoals de naam suggereert, wordt de pinvast-knop gebruikt op
pagina’s vast te pinnen. De vastgepinde pagina’s worden in een lijst
opgeslagen die onder de kop van het Atlantis Dashboard te vinden is.
\ ****

.. raw:: html

   </div>

****

 

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker het
venster samenvouwen. Om het venster samen te vouwen, moet de gebruiker
op de linker pijl-knop drukken. Zodra het venster samengevouwen is, zal
de pijl van richting veranderen (naar rechts draaien). Om het venster
weer uit te vouwen, moet de gebruiker op de rechterpijl-knop drukken.
\ ****

****

 

·         \ **Zoeken:**\  Hiermee kan de gebruiker zoeken op thesaurus
configuraties uit de lijst\ ****

****

 

·         \ **Toevoegen: **\ Klik op **+** om thesaurus
configuratiebestanden toe te voegen.

 

·         \ **Verwijderen: **\ Het **prullenbak-**\ pictogram is de
verwijderknop. Door te klikken op het prullenbak-pictogram wordt er een
bevestigingsvenster weergegeven.

§  \ **Ja**\ : Door op ja te klikken, zal de verwijderactie worden
uitgevoerd.

§  \ **Nee:**\  Door op nee te klikken, wordt de betreffende actie
beëindigd.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Bewerken: **\ Door op het **potlood-**\ pictogram te
klikken, kan de gebruiker configuratiebestanden bewerken.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
