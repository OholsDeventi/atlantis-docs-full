.. raw:: html

   <div class="WordSection1">

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

•        Klik op + of op het **potlood-**\ pictogram om toe te voegen of
te bewerken

.. raw:: html

   </div>

•        \ **Extern: **\ Vink dit vakje aan als het “topconcept” uit een
externe bron afkomstig is.

•        \ **Top Concept: **\ Selecteer het topconcept uit het
thesaurusknooppunt.

•        \ **Kolomnaam: **\ Selecteer de kolomnaam uit de database.

•        \ **Metagegevens-model: **\ Selecteer de gerelateerde
metagegevens-model voor het topconcept.

•        \ **Tabelnaam: **\ Dit veld neemt automatisch de tabelnaam
over, als de gebruiker de gerelateerde kolom selecteert.

•        \ **Verbale waarden negeren: **\ Vink dit vakje aan als de
gebruiker de verbale waarden van het databaseveld wil negeren. Als dit
is aangevinkt, wordt er alleen op de bijbehorende thesaurus term
gezocht.

•        \ **Toevoegen: **\ Hiermee wordt het verzoek verzonden om in de
backend nieuwe/extra informatie toe te voegen als dit verder wordt
aangepast.

****

 

**Reset **

·         Door te klikken op **reset,** wordt er een bevestigingsvenster
weergegeven. ****

·         \ **Ja**\ : Door op ja te klikken, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Cancel**\ : Door op annuleren te klikken, wordt de huidige
actie beëindigd.  \ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Door op opslaan te drukken, worden alle wijzigingen
opgeslagen.\ ****

****

 

.. raw:: html

   </div>

****

 

****

 

****

 

**Afsluiten**

•        Door op afsluiten te drukken, wordt er een bevestigingsvenster
weergegeven. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten.\ ****

§  \ **Annuleren: **\ Door op annuleren te drukken, blijft u op dezelfde
pagina.

.. raw:: html

   </div>
