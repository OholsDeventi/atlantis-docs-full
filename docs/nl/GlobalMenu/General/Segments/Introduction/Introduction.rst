.. raw:: html

   <div class="WordSection1">

**Segmenten **

Zoals de naam doet vermoeden, zijn de metagegevens-modellen gekoppeld
aan verschillende segmenten. Dit maakt ook verschillende segmenten in de
database. Segmenten worden gebruikt om de machtiging van
gebruikersrechten van gebruikersgroepen te beperken per gebruiker. Een
keuze lijst met segmenten wordt laten zien al seen gebruiker
metagegevens-modellen in het content management systeem wil toevoegen of
bewerken.

**Gereedschaps-tip: **\ Om clickables makkelijker te begrijpen, kan de
gebruiker met de muis over de knoppen zweven. Dit geeft de naam van de
knoppen weer.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s vast
te pinnen of op te slaan. De opgeslagen pagina’s worden in een lijst
weergegeven onder de kop van het Atlantis Dashboard. ****

****

 

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de
gebruiker een menu kan samenvouwen. Om het venster samen te vouwen, kan
de gebruiker op de linkerpijl klikken. Zodra het menu samengevouwn is,
zal de pijl van richting veranderen (naar rechts). Door op de
rechterpijl te drukken, wordt het menu weer uitgevouwen.

 

·         \ **Toevoeg-knop:**\  Door te klikken op +, kunnen
auteurrechthouders worden toevoegd. \ ****

****

 

****

 

·         \ **Zoeken:**\  Dit veld stelt de gebruiker in staat te zoeken
en selecteren op auteurrechthouders uit de lijst. ****

****

 

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de details van de
auteurrechthouders bewerken. \ ****

****

 

·         \ **Verwijderen: **\ Klik op het **vuilnisbak-**\ pictogram om
de auteurrechthouder-bestanden te verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie verwijderd. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
