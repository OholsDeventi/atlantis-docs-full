.. raw:: html

   <div class="WordSection1">

Hoe kan ik de gegevens van de auteursrechthouder toevoegen of bewerken? 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

Klik op de **+** knop om toe te voegen of op het potlood-pictogram om
details te bewerken. ****

.. raw:: html

   </div>

 

-          Naam:                             Naam van de
   auteursrechthouder                        

-          Adres:                              Adres van de
   auteursrechthouder                                

-          Postcode:                         Postcode van de
   auteursrechthouder                         

-          Provincie:                                    Provincie van
   de auteursrechthouder

-          Stad:                                Stad van de
   auteursrechthouder

-          Land:                               Land van de
   auteursrechthouder

-          Telefoonnummer:           Telefoonnummer van de
   auteursrechthouder

-          Fax:                                  Fax van de
   auteursrechthouder

-          Website:                          Website van de
   auteursrechthouder

-          Email:                              Email van de
   auteursrechthouder

-          Jaar van overlijden:        Als de auteursrechthouder
   overladen is, vul hier het                 jaar van overlijden in.

 

Tussen de auteursrechthouder en de organisatie kunnen afspraken worden
geregistreerd voor gebruik van het werk van de auteursrechthouder:

 

**Toestemming vereist voor privégebruik: **

·         Als een reproductie voor privégebruik is besteld, moet de
auteursrechthouder toestemming geven.

 

**Toestemming vereist voor publicatie:**

·         Als een reproductie voor publicatie is besteld, moet de
auteursrechthouder toestemming geven.

**Machtiging om op het internet te publiceren:**

·         De eigenaar van het auteursrecht heeft de organisatie
toestemming gegeven om zijn werk op het internet te publiceren.

 

**Machtiging om op websites van derden te publiceren:**

·          De eigenaar van het auteursrecht heeft de organisatie
toestemming gegeven om zijn werk door te geven aan derden voor
publicatie op het internet.

 

**Machtiging voor de productie door derden:  **

·         De eigenaar van het auteursrecht heeft toestemming gegeven
voor de organisatie om reproducties van zijn werk te maken.

 

 

 

 

Wanneer het recht om het werk van de auteursrechthouder te publiceren
wordt toegekend aan de organisatie, kunnen rechten van Creative Commons
van toepassing zijn:

 

·         \ **BY:**\  Alle CC-licenties vereisen dat anderen die uw werk
op een of andere manier gebruiken, u een credit geven zoals u heeft
gevraagd, maar niet op een manier die suggereert dat u hen of hun
gebruik onderschrijft. Als zij uw werk willen gebruiken zonder u te
betalen of zonder u aan te prijzen, moeten zij eerst uw toestemming
vragen.

 

·         \ **SA:**\  U laat anderen uw werk kopiëren, distribueren,
weergeven, uitvoeren of wijzigen, zolang zij het gewijzigde werk
dezelfde voorwaarden verlenen. Als zij het gewijzigde werk onder hun
eigen voorwaarden willen verspreiden, moeten zij eerst uw toestemming
vragen.

 

·         \ **NC**\ : U laat anderen uw werk kopiëren, distribueren,
weergeven, uitvoeren, (tenzij u voor geen derivaten heeft gekozen)
aanpassen en gebruiken voor elk ander doel dan commercieel, alvorens zij
uw toestemming hebben gekregen.

 

·         \ **ND**\ : U laat anderen alleen uw originele kopieën
kopiëren, distribueren, weergeven en uitvoeren. Als zij uw werk willen
wijzigen, moeten ze eerst uw toestemming krijgen

 

**Reset **

Door te klikken op de **Reset-**\ knop, verschijnt het
bevestigingsvenster:

Dit bevestiging venster toont de volgende opties:\ ****

·         \ **Ja**\ : Door te klikken op ja, gaat u terug naar de laatst
opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
desbetreffende taak beëindigd.\ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop sla je alle nieuwe en niet opgeslagen
veranderingen op.\ ****

****

 

.. raw:: html

   </div>

****

 

**Afsluiten**

Door te klikken op het **annuleer-**\ pictogram verschijnt het
bevestigingsvenster:

Dit bevestiging venster toont de volgende opties:\ ****

****

 

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.\ ****

.. raw:: html

   </div>
