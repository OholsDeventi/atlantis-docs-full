.. raw:: html

   <div class="WordSection1">

Hoe kan ik verpakkingssoorten toevoegen of wijzigen?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Druk op de toevoeg- of bewerkknop. **

.. raw:: html

   </div>

 

**• **\ De lijst met vooraf geconfigureerde verpakkingstypes wordt aan
de linkerkant van het scherm getoond.\ ****

**• **\ Klik op het pluspunt (+) om de verpakkingstypes toe te
voegen.\ ****

**• Omschrijving: **\ Gebruiker kan het type verpakking in dit veld
definiëren, bijvoorbeeld vierkante doos, rechthoekkast, enz.\ ****

**• Hoogte: **\ De gebruiker kan de hoogte van de doos opgeven in dit
veld.

**• Breedte: **\ De gebruiker kan de breedte van de doos opgeven in dit
veld.

**• Diepte: **\ De gebruiker kan de diepte van de doos opgeven in dit
veld.

**• Selectievakje**

**Kantelen: **\ Markeer dit veld als de container in de opslag kan
worden gekanteld.\ ****

****

 

**Reset**\ 

§  Door te klikken op de **Reset**-knop, wordt het bevestigingsvenster
weergegeven. 

§  \ **Ja**\ : Door te klikken op de **Ja-**\ knop, keert u terug naar
de laatst opgeslagen positie.

§  \ **Annuleren**\ : Door te klikken op de **annuleer**\ knop, wordt de
betreffende actie beëindigd.

**Opslaan**\ 

§  Door te klikken op de **opslaan-knop**, worden de gemaakte
veranderingen opgeslagen.

****

 

**Afsluit-knop: **\ Door te klikken op het **afsluitpictogram**, wordt
het bevestigingsvenster weergegeven.

§  \ **Ja: **\ Door te klikken op **Ja**, wordt de pagina afgesloten.
****

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina. ****

 

 

.. raw:: html

   </div>
