.. raw:: html

   <div class="WordSection1">

**Mediatypes**

Zoals de naam suggereert, informeert deze module van de applicatie de
beheerder over de soorten multimedia-bestandsformaten zoals bijvoorbeeld
MP3, MP4, DIVX, MOV enz. Deze media bestandstypes zijn aan de linkerkant
van het scherm te vinden. Beheerder kan ofwel een nieuw type multimedia
toevoegen of wijzigingen kunnen maken in de bestaande.

**Gereedschapstip: **\ De gebruiker kan om de clickables makkelijker te
begrijpen, door met de muis over de knoppen te zweven. Hiermee wordt de
naam van de knoppen weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s vast
te pinnen of op te slaan. De opgeslagen pagina’s worden in een lijst
weergegeven onder de kop van het Atlantis Dashboard. ****

****

 

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de
gebruiker een menu kan samenvouwen. Om het venster samen te vouwen, kan
de gebruiker op de linker pijl klikken. Zodra het menu samengevouwen is,
zal de pijl van richting veranderen (naar rechts). Door op de
rechterpijl te drukken, wordt het menu weer uitgevouwen.

 

·         \ **Toevoeg-knop:**\  Door te klikken op +, kunnen
auteursrechthouders worden toegevoegd. \ ****

****

 

****

 

·         \ **Zoeken:**\  Dit veld stelt de gebruiker in staat te zoeken
en selecteren op auteursrechthouders uit de lijst. ****

****

 

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de details van de
auteursrechthouders bewerken. \ ****

****

 

·         \ **Verwijderen: **\ Klik op het **vuilnisbak-**\ pictogram om
de auteursrechthouder-bestanden te verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie verwijderd. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
