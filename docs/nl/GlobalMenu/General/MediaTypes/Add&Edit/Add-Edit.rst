.. raw:: html

   <div class="WordSection1">

Hoe kan ik mediatypes toevoegen of bewerken?

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Klik op het plus (**+)** pictogram om mediatype documenten toe
te voegen.

.. raw:: html

   </div>

·         \ **Naam:**\  Dit veld bestaat uit multimediabestanden die hun
formaat beschrijven.

·         \ **Pictogram:**\  Door te klikken op dit veld, staat de
beheerder toe dat gebruikers door media op de lokale computer kunnen
bladeren. Het pictogram is een symbolische weergave van het gekozen type
multimedia-bestand.

·         \ **Standaard:**\  Het selectievakje staat voor het standaard
mediatype. Het standaard icoon wordt toegepast bij afwezigheid van een
specifiek pictogram.

****

 

**Reset **

Door te klikken op de **Reset-**\ knop, verschijnt het
bevestigingsvenster:

Dit bevestiging venster toont de volgende opties:\ ****

·         \ **Ja**\ : Door te klikken op ja, gaat u terug naar de laatst
opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
desbetreffende taak beëindigd.\ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop sla je alle nieuwe en niet opgeslagen
veranderingen op.\ ****

****

 

.. raw:: html

   </div>

**Afsluiten**

Door te klikken op het **annuleer-**\ pictogram verschijnt het
bevestigingsvenster:

Dit bevestiging venster toont de volgende opties:\ ****

****

 

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten. ****

**Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina

 

.. raw:: html

   </div>
