.. raw:: html

   <div class="WordSection1">

***Hoe kan ik annotatietypes bewerken?***

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Bewerken**\ ****

.. raw:: html

   </div>

·         De gebruiker kan de beschrijving van de annotatietypes
bewerken door op het **potlood-**\ pictogram te klikken. \ ****

·         \ **Beschrijving: **\ Deze **tekst-**\ velden omschrijven de
type van annotaties die de gebruiker wil selecteren. Dit kan een
correctie, een toevoeging of informatie zijn. Dit veld is verplicht.
****

·         \ **Pictogram: **\ Dit is het pictogram van het annotatietype.
Door op dit veld te klikken kan de gebruiker door de eigen afbeeldingen
bladeren en deze uploaden van de lokale computer.

 

**Reset**\ 

§  Door te klikken op de **Reset**-knop, wordt het bevestigingsvenster
weergegeven. 

§  \ **Ja**\ : Door te klikken op de **Ja-**\ knop, keert u terug naar
de laatst opgeslagen positie.

§  \ **Annuleren**\ : Door te klikken op de **annuleer**\ knop, wordt de
betreffende actie beëindigd.

 

**Opslaan**\ 

§  Door te klikken op de **opslaan-knop**, worden de gemaakte
veranderingen opgeslagen.

 

**Afsluit-knop: **\ Door te klikken op het **afsluitpictogram**, wordt
het bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op **Ja**, wordt de pagina afgesloten.
****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleer: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
