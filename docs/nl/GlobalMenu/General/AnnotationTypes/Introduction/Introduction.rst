.. raw:: html

   <div class="WordSection1">

***Annotatietypes***

De annotatietypes kunnen gevonden worden onder het **globale menu**
pictogram (bovenin rechts), precies onder de **algemene** module. Door
te klikken op de annotatietype sectie, verschijnt er een lijst met
voorafgedefinieerde annotatietypes. Annotatietypes zijn cross
metagegevens-modellen en kunnen worden geselecteerd om het type
annotatie dat wordt weergegeven via de annotatievelden af te
beelden.\ ****

**Gereedschaps-tip: **\ Om de clickables gemakkelijk te begrijpen, kan
de gebruiker zijn muis over een knop laten zweven. Hiermee worden de
namen van de knoppen weergegeven.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pinvast-knop** <../Pin%20Button.docx>`__\ **: **\  Zoals
de naam al doet vermoeden, wordt de pinvast-knop gebruikt om de pagina's
op te slaan. De vastgepinde pagina's worden weergegeven in lijsten, die
onder de kop van Atlantis Dashboard te vinden zijn.

.. raw:: html

   </div>

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

 

.. raw:: html

   </div>

·         \ **Samenvouw-knop: **\ deze knop zorgt ervoor dat de
gebruiker een menu kan samenvouwen. Om het venster samen te vouwen, kan
de gebruiker op de linkerpijl klikken. Zodra het menu samengevouwn is,
zal de pijl van richting veranderen (naar rechts). Door op de
rechterpijl te drukken, wordt het menu weer uitgevouwen.

 

·         \ **Zoeken:**\  Met dit veld kan de gebruiker zoeken naar – en
selecteren op annotatietypes uit de lijst.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Bewerkknop: **\ Met het **potlood-**\ pictogram kan de
gebruiker bestanden van annotatietypes bewerken.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
