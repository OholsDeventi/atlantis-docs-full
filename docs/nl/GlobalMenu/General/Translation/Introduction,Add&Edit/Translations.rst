.. raw:: html

   <div class="WordSection1">

**Vertaalwerk**

"Vertalingen" is de laatste optie die u kunt vinden onder het **Global
Menu-**\ pictogram (rechterbovenhoek) direct onder de algemene module.
Zoals de naam suggereert, wordt het gebruikt om de sleutelwoorden in het
Engels, Nederlands, Duits en Vlaams te vertalen. De codeersleutel wordt
gedefinieerd, die vervolgens wordt vertaald naar de bovengenoemde talen,
die op het gereedschaps-tip wordt weergegeven.

**Sleutel**\ : De ontwikkelaars hebben de codeersleutels van de
verschillende functies van Atlantis gedeeld. Deze sleutels worden
vertaald naar de bovengenoemde talen. Nu kan de gebruiker een van die
talen selecteren op de instellingenpagina, zodat de functienaam en het
gereedschapstip in de geselecteerde taal wordt weergegeven.

**Tool Tip:**\  Om opties makkelijk te kunnen begrijpen, kan de
gebruiker over elke knop zweven. Hiermee wordt de naam van de knoppen
weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Zoeken:**\  met dit veld kan de gebruiker zoeken naar en
selecteren op coderingssleutels in de lijst.

.. raw:: html

   </div>

·         \ **Toevoegknop:**\  Klik op (+) om coderingssleutels in
verschillende talen toe te voegen.

·         \ **Bewerkknop:**\  Klik op het **potlood-pictogram** om
coderingssleutels te bewerken. Elke taal heeft zijn eigen bewerkingsknop
(potlood), zodat de gebruiker de coderingssleutel specifiek naar een
taal kan vertalen.

·         \ **Verwijderknop: **\ Klik op het **prullenbak-pictogram** om
de coderingssleutel te verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de huidige
actie beëindigd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         \ **Paginering: **\ Paginering vindt u onderaan de vertaling
pagina. Hiermee kan de gebruiker overstappen naar andere pagina's.

.. raw:: html

   </div>

 

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen – bewerken **

.. raw:: html

   </div>

·         Er zijn 5 tekstvelden. **\*** Alle velden zijn
verplicht.\ ****

·         \ **Sleutel:**\  Voer hier de coderingssleutel in het
tekstveld in.\ ****

·         \ **Engels:**\  Voer de coderingssleutel in de **Engelse**
taal in. ****

·         \ **Nederlands:**\  Voer de coderingssleutel in de
**Nederlandse** taal in. ****

·         \ **Duits: **\ Voer de coderingssleutel in de **Duitse** taal
in. ****

·         \ **Vlaams:**\  Voer de coderingssleutel in het **Vlaams** in.
****

****

 

**Reset **

Door te klikken op reset, wordt het bevestigingsvenster weergegeven. Dit
bevestigingsvenster bestaat uit de volgende opties:  

·         \ **Ja**\ : Door op ja te klikken, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door op annuleren te klikken, wordt de
betreffende actie beëindigd. ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Deze knop slaat alle nieuwe en niet opgeslagen wijzigingen op.
\ ****

·         \ **\***\  Voor de bewerkingsactie wordt alleen de knop
**opslaan** weergegeven.\ ****

****

 

.. raw:: html

   </div>

****

 

.. raw:: html

   </div>
