.. raw:: html

   <div class="WordSection1">

**Set (S-pictogram)**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Set (S-pictogram)**

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker het
menuvenster samenvouwen.

·         \ **Zoeken:**\  This field allows a user to search the set
from the list.\ ****

·         \ **Toevoegen: **\ Click on **+** button to add set
data.\ ****

·         \ **Verwijderen**

·         Klik op het **prullenbak-**\ pictogram om de dataset te
verwijderen:\ ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand
verwijderd.\ ****

§  \ **Annuleren:**\  Door op annuleren te klikken, wordt de betreffende
actie beëindigd. ****

·         \ **Bewerken: **\ Klik op het **potlood-**\ pictogram om de
dataset te bewerken.\ ****

****

 

·         \ `**XSLT: Vanuit hier kunt u de XSLT-bewerker
bereiken** <../XSLT/XSLT%20Editor.docx>`__\ ****

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Toevoegen en bewerken **

.. raw:: html

   </div>

·         Klik op het **+** icoon of op het potlood-pictogram om toe te
voegen of te bewerken ****

·         \ **Naam: **\ Voer de naam van de dataset hier.\ ****

·         \ **Extensie: **\ Voer hier de extensie van de dataset
in.\ ****

·         \ **Afsluitknop: **\ Door te klikken op de afsluitknop, wordt
de toevoegactie beëindigd. ****

**         Reset **

             Door te klikken op de **Reset-**\ knop, verschijnt ere en
bevestigingsvenster.

             In dit venster verschijnen de volgende opties: ****

·         \ **Ja**\ : Door te klikken op ja, keert u terug naar de
laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, beëindigd de
betreffende actie. ****

**          Opslaan**

·         Door te klikken op opslaan, worden alle nieuwe en niet
opgeslagen wijzigen opgeslagen. ****

**     **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

**     Afsluiten**

·         Door te klikken op het **prullenbak-**\ icoon, wordt de
dataset verwijderd. ****

§  \ **Ja: **\ Door te klikken op ja, wordt het bestand verwijderd. ****

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de betreffende
actie beëindigd. ****

 

.. raw:: html

   </div>
