.. raw:: html

   <div class="WordSection1">

**Open Data**

Deze functie wordt gebruikt om documenten in verschillende formatten op
te slaan, zodat dere partijen deze documenten ook kunnen gebruiken.
Verschillende sets van metagegevens-model objecten kunnen gemaakt worden
en in het gewenste formaat worden weergegeven, waardoor het met andere
gebruikers gedeeld kan worden. Datasets kunnen worden ingesteld in
verschillende beschikbare formaten. Door te klikken op open datasets
worden verschillende uitbreidingen geopend (dit betekent dat
verschillende formaten geassocieerd kunnen worde met particuliere open
datasets). De gebruiker kan ook formaten toevoegen. Als een gebruiker op
een open dataset klikt, wordt in de rechterkant van het scherm de
formaten weergegeven, welke op de specifieke open dataset van toepassing
zijn.

**Gereedschapstip: **\ De gebruiker kan om de clickables makkelijker te
begrijpen, door met de muis over de knoppen te zweven. Hiermee wordt de
naam van de knoppen weergegeven.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s vast
te pinnen of op te slaan. De opgeslagen pagina’s worden in een lijst
weergegeven onder de kop van het Atlantis Dashboard. ****

****

 

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de
gebruiker een menu kan samenvouwen. Om het venster samen te vouwen, kan
de gebruiker op de linkerpijl klikken. Zodra het menu samengevouwn is,
zal de pijl van richting veranderen (naar rechts). Door op de
rechterpijl te drukken, wordt het menu weer uitgevouwen.

 

·         \ **Toevoeg-knop:**\  Door te klikken op +, kunnen
auteurrechthouders worden toevoegd. \ ****

****

 

****

 

·         \ **Zoeken:**\  Dit veld stelt de gebruiker in staat te zoeken
en selecteren op auteurrechthouders uit de lijst. ****

****

 

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de details van de
auteurrechthouders bewerken. \ ****

****

 

·         \ **Verwijderen: **\ Klik op het **vuilnisbak-**\ pictogram om
de auteurrechthouder-bestanden te verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie verwijderd. ****

.. raw:: html

   </div>

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Set-knop: **\ Klik op **S** om een set toe tevoegen. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
