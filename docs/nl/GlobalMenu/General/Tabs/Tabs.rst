.. raw:: html

   <div class="WordSection1">

**Tabs**

****

 

Tabbladen worden gebruikt om de verwante metagegevens-objecten te
organiseren en te scheiden voor een aangewezen metagegevens-object.
Tabbladen worden gedefinieerd en beschreven door hun namen. Zij
vertegenwoordigen de gerelateerde metagegevens-objecten. Als er voor een
specifiek metagegevens-object relatie geen tabbladen zijn
geconfigureerd, zijn de metagegevens-objecten te vinden in het tabblad
“relaties”.  Een gebruiker kan de tabbladen veranderen voor een
metagegevens-object door een nieuw tabblad toe te voegen en de relaties
tussen metagegevens-objecten te koppelen of door veranderingen in de
bestaande name naan te brengen. Om toegang te krijgen tot tabbladen,
moet de gebruiker klikken op “\ **Global menu**\ ” onder de algemene
selectie. Zodra de gebruiker hier op klikt, verschijnen de vooraf
geconfigureerde tabbladen. \ ****

****

 

**Gereedschaps-tip:**\  Om het gebruik van opties gemakkelijker te
begrijpen, kan de gebruiker met de muis over een knop zweven. Dit geeft
de naam van de knoppen dan weer.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen of bewerken **

.. raw:: html

   </div>

·         Klik op de (+) knop of op het potlood-pictogram om bestanden
in tabbladen toe te voegen of te bewerken.

·         \ **Beschrijving:**\  Dit geeft de relatie van de tabbladen
weer.

·         \ **Tabbladen:**\  Voer hier de naam van het tabblad in, welke
de gebruiker wil voorzien van een link.

·         \ **Selectie**\ ** vakje **\ 

§  \ **Alleen-lezen**\ : Vink het vakje aan als u “alleen-lezen” rechten
wilt verlenen aan gebruikers, als zij de relaties tussen
metagegevens-objecten willen bekijken.

**Reset **

Door te klikken op **Reset,** verschijnt het bevestigingsvenster.

In dit bevestigingsvenster worden de volgende opties weergegeven: ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Opslaan**

Deze knop slaat alle nieuwe en niet-opgeslagen wijzigingen op.

**Verwijderen**\ ** **

•        Door te klikken op het **prullenbak-**\ pictogram, worden de
tabblad-bestanden verwijderd. ****

§  \ **Ja: **\ Door te klikken op ja, wordt het bestand verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de betreffende
actie beëindigd. ****

.. raw:: html

   </div>

.. raw:: html

   </div>
