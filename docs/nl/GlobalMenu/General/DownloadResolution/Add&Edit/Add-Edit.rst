.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen – bewerken **

.. raw:: html

   </div>

·         \ **Beschrijving: **\ Voer de beschrijving van de resolutie in
het tekstveld in.

·         \ **Bestandstype:**\  Plaats de bestandsextensie in het
tekstveld. De gebruiker moet beschrijven wat voor sort bestand het is.

·         \ **DPI: **\ voeg de **punten per inch** toe in die nodig zijn
om de helderheid van het object weer te geven.\ ****

·         \ **Kwaliteit: **\ Voer de vereiste kwaliteit van het item in,
welke de gebruiker moet weergeven. ****

·         \ **Horizontale resolutie: **\ Voer het aantal pixels op een
beeldscherm horizontaal in.

·         \ **Verticale resolutie: **\ Voer het aantal pixels op een
beeldscherm op de verticaal in.\ ****

**Reset **

Door te klikken op de **Reset-**\ knop, verschijnt het
bevestigingsvenster:

Dit bevestigingvesnter toont de volgende opties:\ ****

·         \ **Ja**\ : Door te klikken op ja, gaat u terug naar de laatst
opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
desbetreffende taak beëindigd.\ ****

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Met deze knop sla je alle nieuwe en niet opgeslagen
veranderingen op.\ ****

****

 

.. raw:: html

   </div>

****

 

**Afsluiten**

Door te klikken op het **annuleer-**\ pictogram verschijnt het
bevestigingsvenster:

Dit bevestigingvesnter toont de volgende opties:\ ****

****

 

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten. ****

**Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>
