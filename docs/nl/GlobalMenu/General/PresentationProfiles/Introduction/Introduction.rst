.. raw:: html

   <div class="WordSection1">

**Presentatieprofielen **

****

 

De presentatieprofielen hebben betrekking op de weergave van diverse
multimedia bestanden. Het configureert de attributen zoals de miniatuur
van een document, pdf-document, schilderijen en afbeeldingen die de
gebruiker nodig heeft (?). Hier kunt u ook de presentatie voor het
internet en het intranet met watermerken instellen. Voor het downloaden
van multimediabestanden kan dit worden ingesteld voor verschillende
situaties waarin downloads van toepassing zijn.

Presentatie profielen zijn over het algemeen toegewezen aan
metagegevens-modellen om multimedia-bestanden te bewerken. Deze optie is
te vinden in het globale menu onder algemene instellingen.

**Gereedschaps-tip: **\ Om clickables beter te begrijpen, kan de
gebruiker met de muis over de knop zweven. Dit geeft de namen van de
knop weer.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s vast
te pinnen of op te slaan. De opgeslagen pagina’s worden in een lijst
weergegeven onder de kop van het Atlantis Dashboard. ****

****

 

.. raw:: html

   </div>

·         \ **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de
gebruiker een menu kan samenvouwen. Om het venster samen te vouwen, kan
de gebruiker op de linkerpijl klikken. Zodra het menu samengevouwn is,
zal de pijl van richting veranderen (naar rechts). Door op de
rechterpijl te drukken, wordt het menu weer uitgevouwen.

 

·         \ **Toevoeg-knop:**\  Door te klikken op +, kunnen
auteurrechthouders worden toevoegd. \ ****

****

 

****

 

·         \ **Zoeken:**\  Dit veld stelt de gebruiker in staat te zoeken
en selecteren op auteurrechthouders uit de lijst. ****

****

 

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de details van de
auteurrechthouders bewerken. \ ****

****

 

·         \ **Verwijderen: **\ Klik op het **vuilnisbak-**\ pictogram om
de auteurrechthouder-bestanden te verwijderen. ****

§  \ **Ja: **\ Door op ja te klikken, wordt het bestand verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door op annuleren te klikken, wordt de betreffende
actie verwijderd. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
