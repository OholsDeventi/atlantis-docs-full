.. raw:: html

   <div class="WordSection1">

Hoe kan ik presentatieprofielen toevoegen of bewerken?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Klik op plus (**+)** om toe te voegen of op het
potlood-pictogram om velden van presentatieprofielen te bewerken. ****

.. raw:: html

   </div>

-  **Naam:**\  De naam van het multimedia-object moet hier worden
   opgegeven. Dit veld is verplicht.

-  **Standaard:**\  Markeer of het presentatieprofiel wordt weergegeven
   als standaard presentatieprofiel en geen profile is toegewezen als
   een metagegevens-model.

-  **Afbeeldingen – Bestandstype**\ : Een uitbreiding zoals JPG, PNG,
   enz. waarin het beeld wordt gepresenteerd op internet, intranet of
   wanneer een pictogram/ miniatuur voor het beeld wordt gepresenteerd.

-  **Afbeeldingen – kwaliteit**\ : De kwaliteit van de afbeeldingen (in
   % voor JPEG, of een nummer voor PNG) wat betreft kwaliteit/
   compressieverhouding of een compressiefactor. Het hangt af van het
   gekozen bestandstype. Het veld is verplicht.

-  **Afbeeldingen – breedte**\ : De maximale breedte in pixels voor
   presentatie. Het beeld wordt gepresenteerd met betrekking tot de
   aspect ratio's. Het veld is verplicht.

-  **Afbeeldingen – hoogte**\ : De maximale hoogte in pixels voor
   presentatie. Het beeld wordt gepresenteerd met betrekking tot de
   aspect ratio's. Het veld is verplicht.

-  **Afbeeldingen – DPI**\ : Het maximumaantal nummers of punten per
   inch voor presentatie. Het beeld wordt gepresenteerd met betrekking
   tot de aspect ratio's. Het veld is verplicht.

 

**Watermerk**\ :

Bij het weergeven van afbeeldingen kan een watermerk worden geplaatst.

• **Afbeelding**: hiermee kunt u door afbeeldingen in de lokale computer
bladeren en instellen als watermerk voor de afbeelding.

• **Watermerk plaatsen**: Dit veld specificeert de positie waar een
watermerk over het gewenste beeld moet worden geplaatst. Bijvoorbeeld -
Centrum.

• **Doorzichtigheid van watermerk**: Specificeer het niveau van de
transparantie van het watermerk die over de afbeelding moet worden
weergegeven.

 

 

 

 

**Download resoluties:**

In verschillende situaties is het downloaden van (of een geselecteerd
onderdeel van) afbeeldingen van toepassing. Met downloadresoluties
kunnen verschillende download resoluties toegewezen worden voor
verschillende situaties.

**Toevoegen of bewerken**

Deze selectievakjes kunnen gebruikt worden in combinatie met elkaar,
waardoor downloadresoluties in verschillende situaties toepasbaar zijn.

·         Klik op de toevoegknop (**+)** om downloadresolutievelden toe
te voegen. ****

-  **Downloadresoluties:**\  Gebruikers kunnen selecteren op een
   specifieke downloadresoluties van het uitvouw-menu.

-  **Internet:**\  Vink dit aan als de downloadresolutie van toepassing
   is op het downloaden van afbeeldingen op internet.

-  **Intranet:**\  Vink dit aan als de downloadresolutie van toepassing
   is op het downloaden van afbeeldingen op intranet.

-  **Beperkt:**\  Vink dit aan vakje als de downloadresolutie van
   toepassing is om een beperkte presentatie van de afbeelding te
   downloaden.

-  **Onbeperkt:**\  Vink dit vakje aan als de downloadresolutie van
   toepassing is om een onbeperkte presentatie van de afbeelding te
   downloaden.

·         \ **Selectie: **\ Vink dit vak aan als de downloadresolutie
van toepassing is op een selectie van een afbeelding.

-  **Download:**\  Vink dit vak aan als de download van een afbeelding
   voor de gebruiker beschikbaar is.

 

 

-  **Reset Button**\ 

Door te klikken op de **reset-**\ knop, keert u terug naar de laatst
opgeslagen positie.

 

·         \ **Opslaan**

§  Door te klikken op **opslaan**, worden de gemaakte wijzigingen
opgeslagen.

§  Zodra de bestanden opgeslagen zijn, verschijnen het potlood-pictogram
en het vuilnisbak-pictogram.

 

·         \ **Wijzigen **\ 

§  Klik op het **potlood-pictogram** om de datum en beide factuurnummers
te bewerken.

 

·         \ **Verwijderen**\ 

§  Klik op het **vuilnisbak-**\ pictogram om nieuwe bestanden/ een rij
bestanden te verwijderen.

§  \ **Ja: **\ Door te klikken op ja, wordt het bestand verwijderd.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de
desbetreffende actie verwijderd.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
