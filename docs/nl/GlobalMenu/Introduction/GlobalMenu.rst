.. raw:: html

   <div class="WordSection1">

**Global Menu**\ 

·         Het global menu pictogram vindt de gebruiker helemaal rechts
op het scherm. Door op het pictogram te klikken, kan de gebruiker
verschillende functies zien die worden aangeboden in de Atlantis
applicatie. Met het Global menu kan de gebruiker gebruik maken van de
verschillende items die in het menu staan en niet gelinkt zijn aan het
Dashboard.

·         Het global menu is verdeeld in vijf onderdelen:

**1.      Algemeen**

¨      Downloadresolutie

¨      Auteursrechthouders

¨      Presentatieprofielen

¨      Annotatietypes

¨      Mediatypes

¨      Verpakkingen

¨      Tabbladen

¨      Segmenten

¨      Open data

¨      Vertalingen

 

**2.      Thema configuratie **

¨      Thema’s

 

**3.      Webshop**

¨      Beheer

¨      Kortingen

¨      Reproducties

¨      Publicaties

 

**4.      Thesaurus**

¨      Configuratie

¨      Browser

****

 

**5.      Indexen**

¨      Indextypes

¨      Indexen

**Welke onderdelen voor de gebruiker beschikbaar zijn, hangt af van de
licenties en/of de gebruikersrechten.  **\ 

.. raw:: html

   </div>
