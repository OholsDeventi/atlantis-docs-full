.. raw:: html

   <div class="WordSection1">

**Kortingen**\ 

Kortingen zijn bedoeld voor de verkoop van ofwel de reproductie van
artikelen of voor publicatie. De korting kan een percentage zijn of een
vast bedrag, eventueel afhankelijk van het aantal bestelde artikelen.

**Gereedschaps-tip:**\  Om het gebruik van clickables te vereenvoudigen
kan de gebruiker de muis boven de knoppen houden om de namen van de
knoppen te laten zien.

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Toevoegen-knop: **\ Klik op de + knop om een boekhouding
van kortingen toe te voegen.

.. raw:: html

   </div>

·         \ **Bewerken-knop: **\ Klik op de **Potlood**-knop om de
boekhouding van de kortingen aan te passen.\ ****

·         \ **Verwijderen-knop: **\ Klik op het **Prullenbak-**\ icoon
om boekhouding van de kortingen te verwijderen. ****

§  \ **Ja: **\ Klik op Ja om de boekhouding van de korting te
verwijderen.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Klik op Annuleren om de huidige actie te staken.
\ ****

.. raw:: html

   </div>

 

**Toevoegen**\ ** of Bewerken**

·         Klik op de + of Potlood knop om de boekhouding van de
kortingen aan te passen of toe te voegen.\ ****

·         \ **Reproductie categorie: **\ In dit veld dient de gebruiker
de categorie te kiezen die gereproduceerd moet worden waarop de korting
van toepassing is. De categorie kan gekozen worden uit een vooraf
bepaalde keuzelijst.

·         \ **Van:**\  In dit veld moet het aantal artikelen worden
ingevoerd vanaf wanneer de korting toegepast moet worden.

·         \ **Tot:**\  In dit veld moet het aantal artikelen worden
ingevoerd vanaf wanneer de korting niet meer toegepast moet worden.

·         \ **Prijs Reductie: **\ In dit veld moet het totale bedrag
worden ingevoerd dat als korting in mindering kan worden gebracht.

·         \ **Reductiepercentage:**\  In dit veld kan de totale korting
als percentage worden ingevoerd.

**Reset**

•        Door op **Reset** te klikken gaat u terug naar de laatst
opgeslagen positie.

**Opslaan**\ ****

·         Deze knop slaat alle wijzigingen op.

·         Zodra de boekhouding is opgeslagen, verschijnen de Potlood- en
Prullenbak-knop.\ ****

**Bewerken**\ ****

•        Klik op de **Potlood**-knop om de korting-boekhouding te
bewerken. ****

**Verwijderen**\ ****

•        Klik op de **Prullenbak**-knop om een boekhouding te
verwijderen.\ ****

•        \ **Ja: **\ Klik op Ja om de boekhouding te verwijderen. \ ****

•        \ **Annuleren**\ : Klik op Annuleren om de huidige actie te
staken. \ ****

 

 

****

 

 

.. raw:: html

   </div>
