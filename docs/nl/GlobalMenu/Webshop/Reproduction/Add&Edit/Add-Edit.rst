.. raw:: html

   <div class="WordSection1">

**Toevoegen**

·         Klik op de **+** knop om bestanden voor reproductie toe te
voegen. ****

·         \ **Betekenis:**\  In dit veld moet de naam van de
reproductiecategorie worden ingevuld.

·         \ **Videoformat: **\ Het gewenste videoformaat voor een kopie
van een video kan hier worden geselecteerd.

·         \ **Bedrag:**\  In dit veld moet de prijs van de reproductie
worden ingegeven. De gebruiker kan ook het bedrag verhogen of verlagen
met behulp van de stepper.

·         \ **Vul metamodellen in: **\ Vink het vakje aan als er een
metadata-model nodig is voor de betreffende categorie.

·         \ **Is Digitaal:**\  Vink dit vakje aan, als de entiteit
digitaal is.

·         \ **Is Highres:**\  Vink dit vakje aan als het gaat om een
reproductie in een hoge resolutie.

·         \ **Direct Download:**\  Vink dit vakje aan als de reproductie
beschikbaar moet zijn voor directe downloads. \ ****

**Reset **

·         Door te klikken op **reset,** wordt er een bevestigingsvenster
weergegeven. ****

·         \ **Ja**\ : door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

****

 

**Opslaan**

Deze knop slaat alle nieuwe en niet-opgeslagen wijzigingen op.

****

 

.. raw:: html

   </div>
