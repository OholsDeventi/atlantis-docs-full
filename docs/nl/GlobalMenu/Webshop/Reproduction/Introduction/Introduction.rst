.. raw:: html

   <div class="WordSection1">

**Reproductie**

Deze sectie bevat de reproductiecategorieën. Er kunnen verschillende
soorten reproducties op bestelling gemaakt worden. Dit scherm toont de
beschikbare soorten reproducties die een gebruiker toe kan voegen aan
zijn of haar winkelwagen.

Het categoriseert de manieren waarop een artikel gereproduceerd kan
worden, bijvoorbeeld: directe download, een gekleurde foto van een
willekeurig artikel, een kopie van een willekeurig artikel, etc. De
categorieën worden weergegeven aan de linkerzijde van het
webshop-scherm. De gebruiker kan categorieën toevoegen, bewerken of
verwijderen met de **+**, **Potlood** of **Prullenbak** knoppen.

 

·         \ **Minimaliseer-knop:**\  Middels deze functie kan de
gebruiker het menu samenvouwen. Als de gebruiker op de linker pijl drukt
zal het menu zich samenvouwen. Als het menu samengevouwen is, zal de
pijl naar rechts draaien. Om het venster weer uit te vouwen, moet de
gebruiker op de rechterpijl klikken.

`·         \ **Vastpin-knop: ** <>`__\ Zoals de naam suggereert, wordt
de vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. ****

·         \ **Toevoegen:**\  Door op + te klikken, kan de gebruiker
publicatiebestanden toevoegen.

·         \ **Zoeken:**\  In dit veld kan de gebruiker
publicatiecategorieën uit de lijst zoeken.\ ****

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de bewerken publicaties bewerken.\ ****

·         \ **Metagegevens-knop: **\ Druk op het **M-**\ pictogram om
metagegevens-modellen toe te wijzen.\ ****

·         \ **Verwijderen: **\ Door te klikken op het
**prullenbak-**\ pictogram, kunnen publicatiebestanden verwijderd
worden. ****

§  \ **Ja: **\ door te klikken op ja, wordt het bestand verwijderd\ ****

§  \ **Annuleren: **\ door te klikken op annuleren, wordt de betreffende
actie beëindigd.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ ****

 

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
