.. raw:: html

   <div class="WordSection1">

**Metagegevens-knop: **\ Door te klikken op **M,** kan de gebruiker
metagegevens-modellen aan reproductiecategorieën toeschrijven. Selecteer
het metagegevens-model uit de vervolgkeuzelijst. De gebruiker kan ook
toegeschreven metagegevens verwijderen door op de verwijderknop in de
chip te drukken.

 

**Reset **

·         Door te klikken op **reset,** wordt het bevestigingsvenster
weergegeven. ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Opslaan**

Met deze knop kunnen nieuwe en niet-opgeslagen bestanden worden
opgeslagen.

 

.. raw:: html

   </div>
