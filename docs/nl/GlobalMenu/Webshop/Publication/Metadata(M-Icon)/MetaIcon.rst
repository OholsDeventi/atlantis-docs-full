.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;background:white">

**Metagegevens-knop: **\ Door te klikken op het **M-**\ icoon, kan de
gebruiker metagegevens-modellen toewijzen aan de reproductiecategorie.
Selecteer het metagegevens-model uit de vervolgkeuzelijst. De gebruiker
kan ook toegewezen metagegevens verwijderen door te klikken op de
verwijder-knop in de chip.

**Reset **

.. raw:: html

   </div>

·         Door te klikken op **Reset,** wordt een bevestigingsvenster
geopend:\ ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

****

 

.. raw:: html

   </div>

****

 

`**Afsluiten** <>`__

§  Door te klikken op afsluiten, verschijnt er een bevestigingsvenster.
****

§  \ **Ja: **Door te klikken op ja, wordt de pagina afgesloten\ ****

§  \ **Annuleren: **Door te klikken op annuleren, blijft u op dezelfde
pagina\ ****

 

.. raw:: html

   </div>
