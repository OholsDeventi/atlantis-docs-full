.. raw:: html

   <div class="WordSection1">

**Publicaties **

Voor publicaties kunnen reproducties worden besteld. De
publicatiecategorie is beschikbaar in de weergegeven lijst in het linker
venster op de pagina.  De gebruiker kan middels verschillende knoppen:
+, **potlood** en **prullenbak** bestanden toevoegen, bewerken of
verwijderen.

Daarnaast worden ook de publicatiekosten weergegeven. Hiervoor moet de
gebruiker klikken op de publicatiecategorie. Een venster met
publicatiekosten wordt dan geopend. In dit venster kan de gebruiker
bestanden toevoegen, bewerken of verwijderen.

 

`·         \ **Vastpin-knop: ** <>`__\ Zoals de naam suggereert, wordt
de vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. ****

·         \ **Minimaliseer-knop:**\  Middels deze functie kan de
gebruiker het menu samenvouwen. Als de gebruiker op de linker pijl drukt
zal het menu zich samenvouwen. Als het menu samengevouwen is, zal de
pijl naar rechts draaien. Om het venster weer uit te vouwen, moet de
gebruiker op de rechterpijl klikken.

·         \ **Toevoegen:**\  Door op + te klikken, kan de gebruiker
publicatiebestanden toevoegen.

·         \ **Zoeken:**\  In dit veld kan de gebruiker
publicatiecategorieën uit de lijst zoeken.\ ****

·         \ **Bewerkknop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de bewerken publicaties bewerken.\ ****

·         \ **Kosten: **\ Klik op de CC knop voor de kosten van een
publicatie.\ ****

·         \ **Metagegevens-knop: **\ Druk op het **M-**\ pictogram om
metagegevens-modellen toe te wijzen.\ ****

·         \ **Verwijderen: **\ Door te klikken op het
**prullenbak-**\ pictogram, kunnen publicatiebestanden verwijderd
worden. ****

§  \ **Ja: **\ door te klikken op ja, wordt het bestand verwijderd\ ****

§  \ **Annuleren: **\ door te klikken op annuleren, wordt de betreffende
actie beëindigd.

.. raw:: html

   </div>
