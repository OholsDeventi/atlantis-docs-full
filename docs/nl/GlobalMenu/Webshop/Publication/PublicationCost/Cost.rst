.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Kosten**

.. raw:: html

   </div>

·         Door te klikken op de CC-knop, worden de publicatiekosten
geopend. De publicatiekosten definiëren de kosten van de
respectievelijke publicatiecategorieën die in de lijst worden
weergegeven. ****

`·         \ **Vastpin-knop: ** <>`__\ Zoals de naam suggereert, wordt
de vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. \ ` <>`__\ ****

·         \ **Afsluiten**

§  Door te klikken op afsluiten, verschijnt er een bevestigingsvenster.
****

§  \ **Ja: **Door te klikken op ja, wordt de pagina afgesloten\ ****

§  \ **Annuleren: **Door te klikken op annuleren, blijft u op dezelfde
pagina\ ****

·         \ **Zoeken:**\  In dit veld kan de gebruiker zoeken op
publicatiekosten uit de lijst. ****

**Toevoegen **

·         Door te klikken op +, kan de gebruiker publicatiekosten
toevoegen

·         \ **Bedrag:**\  De kosten voor een publicatie hangen af van
het aantal publicaties en hoe vaak deze publicaties gepubliceerd worden.

·         \ **Minimaal bedrag:**\  Dit veld geeft het minimale bedrag
voor publiceren weer.

·         \ **Van:**\  In dit veld wordt aangegeven vanaf welk aantal
publicaties wat de kosten per item zullen zijn.

·         \ **Tot:**\  Dit is het maximaal aantal publicaties waar het
bedrag voor geldt.

·         \ **Blokmaat:**\  De grootte van het bestand. Dit wordt
gebruikt voor videoreproducties.

·         \ **Elke eenheid:**\  Dit selectievakje geeft een indicatie
van het bedrag per eenheid van een product.

`**Reset ** <>`__

•        Door te klikken op **reset**, wordt u teruggebracht naar de
laatst opgeslagen positie.

` <>`__\ `**Opslaan** <>`__

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

·         Als het bestand is opgeslagen, worden het potlood-pictogram en
het prullenbak-pictogram weergegeven. ****

`**Bewerken** <>`__

•        Door te klikken het **potlood-**\ pictogram, kan de gebruiker
de publicatiekosten bewerken. \ ****

**Verwijderen**

•        Door op het **prullenbak-**\ pictogram te klikken, kunnen
bestanden worden verwijderd. ****

•        \ **Ja: **\ door te klikken op ja, wordt het bestand
verwijderd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

•        \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
