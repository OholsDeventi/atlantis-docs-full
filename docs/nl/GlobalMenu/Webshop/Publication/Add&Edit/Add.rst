.. raw:: html

   <div class="WordSection1">

**Toevoegen**\ ****

·         Klik op + om nieuwe bestanden toe te voegen.

·         \ **Omschrijving**\ **:**\  Voer hier de naam van de
publicatiecategorie in.

**Reset **

·         Door te klikken op **reset**, wordt ere en bevestigingsvenster
weergegeven. ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Opslaan**\ ****

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

 

.. raw:: html

   </div>
