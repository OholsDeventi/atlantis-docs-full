.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

.. raw:: html

   </div>

•         \ **Naam**\ : Geef de naam van de organisatie op waarvoor de
beschikbare tijd is gescheiden.

•         \ **Starttijd**\ : Voer hier de openingstijden van de
organisatie in.

•         \ **Eindtijd**\ : Voer hier de sluittijd van de organisatie
in.

•         \ **Hele dag:**\  Als er een wijziging in het rooster is, kan
de beheerder het vak aanvinken, waarmee wordt aangegeven dat de winkel
de gehele dag geopend is.

**Reset **

·          Door op reset te klikken, wordt u teruggebracht naar de
laatst opgeslagen positie. ****

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
veranderingen opgeslagen. ****

****

 

**Afsluiten**

§  Door op afsluiten te klikken, wordt er een bevestigingsvenster
weergegeven. ****

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
