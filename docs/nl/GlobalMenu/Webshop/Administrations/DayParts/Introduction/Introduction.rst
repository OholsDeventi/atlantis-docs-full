.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Dagdelen**

.. raw:: html

   </div>

Na het vastleggen van de openings- en sluittijd zal dit scherm de
dagdelen weergeven waarop de organisatie open is. Bijvoorbeeld, wanneer
de organisatie een halve dag beschikbaar is.

·         \ **Pinvast-knop: **\ Zoals de naam suggereert, kan de
pinvast-knop gebruikt worden om pagina’s vast te pinnen. De vastgepinde
pagina’s worden opgeslagen in een lijst die te vinden is in de kop van
Atlantis Dashboard. ****

·         \ **Toevoegen:**\  Door te klikken op +, kunnen
dagdeelbestanden worden toegevoegd. \ ****

·         \ **Zoeken:**\  Met dit veld kan de gebruiker zoeken op
dagdelen in de lijst\ ****

·         \ **Bewerken:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker dagdeel-bestanden aanpassen.
\ ****

·         \ **Verwijderen: **\ Door te klikken op het
**prullenbak-**\ pictogram, kan de gebruiker dagdeel-bestanden
verwijderen.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Ja: **\ Door op ja te klikken, worden de bestanden verwijderd

§  \ **Annuleren: **\ Door op annuleren te klikken, wordt de betreffende
actie beëindigd.

.. raw:: html

   </div>

.. raw:: html

   </div>
