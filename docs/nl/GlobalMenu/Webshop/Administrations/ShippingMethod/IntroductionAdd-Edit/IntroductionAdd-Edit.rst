.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Verzendmethoden**

.. raw:: html

   </div>

Dit scherm geeft de verzendmethoden en -kosten van een te verzenden item
weer. Deze sectie informeert de gebruiker over hoe een item/artikel
verzonden wordt aan de gebruiker en daarnaast laat het ook zien welke
kosten daaraan zijn verbonden. De beheerder komt terecht in een lijst
met items en kan in deze lijst zoeken naar specifieke
verzendmethoden.\ ****

De beheerder kan zowel nieuwe toeslagen toevoegen (+), bewerken
(**potlood-**\ pictogram) of ze verwijderen
(**prullenbak-**\ pictogram). Voor toevoegen of bewerken zijn er de
volgende opties:

·         \ **Vastpin-knop: **\ Zoals de naam suggereert, wordt de
vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. ****

·         \ **Toevoeg-knop:**\  Door te klikken op +, kan de gebruiker
een verzendmethode toevoegen.

·         \ **Zoeken:**\  In dit veld kan de gebruiker zoeken in de
lijst naar verzendmethoden.\ ****

·         \ **Bewerk-knop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de verzendmethoden
bewerken.\ ****

**Verwijderen**

·         Klik op het **prullenbak-**\ pictogram om een bestand te
verwijderen. ****

·         \ **Ja: **\ Door te klikken op ja, wordt het bestand
verwijderd. ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

**Toevoegen**

•        Klik op + om nieuwe bestanden toe te voegen.

•        \ **Omschrijving: **\ Dit veld geeft de omschrijving van de
verzending weer, bijvoorbeeld of het gaat om een digital item en op wat
voor manier deze verzonden zou moeten worden, via email bijvoorbeeld.

•        \ **Bedrag:**\  Dit veld geeft de totale kosten van een te
verzenden item weer.

**Reset **

•        Door te klikken op **reset**, wordt u teruggebracht naar de
laatst opgeslagen positie.

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

Als het bestand is opgeslagen, worden het potlood-pictogram en het
prullenbak-pictogram weergegeven ****

**Bewerken**

•        Door te klikken het **potlood-**\ pictogram, kan de gebruiker
de omschrijving en de kosten bewerken. ****

****

 

**Afsluiten**

§  Door te klikken op afsluiten, verschijnt er een bevestigingsvenster.
****

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
