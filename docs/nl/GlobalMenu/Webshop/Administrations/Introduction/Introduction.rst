.. raw:: html

   <div class="WordSection1">

**Webshop**\ 

De webshop biedt een aantal functies die de gebruiker in staat te
stellen reproducties te bestellen, documenten ter inzage op te vragen en
om aanvragen te scannen (indien beschikbaar volgens de licentie). Door
een winkelwagentje worden deze verzoeken verzameld en daarvandaan
behandeld.

Als er financiële transacties zijn, kan dit handmatig of via
elektronische betaling worden behandeld. Er zijn ook diverse opties
beschikbaar over verzending, kortingen, open dagen, vakanties, enz.

De webshop is te vinden in het Global

**Gereedschaps-tip**\ : Om opties makkelijker te begrijpen, kan de
gebruiker met de muis over de knoppen zweven. Hiermee wordt de naam van
de knoppen weergegeven.

 

**Administratie**

The webshop administratie verbindt een set opties voor de webshop.
Verschillende administraties kunnen worden aangemaakt, zodat
verschillende opties gelinkt kunnen worden aan metagegevens-modellen en
omgevingen, als de webshop door derde partijen gebruikt worden. Welke
opties verbonden zijn, kan gezien worden door de muis over een
administratie te laten zweven. Hierdoor zal ere en menu zichtbaar worden
die laat zien welke opties er beschikbaar zijn voor de administratie.  

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pinvast-knop:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s vast te
pinnen. De vastgepinde pagina’s worden opgeslagen in een lijst die te
vinden is in het Atlantis Dashboard.

.. raw:: html

   </div>

 

·         \ **Minimaliseer-knop:**\  Met deze knop kan de gebruiker een
venster samenvouwen. Om het venster samen te vouwen, moet de gebruiker
op de linker pijl drukken. Zodra het venter is samengevouwen, verandert
de linker pijl van richting (deze draait naar rechts). Als de gebruiker
het venter weer wil uitvouwen, klikt hij op de rechterpijl-knop.

 

·         \ **Toevoegen: **\ Door op + te klikken, kan de gebruiker
webshop-bestanden toevoegen

 

·         \ **Zoeken:**\  Hiermee kan de gebruiker zoeken op
webshop-bestanden uit de lijst\ ****

·         \ **Bewerken: **\ Klik op het **potlood-**\ pictogram om de
webshop-bestanden te bewerken\ ****

·         \ **Verwijderen: **\ Door te klikken op het
**prullenbak-**\ pictogram, kunnen webshop-bestanden worden
verwijderd.\ ****

§  \ **Ja: **\ Door te klikken op ja, wordt het bestand verwijderd\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Door op annuleren te drukken, wordt de betreffende
actie beëindigd. ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
