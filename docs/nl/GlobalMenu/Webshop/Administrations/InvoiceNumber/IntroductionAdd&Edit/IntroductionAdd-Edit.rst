.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Factuurnummer **

.. raw:: html

   </div>

Dit scherm geeft een lijst met factuurnummer weer, met de datum waarop
de factuur is aangemaakt. Specifieke factuurnummers kunnen in dit scherm
opgezocht worden.

De beheerder kan zowel nieuwe factuurnummers toevoegen (d.m.v. +), als
ook factuurnummers bewerken (met het **potlood-**\ pictogram) of de
factuurnummers verwijderen (met het **prullenbak-**\ pictogram). 

 

`·         \ **Vastpin-knop: ** <>`__\ Zoals de naam suggereert, wordt
de vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. ****

`·         \ **Toevoeg-knop:** <>`__\  Door te klikken op +, kan de
gebruiker een factuurnummer toevoegen.

`·         \ **Zoeken:** <>`__\  In dit veld kan de gebruiker zoeken
naar factuurnummers.\ ****

·         \ **Bewerk-knop:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker factuurnummers bewerken.\ ****

**Verwijderen**

·         Klik op het **prullenbak-**\ pictogram om een bestand te
verwijderen. ****

` <>`__\ `·         \ **Ja: ** <>`__\ Door te klikken op ja, wordt het
bestand verwijderd. ****

·         \ **Annuleren**: Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

****

 

`**Toevoegen** <>`__

•        Klik op + om nieuwe bestanden toe te voegen.

•        \ **Startdatum: **\ De datumprikker kan gebruikt worden als een
factuurnummer gemaakt of verstuurd wordt.

•        \ **Eerste factuurnummer:**\  Voer hier het nummer van het
eerste factuurnummer in.

•        \ **Laatste factuurnummer:**\  Hier wordt het laatste
factuurnummer weergegeven. Dit veld is een alleen-lezen veld.

` <>`__\ `**Reset ** <>`__

•        Door te klikken op **reset**, wordt u teruggebracht naar de
laatst opgeslagen positie.

` <>`__\ ` <>`__\ `**Opslaan** <>`__

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

·         Als het bestand is opgeslagen, worden het potlood-pictogram en
het prullenbak-pictogram weergegeven. ****

`**Bewerken** <>`__

•        Door te klikken het **potlood-**\ pictogram, kan de datum van
het eerste factuurnummer worden bewerkt. ****

`**** <>`__

 

`**Afsluiten** <>`__

§  Door te klikken op afsluiten, verschijnt er een bevestigingsvenster.
****

§  \ **Ja: **Door te klikken op ja, wordt de pagina afgesloten\ ****

§  \ **Annuleren: **Door te klikken op annuleren, blijft u op dezelfde
pagina\ ****

 

.. raw:: html

   </div>
