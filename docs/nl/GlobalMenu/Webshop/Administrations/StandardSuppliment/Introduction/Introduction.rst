.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Standaard supplementen **

.. raw:: html

   </div>

`In dit scherm worden de bedragen en omschrijvingen van items in
verschillende situaties weergegeven, die toegevoegd zijn aan het
winkelwagentje. De beheerder kan zowel nieuwe items toevoegen (+),
bewerken (**potlood-**\ pictogram) of de items verwijderen
(**prullenbak-**\ pictogram).
 <https://bitbucket.org/rkdahiya/atlantis-help-manual/src/c8baccc755f234e98ee9e4a01a3350820441c9c4/Webshop/Standard-Suppliments.md?at=master&fileviewer=file-view-default>`__

**Vastpin-knop:** Zoals de naam suggereert, wordt de vastpin-knop
gebruikt om pagina’s vast te pinnen. De vastgepinde pagina’s worden in
een lijst opgeslagen, die te vinden is in het Atlantis Dashboard.

****

 

**Toevoeg-knop:**\  Door te klikken op +, kan de gebruiker standaard
items aan de lijst toevoegen.

**Zoeken:**\  In dit veld kan de gebruiker zoeken naar
standaarditem-bestanden zoeken.\ ****

`**Bewerken: ** <>`__\ door te klikken het **potlood-**\ pictogram, kan
de gebruiker het standaard item bewerken. ****

****

 

**Verwijderen: **\ Door te klikken op het **prullenbak-**\ pictogram,
wordt het geselecteerde item verwijderd. ****

§  \ **Ja: **\ door te klikken op ja, wordt het bestand verwijderd. ****

§  \ **Annuleren: **\ door te klikken op annuleren, wordt de betreffende
actie beëindigd.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

 

.. raw:: html

   </div>

.. raw:: html

   </div>
