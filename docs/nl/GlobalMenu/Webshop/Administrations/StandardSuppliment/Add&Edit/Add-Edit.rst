.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

.. raw:: html

   </div>

•        \ **Omschrijving:**\  In dit veld kan de gebruiker de
supplementennaam en de beschrijving daarvan toevoegen.

•        \ **Bedrag:**\  Dit veld geeft het bedrag van de omschreven
items weer. Het bedrag kan ook variëren met behulp van de increment- en
decrement-knop.

•        \ **Is digitaal:**\  Het vinkje op dit veld informeert of het
supplement moet worden toegepast op items die als digitaal zijn
gemarkeerd

•        \ **Per item:**\  Dit veld geeft weer of het supplement per
artikel moet worden toegepast in de winkelwagen of als gehele bestelling
geplaatst moet worden.

•        \ **Is vast aangevuld:**\  Dit veld geeft aan of de prijs vast
staat of niet

**Reset **

•        Door te klikken op **reset**, wordt u teruggebracht naar de
laatst opgeslagen positie.

`**Opslaan** <>`__

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

·         Als het bestand is opgeslagen, worden het potlood-pictogram en
het prullenbak-pictogram weergegeven. ****

`**Bewerken** <>`__

•        Door te klikken het **potlood-**\ pictogram, kan de datum van
het eerste factuurnummer worden bewerkt. ****

****

 

`**Afsluiten** <>`__

§  Door te klikken op afsluiten, verschijnt er een bevestigingsvenster.
****

§  \ **Ja: **Door te klikken op ja, wordt de pagina afgesloten\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

**Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
