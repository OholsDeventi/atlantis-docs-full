.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

.. raw:: html

   </div>

•       \ **Omschrijving**\ : De naam van de feestdag moet worden
ingevoerd in dit veld

•       \ **Datumprikker**\ : De datumprikker geeft de keuze van de
feestdagen weer.

 

**Reset **

·         Door te klikken op **Reset,** wordt het bevestigingsvenster
weergegeven\ ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie. ****

·         \ **Cancel**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd ****

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

·         Als een bestand opgeslagen is, worden het potlood-pictogram en
prullenbak-pictogram weergegeven. ****

**Bewerken**

•        Door te klikken op het **potlood-**\ pictogram, kan de
gebruiker de omschrijving en/of de datumprikker wijzigen ****

****

 

**Afsluiten**

§  Door te klikken op **afsluiten,** wordt er een bevestigingsvenster
weergegeven.\ ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina\ ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
