.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Holiday**

.. raw:: html

   </div>

Op het scherm wordt een lijst met vakantiedagen/feestdagen weergegeven.
De gebruiker kan ook op specifieke dagen zoeken. De beheerder kan ofwel
nieuwe data toevoegen (+) of data wijzigen (**potlood-**\ pictogram).
Feestdagen kunnen ook verwijderd worden door gebruik te maken van het
**prullenbak-**\ pictogram.

·         \ `**Vastpin-knop:
** <../../../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s vast te
pinnen. De vastgepinde pagina’s worden in een lijst opgeslagen die te
vinden is in de kop van Atlantis Dashboard. \ ****

·         \ **Toevoegen:**\  Door te klikken op +, kunnen
vakantie-bestanden worden toegevoegd\ ****

·         \ **Zoeken:**\  In dit veld kan de gebruiker zoeken op
vakanties uit de lijst\ ****

·         \ **Bewerken:**\  Door te klikken op het
**potlood-**\ pictogram, kan de gebruiker de vakanties bewerken\ ****

**Verwijderen**

·         Door te klikken op het **prullenbak-**\ pictogram, kunnen
bestanden of een rij bestanden worden verwijderd. ****

·         \ **Ja: **\ Door te klikken op ja, wordt het bestand
verwijderd.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
