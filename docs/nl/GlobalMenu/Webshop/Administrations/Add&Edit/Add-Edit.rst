.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen of bewerken **

.. raw:: html

   </div>

·         Klik op **+ of** op het **potlood-**\ pictogram om een nieuwe
webshop toe te voegen en de webshop met een metagegevens-model te
linken\ ****

·         \ **Applicatienaam:**\  Voer hier de naam van de applicatie in

·         \ **Ontvanger bestelling: **\ het emailadres van de ontvanger
van de bestelling is verplicht in dit veld. 

·         \ **Verzender bestelling:**\  Het e-mailadres van de afzender
van het antwoord op de bestelling is verplicht in dit veld.

·         \ **Onderwerp bestelling:**\  Het onderwerp van de bestelling
moet in dit veld worden ingevoerd.

·         \ **Applicatie ontvanger:**\  Het emailadres van de ontvanger
van de applicatie moet wegens inspectie verplicht worden ingevuld hier.

·         \ **Applicatie verzender:**\  Het emailadres van de verzender
van de applicatie moet wegens inspectie verplicht worden ingevuld hier.

·         \ **Onderwerp van de applicatie:**\  Het onderwerp betreffende
het antwoord van de aanvraag moet hier worden ingevuld.

·         \ **Onderwerp scanning op aanvraag:**\  Onderwerp van het
emailbericht van het antwoord op het verzoek om scannen op aanvraag is
verplicht in dit veld.

·         \ **Uit scannen op aanvraag:**\  Het e-mailadres van de
afzender van het antwoord op het verzoek om scannen op aanvraag is
verplicht in dit veld.

·         \ **Maximum aanvraag:**\  Het maximumaantal items dat kan
worden aangevraagd moet in dit veld worden aangegeven.

·         \ **Maximum Order:**\  Maximumaantal items om te bestellen
moet hier worden aangegeven.

**Reset **

·         Door op **reset** te klikken, wordt het bevestigingsvenster
weergegeven. ****

·         \ **Ja**\ : Door op ja te klikken, wordt u teruggebracht naar
de laatst opgeslagen positie. ****

·         \ **Annuleren**\ : Door op annuleren te klikken, wordt de
betreffende actie beëindigd. ****

**Opslaan**

·         Met opslaan kan de gebruiker alle nieuwe en niet-opgeslagen
wijzigingen opslaan. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

****

 

.. raw:: html

   </div>

****

 

****

 

****

 

****

 

****

 

****

 

**Afsluiten**

•        Door op afsluiten te drukken, wordt er een bevestigingsvenster
weergegeven: ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten.\ ****

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina.\ ****

 

.. raw:: html

   </div>
