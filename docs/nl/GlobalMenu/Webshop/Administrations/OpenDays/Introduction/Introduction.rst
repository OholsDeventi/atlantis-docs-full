.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Dagen open**

.. raw:: html

   </div>

Dit scherm geeft de dagen weer van de komende week. Door het
selectievakje aan te vinken van de beschikbare dagen, kan de beheerder
laten zien op welke dagen de organisatie open is voor verzoekinspecties.

·         \ **Vastpin-knop: **\ Zoals de naam suggereert, wordt de
vastpin-knop gebruikt om pagina’s vast te pinnen. De vastgepinde
pagina’s worden in een lijst opgeslagen, die te vinden is in het
Atlantis Dashboard. ****

****

 

•        Vink aan wanneer de leesruimte open is

 

**Reset **

Door te klikken op **reset**, wordt u teruggebracht naar de laatst
opgeslagen positie.

 

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen.\ ****

****

 

****

 

**Afsluiten**

·         \ **Ja: **\ Door te klikken op ja, wordt de pagina
afgesloten\ ****

·         \ **Annuleren: **\ Door te klikken op annuleren, blijft u op
dezelfde pagina\ ****

 

.. raw:: html

   </div>
