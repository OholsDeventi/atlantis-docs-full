.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Termen-knop**\ : Door op het Ⓣ icoon te klikken wordt er een formulier
geopend aan de rechterzijde van het scherm. Het formulier kan vooraf
gedefinieerde termen uit de lijst van Tabellen en Kolommen bevatten maar
de gebruiker kan er ook een nieuwe term maken of een reeds bestaande
term aanpassen.

.. raw:: html

   </div>

·         \ **Label toevoegen:**\  Middels dit veld worden labels
toegevoegd. De gebruiker hoeft enkel de naam van het label in te voeren
en op de Opslaan-knop te drukken.

·         \ **Standaard:**\  De geselecteerde term is de standaard term.
De gebruiker dient op een willekeurig label te klikken en vervolgens een
vinkje plaatsen bij het Standaard-hokje. De kaders van he item worden
groen om aan te geven dat het een Standaard-term is.

·         \ **Sluiten:**\  Klik op de Sluiten-knop om een term die te
zien is in het rechter scherm te verwijderen.

§  \ **Ja: **\ Klik op Ja om de geselecteerde term te verwijderen.
\ ****

§  \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken.\ ****

·         \ **Reset: **\ Door op de Reset-knop te drukken verschijnt er
een bevestigingsscherm. Dit scherm bevat de volgende opties:  \ ****

**Ja**\ : Klik op Ja om terug te keren naar de laatst opgeslagen
instellingen.\ ****

**Annuleren**\ : Klik op Annuleren op de huidige actie af te breken.

****

 

·         \ **Opslaan: **\ Klik op Opslaan om alle wijzigingen op te
slaan.\ ****

 

**Sluiten**\ ****

•        Door op Sluiten te klikken verschijnt er een
bevestigingsscherm.\ ****

§  \ **Ja: **\ Klik op Ja om de pagina af te sluiten. \ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ klik op Annuleren om op dezelfde pagina te
blijven.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
