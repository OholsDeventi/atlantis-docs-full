.. raw:: html

   <div class="WordSection1">

**Lijst**\ ** met termen**

De gebruiker kan naar het huidige scherm navigeren vanuit het **globale
menu** op het dashboard. Door te klikken op de link komt de gebruiker
direct op de pagina “lijst met termen”. Elk item dat als volgt op de
pagina staat: tabel naam.Kolomnaam: ‘ACT\_ACTOR.BRONNEN’ is gekoppeld
aan de bijbehorende kolomnaam uit de database en een metagegevens-model
dat is toegewezen voor het item. Alle geconfigureerde termen op de
pagina worden weergegeven in het linkerdeel van het scherm. De gebruiker
kan op het 'Ⓣ' pictogram van de specifieke selectielijst klikken en de
overeenkomstige termen voor die selectielijst zien in het
rechterdeelvenster.

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Vastpin-knop: **\ Zoals de naam suggereert, wordt de
vastzet-knop gebruikt om pagina’s vast te zetten. De vastgezette
pagina’s worden weergegeven in een lijst die in de kop van het Atlantis
dashboard te vinden is. ****

.. raw:: html

   </div>

****

 

**Zoeken :**\  Dit veld staat de gebruiker toe te zoeken op een
tabelnaam en deze van de lijst te selecteren.

·         \ **Toevoeg-knop:**\  Klik op de + knop om termen toe te
voegen.

·         \ **Verwijderen: **\ Klik op het **prullenbak-pictogram** om
termen te verwijderen. ****

§  \ **Ja: **\ Door te klikken op ja, worden de termen verwijderd
.\ ****

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de betreffende
actie beëindigd. ****

·         \ **Bewerk-knop:**\  Klik op het **potloodpictogram** om
termen te wijzigen (Tabelnaam.Kolomnaam).

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Label-knop: **\ Klik op de **** Ⓣ knop om de geselecteerde
item(s) te labelen (Tabel naam.Kolom naam).

.. raw:: html

   </div>

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen en wijzigen (Klik op ‘+’) voor het volgende:**

.. raw:: html

   </div>

**• Tabel naam – Kolom naam:**\  Voeg een geselecteerd item uit de lijst
toe aan een reeds bestaande lijst van Tabellen & Kolommen uit de
database. De gebruiker kan ook zoeken naar de gerelateerde tabel naam en
deze openen om de corresponderende kolom naam te selecteren.   

**• Niet-bestaande termen toestaan:**\  Met de vlag (vinkje) kunt u de
niet-bestaande items die in tabellen en kolommen worden vermeld,
markeren.

**• Bewaar niet-bestaande:**\  Met de vlag (vinkje) kunt u de lijst met
niet-bestaande items van tabellen en kolommen opslaan in de database.

**• Metagegevens-model:**\  Selecteer de metagegevens-modellen van de
vervolgkeuzelijst.

****

 

**Resetten**

Door te klikken op de **reset-**\ knop, verschijnt het
bevestigingsvenster. Dit bevestigingsvenster bevat de volgende opties:

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positive\ ****

·         \ **Annuleren:**\  Door te klikken op annuleren, wordt de
huidige actie beëidingd. \ ****

**Opslaan**\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Deze knop slaat nieuwe en niet-opgeslagen wijzigingen op.

**Afsluit-knop: **\ Klik op **afsluiten,** om de rij af te sluiten
tijdens het toevoegen en bewerken.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
