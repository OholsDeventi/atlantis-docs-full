.. raw:: html

   <div class="WordSection1">

**Categorieën**

De metagegevens-objecten zijn gerangschikt aan de hand van de
categorieën per metagegevens-model. Categorieën bevatten de lijst met
knooppunten die aan de hand van een boomstructuur zijn gecategoriseerd.
Vanuit deze lijst kan de gebruiker categorieën toevoegen en aanpassen
alsmede de knooppunten in de lijst verplaatsen. Er zijn verschillende
opbouwvormen beschikbaar voor de categorieën, namelijk: een schema
benoemen, toewijzing aan een metagegevens-model, toegang tot de
hiërarchische knooppunten en het becijferen van de categorieën door
code- en scheidings-eigenschapen toe te passen. De gebruiker kan de
categorie sorteren om broers en zussen te ordenen door een sorteernummer
op te geven. De gebruiker kan door met de rechtermuisknop op een
knooppunt te klikken deze bewerken, een nieuw knooppunt toevoegen, de
globale selectie vervangen door geselecteerde documenten (categorieën),
het converteren naar een ander type en de geselecteerde documenten
toevoegen aan de \ `globale
selectie <file:///C:\Users\Bo\Dropbox\Help%20Folder%20Structure-%20Revised-V.02\Categories\Introduction\Global%20Menu.docx>`__\ .
****

 

-  `**Pinvast-knop:
   ** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
   de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s
   vast te pinnen of op te slaan. De opgeslagen pagina’s worden in een
   lijst weergegeven onder de kop van het Atlantis Dashboard. ****

-  **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de gebruiker een
   menu kan samenvouwen. Om het venster samen te vouwen, kan de
   gebruiker op de linkerpijl klikken. Zodra het menu samengevouwen is,
   zal de pijl van richting veranderen (naar rechts). Door op de
   rechterpijl te drukken, wordt het menu weer uitgevouwen.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Zoeken:**\  Met dit veld kan een gebruiker
metagegevens-modellen zoeken. Door op een metagegevens-model te klikken
verschijnen de gerelateerde knooppunten.

.. raw:: html

   </div>

.. raw:: html

   </div>
