.. raw:: html

   <div class="WordSection1">

**Klik op een metadata-model, deze actie brengt de gebruiker naar het
overzichtspaneel: **

****

 

·         \ **Groene markering: **\ de groene kleur markering
symboliseert de knooppunten die gemodereerd zijn.\ ****

·         \ **Grijze markering: **\ De grijze kleur markering
symboliseert de knooppunten die nog niet gemodereerd zijn. ****

·         \ **Uitbreidknop +: **\ Door te klikken op de uitbreidknop,
breiden de knooppunten uit en laat het de totaal aantal knooppunten
zien. De gebruiker kan de categorieën ook slepen en loslaten, achter of
boven de categorieën.

·         \ **Verwijderknop: **\ Door te klikken op het
**prullenbackpictogram,** wordt het verwijder validatie bericht
weergegeven om een categorie te verwijderen.

·         \ **Invouwen deelvensterknop**\ : Klik op de **linker pijl**
knop om het deelvenster in te vouwen. Zodra het deelvenster ingevouwen
is, zal diezelfde pijl naar rechts draaien. Door te klikken op de
**rechter pijl,** zal het menu weer uit worden gevouwen. \ ****

·         \ **Zoeken: **\ Dit veld staat gebruikers toe te zoeken naar
knooppunten van geselecteerde knooppunten of modellen. ****

De gebruiker kan de categorie(ën) slepen en ze voor, achter of bovenop
een andere categorie loslaten. Door een categorie voor een andere
categorie te plaatsen, kan de gebruiker een categorie in een
subcategorie veranderen.

 

.. raw:: html

   </div>
