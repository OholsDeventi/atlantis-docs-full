.. raw:: html

   <div class="WordSection1">

**Hoe voeg ik een onderliggend knooppunt in een categorie toe? **\ 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

Een onderliggend knooppunt wordt gecreëerd onder het ouderlijke
knooppunt. Klik op de linkermuisknop en selecteer “nieuw knooppunt”.
Deze actie zal een nieuw formulier aan de rechterzijde van het scherm,
met de volgende velden, openen:

.. raw:: html

   </div>

·         \ **Code: **\ voer de code in.

·         \ **Titel:**\  Voer de titel van de nieuwe code in.

·         \ **Scheidingsmerk: **\ Scheidt de code van een knooppunt van
de code van zijn ouder.

·         \ **Vroegste:**\  De gebruiker kan de vroegst mogelijke datum
kiezen.

·         \ **Laatste:**\  De gebruiker kan de meest recente datum
kiezen. \ ****

·         \ **Periode: **\ De toegewezen duur van het knooppunt. \ ****

·         \ **Telt niet in codering**\ : De code van dit knooppunt wordt
niet gepresenteerd. ****

·         \ **Sorteernummer: **\ Hier wordt de opeenvolging van
knooppunten en ouderknooppunten getoond.\ ****

·         \ **Opmerking: **\ Extra gemarkeerde notities.\ ****

·         \ **Omvang en inhoud: **\ De omschrijving van de omvang en de
inhoud van knooppunten. \ ****

·         \ **Gemodereerd: **\ Controleer het vakje of een categorie
gemodereerd is. De datum, tijd, status en gebruiker die de categorie
gemodereerd heft, worden weergegeven.\ ** **

**Herstelknop**

Door te klikken op de **herstelknop** wordt het bevestigingsvenster
weergegeven.

Dit bevestigingsvenster kent de volgende opties: ****

·         \ **Ja**\ : Door te klikken op Ja, keert u terug naar de
laatst opgeslagen positie.\ ****

·         \ **Annuleer**\ : Door op annuleren te klikken, wordt de
huidige actie beëindigd.  \ ****

****

 

**Opslaan**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white">

·         Deze knop slaat alle nieuwe of niet opgeslagen veranderingen
op. ****

****

 

.. raw:: html

   </div>

****

 

**Afsluitpictogram **

·         \ **Afsluitpictogram: **\ Door te klikken op het
**sluitpictogram** wordt het bevestigingsvenster weergegeven.  \ ****

§  \ **Ja: **\ Als u op Ja klikt, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ Door op de knop annuleren te klikken, blijft u op
dezelfde pagina.

 

.. raw:: html

   </div>
