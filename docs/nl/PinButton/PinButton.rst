.. raw:: html

   <div class="WordSection1">

**Pin Button**

The pin button can be found on the top of each page. The main purpose of
the pin button is to create a quick and easy access shortcut to the
pages the user wants to access frequently.

The links defined in this pinned list act as hyperlinks or as quick
access points to the screens the user has pinned. Pinned pages can be
removed from this list by navigating to the respective screens and
un-pinning them.

 

**‘Pin’ knop**\ 

Bovenaan elke pagina vind men de ‘pin’ knop. Met de ‘pin’ knop kan men
op een snelle en makkelijke manier pagina’s vastpinnen die de gebruiker
vaak bezoekt.

De links die in deze ‘pin’ lijst staan fungeren als hyperlinks of als
snelle toegang tot de door de gebruiker gepinde pagina’s. Gepinde
pagina’s kunnen worden verwijderd door naar de respectievelijke pagina’s
te navigeren en ze aldaar te on-‘pinnen’.

 

.. raw:: html

   </div>
