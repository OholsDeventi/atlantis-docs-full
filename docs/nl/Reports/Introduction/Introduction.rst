.. raw:: html

   <div class="WordSection1">

**Documenten**

In het **Atlantis Dashboard** vindt men het document-miniatuur. Het
scherm laat alle beschikbare documenten zien. De gebruiker kan elk
document selecteren om daar vervolgens verschillende handelingen bij uit
te kunnen voeren, zoals toevoegen (met het **+** icoon), verwijderen
(met het **Prullenbak** icoon) of bewerken (met het **Potlood** icoon).
De documentindelingen kunnen makkelijk worden aangepast met de
XSLT-bewerker. De gebruiker kan zijn of haar eigen velden en inhoud
ontwerpen. Documenten dienen geassocieerd te worden met de
respectievelijke metagegevens-modellen voor welke ze worden vervaardigd.

**Gereedschaps-tip: **\ Om het gebruik van clickables te vereenvoudigen
kan de gebruiker de muis boven de knoppen houden om de namen van de
knoppen te laten zien.

 

-  `**Pinvast-knop:
   ** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Zoals
   de naam doet vermoeden, wordt de pinvast-knop gebruikt om pagina’s
   vast te pinnen of op te slaan. De opgeslagen pagina’s worden in een
   lijst weergegeven onder de kop van het Atlantis Dashboard. ****

-  **Minimaliseer-knop:**\  deze knop zorgt ervoor dat de gebruiker een
   menu kan samenvouwen. Om het venster samen te vouwen, kan de
   gebruiker op de linker pijl klikken. Zodra het menu samengevouwen is,
   zal de pijl van richting veranderen (naar rechts). Door op de
   rechterpijl te drukken, wordt het menu weer uitgevouwen.

·         \ **Toevoeg-knop: **\ Klik op de **+** knop om een document
toe te voegen\ **.** Click on the **+** button to add reports.\ ****

·         \ **Zoeken:**\  Met dit veld kan een gebruiker een document
zoeken en selecteren in een lijst.\ ****

·         \ **Bewerk knop**\ : Klik op het **Potlood-icoon** om een
document te bewerken.\ ****

·         \ **XSLT knop: **

o   \ **Kop – **\ Hiermee kan de gebruiker de kopsectie van een
documentsjabloon ontwerpen middels de rich editor.\ ****

o   \ **Inhoud – **\ Hiermee kan de gebruiker de inhoudssectie van een
documentsjabloon ontwerpen middels de rich editor. ****

o   \ **Voet – **\ Hiermee kan de gebruiker de voetsectie van een
documentsjabloon ontwerpen middels de rich editor.\ ****

****

 

·         \ **Verwijderen: **\ Klik op **Prullenbak** icoon om
documenten te verwijderen.\ ****

§  \ **Ja: **\ Klik op Ja om een document te verwijderen. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
