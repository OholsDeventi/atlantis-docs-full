.. raw:: html

   <div class="WordSection1">

**Toevoeg**\ **-knop**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Klik op de plus (+) knop om een document toe te voegen en klik
op het potlood icoon om een document aan te passen.

.. raw:: html

   </div>

·         \ **Beschrijving:**\  Voeg hier de details van het document
toe zoals de naam, het nummer, etc.

·         \ **Output Extensie:**\  De gebruiker kan hier de extensie van
het document specificeren.

·         \ **Is Label:**\  Vink dit hokje aan als het document gebruikt
wordt om labels te genereren. Door het hokje aan te vinken verschijnen
er tevens andere functies:

§  \ **Paginagrootte:**\  Geef hier de gewenste paginagrootte op,
bijvoorbeeld A4, A6 etc.

§  \ **Label breedte: **\ Geef hier de breedte van het documentlabel op.

§  \ **Hoogte:**\  Geef hier de hoogte van het document op.

§  \ **Horizontale tussenruimte label:**\  Geef hier de horizontale
ruimte tussen twee labels op.

§  \ **Verticale tussenruimte label: **\ Geef hier de verticale ruimte
tussen twee labels op.

§  \ **Linker Marge:**\  Geef hier de marge op die de gebruiker over
moet laten aan de linkerzijde van het document.

§  \ **Rechter Marge: **\ Geef hier de marge op die de gebruiker over
moet laten aan de rechterzijde van het document.

§  \ **Marge bovenzijde:**\  Geef hier de marge op die de gebruiker over
moet laten aan de bovenzijde van het document.

§  \ **Marge onderzijde:**\  Geef hier de marge op die de gebruiker over
moet laten aan de onderzijde van het document.

§  \ **Labels Per Rij:**\  Geef hier het aantal labels aan die de
gebruiker per rij nodig heeft.

§  \ **Rijen Per Pagina:**\  Geef hier het aantal rijen aan die de
gebruiker per pagina nodig heeft.

 

·         \ **Detail Document:**\  Vink dit hokje aan als het document
gebruikt wordt voor het in detail beschrijven van een object.

·         \ **Voor Thesaurus:**\  Vink dit hokje aan als het document
gebruikt wordt voor het rapporteren over Thesaurus. \ ** **\ 

****

 

** **\ **Toevoeg**\ **-knop**

·         Klik op de **+** knop om een metagegevens-model uit de
keuzelijst toe te voegen, zodat een document met deze respectievelijke
metagegevens verbonden kan worden. ****

·         \ **Standaard Document: **\ Vink dit hokje aan als de
gebruiker een standaard document nodig heeft voor het geselecteerde
metagegevens-model. ****

·         \ **Bewerk-knop:**\  Klik op het potlood-icoon om de
gerelateerde metagegevens aan te passen of verwijder het standaard
vinkje.\ ****

·         \ **Verwijder-knop: **\ Klik op het **Prullenbak-**\ icoon
**** om het bevestigingsscherm te openen via waar een categorie
verwijdert kan worden.

**                                   **

**Reset knop**

Klik op de **Reset** knop om een bevestigingsscherm te openen.

Het bevestigingsscherm laat de volgende opties zien:\ ****

·         \ **Ja**\ : Klik op Ja om terug te keren naar de laatst
opgeslagen instellingen. ****

·         \ **Annuleren**\ : Klik op Annuleren om de huidige actie af te
breken.\ ****

****

 

**Opslaan**\ ** knop**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Klik op Opslaan om de huidige instellingen op te slaan.\ ****

.. raw:: html

   </div>

 

**Sluiten**\ ** knop**

•        Klik op de Sluiten knop om een bevestigingsscherm te openen.
****

§  \ **Ja: **\ Klik op de Ja knop om de pagina af te sluiten.\ ****

§  \ **Annuleren: **\ Klik op Annuleren om dezelfde pagina te blijven.

.. raw:: html

   </div>
