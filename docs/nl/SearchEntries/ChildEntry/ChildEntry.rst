.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Nieuwe invoer**

.. raw:: html

   </div>

Middels deze invoer kan een gebruiker een **child search entry**
toevoegen aan een geselecteerde parent group.

·         \ **Naam:**\  Dit is een verplicht veld en bevat de
zoek-invoer naam.

·         \ **Naam zoek scherm:**\  De gebruiker kan hier een naam
instellen voor een zoek-scherm.

·         \ **Specifiek zoek scherm: **\ De gebruiker kan een specifiek
zoek-scherm instellen.

·         \ **Parent Group:**\  Dit veld bevat een keuzelijst van de
parent group; dat wil zeggen de groep waar de gebruiker de zoek-invoer
naar moet verwijzen.

·         \ **Openbaar:**\  Door dit aan te vinken is de zoek-invoer
openbaar, anders zijn deze alleen zichtbaar in de management omgeving.

·         \ **Reset **\ 

§  Door op de **Reset** knop te drukken verschijnt er een
conformatie-scherm.\ ****

§  \ **Ja**\ : Klik op de Ja knop om te bevestigen.\ ****

§  \ **Annuleren**\ : Klik op de Annuleren knop om de Reset actie af te
breken.\ ****

·         \ **Opslaan**\ ****

§  Klik op de **Opslaan** knop om de wijzigingen op te slaan.\ ****

·         \ **Sluiten**\ ****

§  Door op de **Sluiten** knop te drukken verschijnt er een
conformatie-scherm.\ ****

§  \ **Ja: **\ Klik op de Ja knop om te bevestigen en het scherm te
sluiten.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Klik op de Annuleren knop om de Sluiten actie af
te breken.\ ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
