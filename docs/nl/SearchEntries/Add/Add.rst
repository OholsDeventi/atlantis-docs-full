.. raw:: html

   <div class="WordSection1">

**Zoektermen **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen**

.. raw:: html

   </div>

·         Klik op de + knop om het object toe te voegen in
zoekinvoeringen.

·         \ **Naam:**\  Dit veld is verplicht en vereist de naam van de
zoekinvoer.

·         \ **Naam zoekscherm:**\  Gebruikers kunnen de naam van een
specifiek zoekscherm opgeven

·         \ **Specifiek zoekscherm: **\ Gebruikers kunnen de naam van
een specifiek zoekscherm opgeven.

·         \ **Oudergroep: **\ Dit veld bevat een vervolgkeuzelijst van
oudergroepen, waar de gebruiker de zoekopdracht aan moet koppelen.

·         \ **Openbaar:**\  Het selecteervakje laat de openbare weergave
van een bepaalde zoekopdracht zien, wat anders alleen zichtbaar is voor
de beheerdersomgeving.

**Reset **

§  Door te klikken op de **Reset** knop, verschijnt het
bevestigingsvenster. ****

§  \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie.\ ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

**Opslaan**

·         Door op **opslaan** te klikken, worden alle gemaakte
veranderingen opgeslagen.\ ****

**        **

**Afsluiten**

•        Door op **annuleren** te klikken, verschijnt er een
bevestigingsvenster. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina. \ ****

.. raw:: html

   </div>

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Oudergroep: **

.. raw:: html

   </div>

In het linker deelvenster op het scherm verschijnt de zoekboom van de
oudergroepen. Elke oudergroep is vertegenwoordigd met een + knop. Als de
gebruiker op de + knop drukt, breidt het menu zich uit met meer
zoekopdrachten. 

Om zoekinvoeren te kunnen groeperen, moet de gebruiker zoekopdrachten
slepen naar en loslaten bij de gewenste zoekopdracht.

De groepering van metagegevens-modellen wordt ook gedaan door het slepen
en neerzetten bij de gewenste metadata.

De gebruiker moet links klikken op de oudergroep-cel. Deze geeft het
menu weer om knoppen te bewerken, de invoer te verwijderen, meetgegevens
toe te voegen en een nieuwe ‘onderliggende invoer’ toe te voegen.

 

**Bewerken**

·         Klik op het potloodpictogram om de zoekinvoer te bewerken.

·         \ **Naam:**\  Dit veld is verplicht en vereist de naam van de
zoekinvoer.

·         \ **Naam zoekscherm:**\  Gebruikers kunnen de naam van een
specifiek zoekscherm opgeven

·         \ **Specifiek zoekscherm: **\ Gebruikers kunnen de naam van
een specifiek zoekscherm opgeven.

·         \ **Oudergroep: **\ Dit veld bevat een vervolgkeuzelijst van
oudergroepen, waar de gebruiker de zoekopdracht aan moet koppelen.

·         \ **Openbaar:**\  Het selecteervakje laat de openbare weergave
van een bepaalde zoekopdracht zien, wat anders alleen zichtbaar is voor
de beheerdersomgeving.

 

·         \ **Reset Action**

§  Door te klikken op de **Reset** knop, verschijnt het
bevestigingsvenster. ****

§  \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie.\ ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

****

 

·         \ **Opslaan**

§  Door op **opslaan** te klikken, worden alle gemaakte veranderingen
opgeslagen.\ ****

**        **

**       **

·         \ **Afsluiten**

§  Door op **annuleren** te klikken, verschijnt er een
bevestigingsvenster. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina. \ ****

**Verwijderen**

•        Klik op het vuilnisbak-pictogram om zoekinvoer-bestanden te
verwijderen.  \ ****

§  \ **Ja: **\ Klik op ja om de bestanden te verwijderen ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:1.0in;margin-right:0in">

****

 

.. raw:: html

   </div>

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Metagegevens **

.. raw:: html

   </div>

·         Klik op de metagegevens-knop om metagegevens aan de pagina toe
te wijzen. ****

·         De gebruikers moeten een metagegevens-model aan een oudergroep
toevoegen. Het metagegevens-model bepaalt respectieve objecten die
gezocht moeten worden. ****

****

 

·         \ **Reset **

§  Door te klikken op de **Reset** knop, verschijnt het
bevestigingsvenster. ****

§  \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie.\ ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

·         \ **Opslaan**

§  Door op **opslaan** te klikken, worden alle gemaakte veranderingen
opgeslagen.\ ****

**        **

·         \ **Afsluiten**

§  Door op **annuleren** te klikken, verschijnt er een
bevestigingsvenster. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina.

****

 

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Nieuwe invoer **

.. raw:: html

   </div>

Met deze invoer kan een gebruiker een ‘onderliggende invoer’ toevoegen
aan een geselecteerde oudergroep. 

·         \ **Naam:**\  Dit veld is verplicht en vereist de naam van de
zoekinvoer.

·         \ **Naam zoekscherm:**\  Gebruikers kunnen de naam van een
specifiek zoekscherm opgeven

·         \ **Specifiek zoekscherm: **\ Gebruikers kunnen de naam van
een specifiek zoekscherm opgeven.

·         \ **Oudergroep: **\ Dit veld bevat een vervolgkeuzelijst van
oudergroepen, waar de gebruiker de zoekopdracht aan moet koppelen.

·         \ **Openbaar:**\  Het selecteervakje laat de openbare weergave
van een bepaalde zoekopdracht zien, wat anders alleen zichtbaar is voor
de beheerdersomgeving

·         \ **Reset **

§  Door te klikken op de **Reset** knop, verschijnt het
bevestigingsvenster. ****

§  \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie.\ ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

·         \ **Opslaan**

§  Door op **opslaan** te klikken, worden alle gemaakte veranderingen
opgeslagen\ ****

·         \ **Afsluiten **

§  Door op **annuleren** te klikken, verschijnt er een
bevestigingsvenster. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten. ****

§  \ **Annuleren: **\ Door op annuleren te klikken, blijft u op dezelfde
pagina\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

 

 

.. raw:: html

   </div>
