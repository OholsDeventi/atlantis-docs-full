.. raw:: html

   <div class="WordSection1">

**Zoekopdrachten**

Metagegevens model objecten kunnen worden doorzocht door middel van
zoekopdrachten. Deze zoekopdrachten kunnen worden uitgevoerd door zowel
de management als de publieke omgeving. Zoekopdrachten zijn hiërarchisch
ingedeeld en geven de gebruikers de mogelijkheid om zowel algemene
zoekopdrachten uit te voeren als meer specifieke zoekopdrachten.

Zoekopdrachten kunnen worden gemarkeerd als openbaar beschikbaar. Als
een zoekopdracht niet gemarkeerd is, is deze alleen beschikbaar voor in
de management omgeving. Zoekopdrachten uit de openbare omgeving en uit
de management omgeving hebben ieder een eigen kleur om ze uit elkaar te
houden.

Aan elk niveau van de zoekopdracht kan een metagegevens model worden
toegewezen om het bereik hiervan te bepalen.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Vastzet-knop: **\ Klik op de Vastzet-knop om de pagina
vast te zetten. De opgeslagen pagina’s worden in een lijst weergegeven
die in de kop van het Atlantis dashboard te vinden is.

.. raw:: html

   </div>

·         \ **Zoeken:**\  Middels dit veld kan een gebruiker een zoeken
naar objecten uit de lijst.\ ****

·         \ **Toevoeg-knop:**\  Klik op de plus **(+)** knop om de
download resolutie velden toe te voegen.

·         \ **Verwijderen: **\ Klik op het **Prullenbak** icoon om de
download resolutie velden te verwijderen.\ ****

§  \ **Ja: **\ Klik op de Ja knop om de invoer te verwijderen.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Klik op de Annuleren knop om de actie te
annuleren.\ ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
