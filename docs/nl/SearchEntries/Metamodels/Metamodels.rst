.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**MetaModel**

.. raw:: html

   </div>

·         Klik op de Metamodel-knop om naar de **Metamodel**
**toewijzen** pagina te gaan.\ ****

·         De gebruikers zijn nodig om de metagegevens-modellen aan de
ouder groep toe te wijzen. De metagegevens-modellen bepalen de
respectievelijke objecten die gezocht moeten worden.\ ****

****

 

·         \ **Reset**\ 

§  Door op de **Reset** knop te drukken verschijnt het
conformatiescherm.\ ****

§  \ **Ja**\ : Klik op de Ja knop om terug te keren naar de laatst
opgeslagen instellingen.\ ****

§  \ **Annuleren**\ : Klik op de Annuleren knop om de actie te
staken.\ ****

·         \ **Opslaan**

§  Klik op **Opslaan** knop om de wijzigingen op te slaan.\ **  **

·         \ **Sluiten**

§  Door op de **Sluiten** knop te drukken verschijnt het
conformatiescherm\ ****

§  \ **Ja: **\ Klik op de Ja knop om de pagina te sluiten.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Klik op de Annuleren knop om de actie te
staken.\ ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
