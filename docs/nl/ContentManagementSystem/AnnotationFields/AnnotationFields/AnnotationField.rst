.. raw:: html

   <div class="WordSection1">

***Annotatievelden***

Een gebruiker kan opmerkingen/notities aan objecten van het gewenste
metadata-model toevoegen. Hiervoor moeten zogenoemde annotatievelden
worden toegevoegd en samengehangen worden met de **gerelateerde
metagegevens-model in het linker venster.**  De gebruiker moet hiervoor
met de muis over de meta-modellen zweven die aan de linkerzijde worden
weergegeven en “annotatievelden” uit de menu selecteren. Deze actie
geeft de lijst met annotatievelden voor een bepaald metadata-model weer.
De gebruiker kan hier commentaar toevoegen. De beschrijving van de
tekstvelden kan alles zijn, zoals de naam van een document, persoon of
een nummer, etc.

·         \ `Pinvast-knop:
 <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s te
bevestigen of “vast te pinnen”. De vastgepinde pagina’s worden
weergegeven in lijsten die onder de kop “Atlantis Dasboard” te vinden
zijn. \ ****

****

 

·         \ **Afsluit-knop: **\ Door te klikken op het
**afsluitpictogram**, wordt het bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op **Ja**, wordt de pagina afgesloten.
****

**Annuleer: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.
****

·         \ **Zoeken:**\  In dit veld kunnen gebruikers zoeken op
annotatievelden uit de lijst en deze ook selecteren.

 

·         \ **Toevoegknop: **\ Door te klikken op de **+ knop** kunnen
documenten aan de annotatievelden worden toegevoegd.

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;background:white">

**Toevoegen **

.. raw:: html

   </div>

·         Klik op het **+** pictogram en voeg annotatievelden toe.

·         \ **Omschrijving:**\  voeg een omschrijving of opmerking toe
in het **annotatieveld**. Dit annotatieveld moet verplicht ingevuld
worden.

 

**Herstellen**

·         Door te klikken op de **herstelknop**, wordt er een
bevestigingsvenster weergegeven. ****

·         \ **Ja**\ : Door te klikken op de **ja-**\ knop, keert u terug
naar de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op de **annuleerk**\ nop,
wordt de betreffende actie beëindigd.\ ****

****

 

**Opslaan**

·         Deze knop slaat alle nieuwe en niet opgeslagen veranderingen
op. ****

·         Zodra dit bestand opgeslagen is, zullen het potloodpictogram
en het vuilnisbakpictogram worden weergegeven. ****

**Bewerken**

·         Klik op het **potloodpictogram** om opmerkingen aan te
passen.\ ****

·         Bewerk het **tekstveld** van de omschrijving op opmerkingen.
****

****

 

**Verwijderen **

·         Door te klikken op het **vuilnisbakpictogram** wordt ere en
bevestigingsvenster weergegeven. ****

·         \ **Ja**\ : Door te klikken op de **ja-**\ knop, wordt de
verwijder actie uitgevoerd. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Nee**\ : Door te klikken op de **annuleerk**\ nop, wordt
de verwijderactie beëindigd.

.. raw:: html

   </div>

**Afsluiten**

•        Door op het **afsluitpictogram** te klikken, wordt er een
bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door te klikken op de **ja-**\ knop, wordt de pagina
afgesloten. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op de **annuleerk**\ nop, blijft u
op dezelfde pagina. ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
