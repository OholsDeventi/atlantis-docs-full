.. raw:: html

   <div class="WordSection1">

***Dynamisch veld ***

Het dynamisch veld is gemaakt om extra velden toe te voegen aan tabellen
van het metadata-model. De weergave van dynamisch velden hangt af van
het veldtype dat bij de creatie van de velden wordt gedefinieerd.

`**Vastpin-knop:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Zoals
de naam suggereert, wordt de vastpin-knop gebruikt om pagina’s op te
slaan. De vastgepinde pagina’s worden in een lijst opgeslagen, die te
vinden is onder de kop van Atlantis Dashboard. \ ****

·         \ **Samenvouwen deelvenster-knop: **\ Klik op de
**linkerpijl-**\ knop om het venster samen te vouwen. Zodra het venster
samengevouwen is, draait de pijl naar rechts. Klik op de **rechterpijl**
om het deelvenster weer uit te vouwen.

·         \ **Toevoegknop: **\ Klik op de **+** knop om dynamisch veld
documenten toe te voegen.

·         \ **Zoeken:**\  Dit veld staat gebruikers in staat om te
zoeken en selecteren op dynamisch veld van de lijst.

·         \ **Verwijderknop: **\ Het prullenbak-pictogram is de
verwijderknop. Door te klikken op het prullenbak-pictogram wordt het
deelvenster weergegeven.

§  \ **Ja**\ : Door te klikken op **Ja,** wordt de verwijderactie
uitgevoerd.

§  \ **Nee:**\  Door te klikken op **annuleren,** wordt de
verwijderactie beëindigd.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Bewerkknop: **\ Door te klikken op het
**potloodpictogram**, kunnen de dynamisch veld documenten bewerkt
worden.

.. raw:: html

   </div>

 

Dynamische velden worden per metadata-model geselecteerd. Dynamische
velden toevoegen/bewerken wordt beschikbaar door te klikken op de optie
“dynamische velden” in het menu voor een metadata-model.  

 

 

 

.. raw:: html

   </div>
