.. raw:: html

   <div class="WordSection1">

Hoe kan ik een ***dynamisch veld toevoegen***?\ ******

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

*Toevoegen: *

.. raw:: html

   </div>

·         \ *Klik op de + knop om documenten toe te voegen aan het
dynamisch veld. \* Alle velden zijn verplicht. *

-  **Tabel: **\ De gebruiker kan de tabel selecteren uit de keuzelijst
   welke door de backend wordt ingevuld. Het veld wordt toegevoegd in de
   geselecteerde tabel.

-  **Column naam:**\  Geef de naam van een nieuwe kolom in de tabel. In
   de kolomnaam zijn geen spaties toegestaan.

-  **Veld type:**\  In de uitvouwlijst verschijnen vier type velden,
   namelijk:

§  \ **String:**\  Dit veld creëert een normaal tekstveld voor de
gebruiker.

§  \ **Bool:**\  Bool staat voor Boolean en dit geeft het veld aan met
een ja/nee met de waarde van 1 of 0. Dit veld is altijd een teken lang.

§  \ **Datum:**\  Dit veld slaat de datum op. Dit veld is altijd acht
tekens lang (jjjjmmdd).  

§  \ **Numeriek:**\  Dit is een numeriek veld en kan alleen nummers
bevatten. Het is niet mogelijk om hier nummers met komma’s in te voeren.
Dit veld heeft een maximale lengte van 38 tekens.

-  **Lengteveld: **\ voeg de tekenlengte toe voor het veld.

-  **Hulptekst: **\ In dit veld dient de gebruiker precieze informatie
   in te voeren over het nieuwe dynamisch veld, welke weergegeven wordt
   wanneer de gebruiker er met zijn muis overheen gaat. De maximale
   lengte is 50 tekens.

-  **Koptekst:**\  Deze tekst wordt gebruikt om de kop aan te passen, in
   het geval dat dit veld een samenstelling is. De maximale lengte is 50
   tekens.

-  **Label tekst:**\  Deze tekst wordt weergegeven als een label boven
   het veld in het formulier, met een maximale lengte van 50 tekens.

-  **Fouttekst:**\  Deze tekst wordt weergegeven met een rood
   uitroepteken als dit veld gemist of incorrect ingevuld wordt,
   bijvoorbeeld als een verplicht veld leeggelaten is. De maximale
   lengte van dit veld is 50 tekens.

**Herstelactie **

·         Door te klikken op de **herstel**\ knop, wordt het
bevestigingsvenster weergegeven. ****

·         \ **Ja**\ : Door te klikken op de **ja-**\ knop, wordt u
teruggebracht naar de laatst opgeslagen positie ****

·         \ **Annuleren**\ : Door te klikken op het **annuleren**-knop,
wordt de betreffende actie beëindigd. ****

**Opslaan**

·         Deze knop slaat alle nieuwe en niet opgeslagen veranderingen
op. \ ****

****

 

**Afsluitknop **

•        Door te klikken op het **afsluit-**\ pictogram, wordt het
bevestigingsvenster weergegeven.  \ ****

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>

.. raw:: html

   </div>
