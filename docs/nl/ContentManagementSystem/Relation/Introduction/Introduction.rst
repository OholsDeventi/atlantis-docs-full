.. raw:: html

   <div class="WordSection1">

***Relaties ***

Dit scherm geeft verschillende manieren weer waarop een
metagegevens-model met andere bestanden in relatie staat. Om de
verschillende relaties te zien, kunnen de gebruikers een
metagegevens-model aan de linkerkant van het scherm selecteren.
"Relaties" wordt gemarkeerd wanneer de gebruiker de muis over de
desktopweergave laat zweven of in mobiele weergave op de knooppunten of
een specifieke relatie klikt. De knooppunten (oranje en gele cirkels)
zijn de metagegevens-modellen. De relaties kunnen 2 typen zijn; boven
(blauw) of onder (groen). Daarnaast kan een relatie hiërarchisch zijn
(pijl). Hiërarchische relaties kunnen worden beschouwd als primaire
relaties die altijd worden gepresenteerd bij het navigeren van
gerelateerde objecten. Navigatie op hiërarchische relaties is optioneel.

Er kan ook een eigendomsrelatie zijn tussen metagegevens-modellen
(vetgedrukt). Dit houdt in dat objecten van de metagegevens-modellen
niet kunnen bestaan ​​zonder objecten van de metagegevens-modellen te
bezitten.

Elke wijziging in de hiërarchie of positie (boven / onder) van de
relaties, kan veranderingen weerspiegelen in de navigatie door de
verbonden gegevensobjecten.

 

 

`**PInvast-knop:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Zoals
de naam suggereert, kan de vastpin-knop gebruikt worden om pagina’s vast
te pinnen. De vastgepinde pagina’s worden opgeslagen in een lijst die te
vinden is onder het Atlantis Dashboard.

+--------------------------------------------------------------------------+
| **Oranje**\  – Dit is het geselecteerde knooppunt. Het selecteren van    |
| een knooppunt verdeelt het in “relatie boven toevoegen” of “relatie      |
| onder toevoegen”.                                                        |
|                                                                          |
|                                                                          |
|                                                                          |
| **Blauw**\       - Dit is de relatie die boven het (huidige) knooppunt   |
| geselecteerd is.                                                         |
|                                                                          |
|                                                                          |
|                                                                          |
| **Groen**\   - Dit is de relatie die onder het (huidige) knooppunt       |
| geselecteerd is.                                                         |
|                                                                          |
|                                                                          |
|                                                                          |
| **Pijl**\    - Dit is een hiërarchische relatie                          |
|                                                                          |
|                                                                          |
|                                                                          |
| **Vetgedrukt**\   - Dit is de eigendomsrelatie                           |
|                                                                          |
|                                                                          |
+--------------------------------------------------------------------------+

 

**Pijlen:**\  Op de relatiepagina kan de gebruiker de vier pijlen in de
onderste linker hoek zien. De gebruiker kan de relatiekaart verplaatsen
voor een betere weergave door op de betreffende richtingpijl te klikken.

 

**Zoom in – Zoom out knop:**\  Op de relatiepagina kan de gebruiker de
zoom-in en zoom-uit knop rechts onderin vinden. De gebruiker kan de
relatiekaart vergroten door op de zoom in-knop te klikken. De gebruiker
kan de relatiekaart verkleinen door op de zoom-out knop te klikken.
Scrollen van het muiswiel kan ook het inzoomen - uitzoomen effect geven.

 

**Standaard weergave:**\  Op de relatiepagina kan de gebruiker de
standaard weergave-knop, net boven de zoom in knop rechts onderin het
scherm vinden. De gebruiker kan op de standaardweergave knop klikken om
de relatiekaart weer te geven in de standaardweergave.

 

**Sleepfunctie:**\  De gebruiker kan de relatiekaart in de gewenste
richting slepen door op de linkermuisknop te klikken.

.. raw:: html

   </div>
