.. raw:: html

   <div class="WordSection1">

Hoe kan ik relaties voor metagegevens-modellen toevoegen?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen/bewerken**

.. raw:: html

   </div>

·         Gebruikers kunnen een relatie toevoegen door te klikken op het
oranje knooppunt.\ ****

·         Een eerste klik op een knooppunt geeft de selectie van het
knooppunt in oranje kleur weer. ****

·         Met een tweede klik wordt het volgende weergegeven:\ ****

·         \ **+**\  **> en +<** Toevoeg-knoppen: Klik op een van de
toevoeg-knoppen om een relatie boven of onder het huidige
metagegevens-model toe te voegen.\ ****

·         De gebruiker kan de relatie bewerken door op het
**potlood-**\ pictogram te klikken.\ ****

·         \ **Delete Button: **\ The **trash** icon is the delete
button. Clicking the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : Clicking the **Yes** button will delete the relation.

§  \ **No**\ : Clicking the **Cancel** button will terminate the delete
action.

·         \ **Verwijderen: **\ Klik op het prullenbak-pictogram om
bestanden te verwijderen.  Door te klikken het prullenbak-pictogram
verschijnt er een bevestigingsvenster:

§  \ **Ja: **\ Klik op ja om de bestanden te verwijderen ****

§  \ **Annuleren**\ : Door te klikken op annuleren, zal de betreffende
actie worden beëindigd.  \ ****

 

\*\ ***Om het huidige metagegevens-model te bewerken:*** *klik op het
knooppunt\ *****

-  Klik op het potlood-pictogram om relaties te bewerken.

-  **Metagegevens-modellen:**\  Metagegevens-modellen zijn vooraf
   ingesteld, waardoor de gebruiker kan kiezen uit verschillende
   beschikbare modellen uit de vervolgkeuzelijst. ALs de gebruiker een
   nieuw metagegevens-model wil toevoegen, verandert het plan. Dit veld
   moet verplicht worden ingevoerd.

-  **Tabblad vooruit:**\  Selecteer het tabblad van de
   vervolgkeuzelijst. In dit tabblad wordt in het linkerdeel van het
   scherm weergegeven.

-  **Tabblad achteruit:**\  Selecteer het tabblad van de
   vervolgkeuzelijst. In dit tabblad wordt in het rechterdeel van het
   scherm weergegeven.

-  **URL vooruit:**\  dit linkt naar een specifiek scherm dat het
   standaard zoekformulier verbindt met metagegevens-modellen. Er worden
   verbindingen gemaakt tussen objecten en metagegevens-modellen. Aan de
   rechterkant kunt u objecten selecteren en aan de linkerkant de
   metagegevens-modellen.

 

 

 

-  **URL terug: **\ dit linkt naar een specifiek scherm dat het
   standaard zoekformulier verbindt met metagegevens-modellen. Er worden
   verbindingen gemaakt tussen objecten en metagegevens-modellen. Aan de
   rechterkant kunt u objecten selecteren en aan de linkerkant de
   metagegevens-modellen.

-  **Relation name forth:**\  Name of the relation from the metadata
   model selected on the right to the metadata model selected on the
   left.

-  **Relation name back:**\  Name of the relation from the metadata
   model selected on the right to the metadata model selected on the
   left.

-  **Max number forth:**\  Total number of relations from the metadata
   model selected on the right to the metadata model selected on the
   left. 0 implies unlimited number of relations. This is a mandatory
   field.

-  **Max number back:**\  Relations from the metadata model selected on
   the left to the metadata model selected on the right. 0 implies
   unlimited number of relations. This is mandatory field.

 

·         \ **Aanvinkvakjes**

§  \ **Is hiërarchisch : **\ Vink dit aan als er een relatie is tussen
de hiërarchieën ****

§  \ **Is bezitter: **\ Vik dit aan als het knooppunt een ouderknooppunt
is. ****

§  \ **Kopie voort: **\ Vink dit aan als de relatie verder gekopieerd
kan worden.\ ****

§  \ **Index Forth:**\  Flag for a forward index.\ ****

§  \ **Navigeren voor gebruik: **\  Markeer als de verbinding met andere
metagegevens-modellen gezien kan worden. ****

§  \ **Is Multimedia:**\  Markeer als het gaat om een
multimediabestand.\ ****

§  \ **Verberg de relatie tab: **\ Markeer als de gebruiker de
relatietabblad wil verbergen.\ ****

§  \ **Kopieer terug:**\  Markeer als de gebruiker de relatie wil
weergeven.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.5in;margin-right:0in">

§  \ **Index terug: **\ Markeer voor een achteruit index.\ ****

****

 

.. raw:: html

   </div>

******

 

******

 

**Reset **

·         Door te klikken op **reset**, wordt het bevestigingsvenster
weergegeven.\ ****

·         \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar
de laatst opgeslagen positie.\ ****

·         \ **Annuleren**\ : Door te klikken op annuleren, wordt de
betreffende actie beëindigd. ****

****

 

**Opslaan**

·         Met deze knop worden alle nieuwe en niet-opgeslagen
wijzigingen opgeslagen. ****

****

 

******

 

**Afsluiten**

•        Door te klikken op **afsluiten,** wordt er een
bevestigingsvenster weergegeven. ****

§  \ **Ja: **\ Door op ja te klikken, wordt de pagina afgesloten.\ ****

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

 

.. raw:: html

   </div>
