.. raw:: html

   <div class="WordSection1">

Hoe kan ik CM-velden velden bewerken? \ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Toevoegen of bewerken **

.. raw:: html

   </div>

·         Klik op de **+ knop** om **toe te voegen** of op het
**potloodpictogram** om **CM-velden** te bewerken.

·         \ **Naam:**\  De naam van het CM-veld. Dit tekstveld is
verplicht.

·         \ **Label: **\ Voeg hier de tekst toe die u als gebruiker wilt
laten weergeven.

·         \ **Serienummer:**\  Voeg hier het nummer in dat wordt
gebruikt om de presentatievolgorde te beïnvloeden, in bijvoorbeeld een
zoekscherm, sorteren, resultatenkolom, etc. 

·         \ **Gewicht: **\ Voer hier het voorrgangnummer van het CM-veld
in. Dit bepaalt de oplopende en dalende volgorde van een CM-veld.

·         \ **Keuzevakjes:**

§  \ **Directe velden: **\ een CM-veld kan een direct veld zijn als het
bestaat uit een of meer velden uit een metadata-model. Deze kunnen
worden geselecteerd uit een lijst met beschikbare Metapaths. ****

-          Een vinkje op **direct veld** zal een ander formulier openen
om een MetaPath toe te voegen. ****

-          \ **MetaPath: **\ Selecteer de MetaPath uit de lijst. ****

-          \ **MetaField:**\  Selecteer de MetaField uit de lijst. ****

-          \ **Is\_From:**\  markeer als het metamodel van een ander pad
komt. ****

-          \ **ISTM: **\ Markeer als het metamodel aan een ander pad
toegevoegd moet worden. ****

-          \ **Sluitknop:**\  Klik op de sluitknop om de MetaPath velden
af te sluiten. ****

-          \ **Opslaan: **\ De MetaPaths kunnen worden opgeslagen door
het algemene opslaan-pictogram aan het einde van de pagina. ****

****

 

-          \ **Verwijderknop: **\ Het vuilnisbak-icoon is de
verwijderknop. Door te klikken op de verwijderknop verschijnt er een
bevestigingsvenster. \ ****

o   \ **Ja**\ : Door te klikken op de **ja-**\ knop, wordt de
verwijderactie uitgevoerd. ****

o   \ **Nee**\ : Door te klikken op de **annuleerk**\ nop, wordt de
verwijderactie beëindigd. \ ****

****

 

****

 

-          \ **Opslaan**

o   Door te klikken op de **opslaan-**\ knop, worden de veranderingen
opgeslagen. Let op: de veranderingen kunnen niet opgeslagen worden
totdat alle gerelateerde velden op de juiste manier zijn ingevuld.
\ ****

o   Zodra de veranderingen zijn gemaakt, zal het potloodicoon
weergegeven worden zodat veranderingen kunnen worden **aangepast** en
het vuilnisbakicoon om berichten te **verwijderen.**  \ ****

****

 

§  \ **Soort veld: **\ Specificeert of het veld al dan niet gebruikt
wordt voor sorteren.\ ****

§  \ **Zoekveld: **\ Specificeert of dit gebruikt al dan niet gebruikt
wordt voor zoeken. ****

§  \ **Algemeen: **\ Specificeert of de CM-veld algemeen is. ****

§  \ **Openbaar: **\ Markeer of het CM-veld door alle gebruikers gezien
kan worden. ****

§  \ **Management: **\ Markeer of het CM-veld beperkt is tot alleen de
beheerders.\ ****

§  \ **Stijging: **\ Markeer om het CM-veld in oplopende volgorde weer
te geven.\ ****

§  \ **Resultatenveld:**\  Markeer om de resultaat kop in het CM-veld
weer te geven.\ ****

§  \ **Pictogramveld:**\  Markeer of een pictogramveld in het CM-veld
toegevoegd moet worden. \ ****

§  \ **Locatieveld:**\  Markeer of de locatie van het CM-veld moet
worden opgenomen.\ ****

§  \ **Contextveld: **\ Markeer of de context in het CM-veld moet worden
opgenomen. ****

§  \ **Sorteren op naam:  **\ Markeer of de storting op naam van het
CM-veld moet worden uitgevoerd. ****

§  \ **Datum: **\ Markeer om de datum toe te voegen. ****

§  \ **Nummer:**\  Markeer om een nummerveld toe te voegen. ****

§  \ **Tekst: **\ Markeer om een tekstveld toe te voegen. ****

§  \ **Volledige tekst: **\ Markeer om een volledige tekst in het
CM-veld toe te voegen. ****

§  \ **Relevantieveld: **\ Markeer om de velden die relevant zijn voor
het huidige CM-veld in te vullen. ****

§  \ **Verkorte beschrijving: **\ Markeer indien gewenst of de
beschrijving kort is. ****

§  \ **Padvermelding:**\  Markeer of de gebruiker het pad moet
toevoegen. ****

§  \ **Standaard alfabetisch: **\ Dit vinkje wordt gebruik om
alfabetisch te navigeren. ****

§  \ **Uitschakelen van de markering: **\ Vink dit aan om de markering
uit te schakelen.\ ****

§  \ **Filter:**\  Markeer om de filterfunctie in te vullen. ****

****

 

·         \ **Zoekopdrachten selectie: **\ In dit veld moet de gebruiker
de tabelnaam invullen en kan de gebruiker de kolom selecteren door de
tabel uit te breiden. ****

·         \ **Hulptekst:**\  Gedetailleerde informatie betreffende de
CM-velden wordt hier weergegeven als de gebruiker de muis hier overheen
laat zweven.

 

**Reset **

·         Door te klikken op de **reet**\ ** **\ wordt het
bevestigingsvenster weergegeven. \ ****

·         \ **Ja:**\  Door te klikken op de **ja-**\ knop, keert u terug
naar de laatst opgeslagen positie.\ ****

·         \ **Annuleer:**\  Door te klikken op de **annuleerk**\ nop,
wordt de betreffende actie verwijderd. ****

****

 

**Opslaan**

·         Door te klikken op de **opslaan**-knop, worden de gemaakte
wijzigingen opgeslagen. Let op: de wijzigingen worden niet opgeslagen
totdat alle betreffende velden correct zijn ingevuld. ****

·         Nadat de wijzigingen zijn opgeslagen, wordt het
potloodpictogram weergegeven om te **bewerken** en het
**prullenbakpictogram** om te verwijderen. ****

****

 

**Afsluiten **

Door te klikken op het **sluitpictogram**, wordt het bevestigingsvenster
weergegeven. ****

§  \ **Ja: **\ Door te klikken op de **ja-**\ knop klikt, wordt de
pagina afgesloten. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren: **\ Door te klikken op de **annuleerknop**, blijft u
op dezelfde pagina.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
