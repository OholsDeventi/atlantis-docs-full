.. raw:: html

   <div class="WordSection1">

***CM-velden***

Content Management (CM) velden worden gebruikt om verschillende
functionaliteiten in het systeem te configureren. Ze worden gebruikt
voor verschillende toepassingen, zoals het zoeken, sorteren, filteren en
presenteren van gegevens op een gedetailleerde manier. In deze CM-velden
wordt gedefinieerd hoe database-informatie wordt gebruikt om specifieke
velden te implementeren voor zoeken, enzovoort.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·        
\ `Pinvast-knop: <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\ 
Zoals de naam suggereert, kunnen pagina’s hiermee worden vastgepind. De
vastgepinde pagina’s worden in een lijst opgeslagen die te vinden is
onder de kop van de “Atlantis Dashboard”. \ ****

.. raw:: html

   </div>

·         \ **Samenvouw-knop:**\  Klik op de **linkerpijl-knop** om het
deelvenster samen te vouwen. Zodra het venster samengevouwen is, zal
diezelfde pijl naar rechts draaien. Klik op de **rechterpijl** om het
deelvenster weer uit te vouwen.

·         \ **Toevoegknop:**\  Klik op de **+** knop om CM-velden toe te
voegen.

**Filterpictogram:**\  De filterfunctie bevat verschillende koppen
waarin de gebruiker het CM-veld wil weergeven. De types van CM-velden
die in de tabel worden weergegeven, kunnen worden geselecteerd door te
aan- of uit te vinken. Bestaande CM-velden worden in een tabelformaat
aan de gebruiker weergegeven. De tabel kan worden gefilterd door gebruik
te maken van het filterpictogram boven de tabel.

De filter bevat de koptekst, bestaande uit: 

·         \ **Serie**\ : Het serienummer van het CM-veld\ ****

·         \ **Naam: **\ Naam van het CM-veld\ ****

·         \ **Openbaar: **\ Markeer of het CM-veld in een openbare
omgeving gebruikt wordt.  \ ****

·         \ **Management: **\ Markeer of het CM-veld gebruikt moet
worden in een managementomgeving.

·         \ **Sorteren: **\ Markeer of het CM-veld voor het sorteren van
zoekgegevens is.

·         \ **Zoeken: **\ Markeer of het CM-veld bedoeld is voor
zoekopdrachten

·         \ **Globaal: **\ Markeer of het CM-veld bedoeld is voor alle
metagegevens-modellen.

·         \ **Resultaat: **\ Markeer of het CM-veld een zoekresultaten
kolom is

·         \ **Pictogram: **\ Markeer of de gebruiker het pictogram wil
toevoegen in het CM-veld

·         \ **Detail**\ : Markeer of het CM-veld in detail getoond moet
worden

·         \ **Direct: **\ Markeer of CM-velden direct toegevoegd worden
door MetaPath. Als de inhoud van een CM-veld slechts uit een of meer
velden van de metagegevens-modellen bestaat, is de registratie via
“direct field” van toepassing.

·         \ **Filter: **\ Markeer of het CM-veld gebruikt wordt voor het
filteren van zoekresultaten

·         \ **Xslt: **\ Ga naar de XSLT Editor om het CM-veld te
bepalen/bewerken \ ****

·         \ **Soort functie van de koptekst: **\ elk van de
kolomkop-eigenschappen kan worden aangeklikt om de tabel van die
specifieke kolom te sorteren. Door nogmaals te klikken wordt de
sortering omgedraaid. ****

·         Elke kolom heeft een **SORTEER**-pictogram naast de naam. ****

·         Klik op het sorteerpictogram om documenten in oplopende- of
aflopende volgorde te zien. ****

·         \ **Verwijderknop: **\ Het **prullenbakpictogram** is de
verwijderknop. Door te klikken op het prullenbakpictogram, verschijnt er
een bevestigingsvenster. 

§  \ **Ja:**\  Door te klikken op de **ja-**\ knop, wordt de
verwijderactie uitgevoerd.

§  \ **Nee**\ : Door te klikken op de **annuleer-**\ kop, wordt de
verwijderactie beëindigd.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Bewerkknop: **\ Klik op het potloodpictogram om
CM-velddocumenten te bewerken.

.. raw:: html

   </div>

.. raw:: html

   </div>
