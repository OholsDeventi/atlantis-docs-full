.. raw:: html

   <div class="WordSection1">

***E-Depots***

E-Depot toont de status van de applicatie. ` <>`__\ `**E-Depots**
 <>`__\ `**XSLT
editor**\ `[PvD1] <#_msocom_1>`__\   <../XSLT%20Editor/XSLT%20Editor.docx>`__\ `[TP2] <#_msocom_2>`__\  \ :
Het is een editor waarmee een gebruiker wijzigingen kan aan brengen of
nieuwe functionalitieiten kan ontwikkelen en toevoegen in het E-Depot
XSLT.

**E-Depot Account:** Controleer het vak, zodat de ingevoerde gegevens in
XSLT alleen in e-depots kunnen worden weergegeven.

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__There should be a link to the help file
describing the XSLT Editor funcitons.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_2" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_2','_com_2')"
   onmouseout="msoCommentHide('_com_2')">

` <>`__
 `[TP2] <#_msoanchor_2>`__Unfortunately the link does not work in this
document

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
