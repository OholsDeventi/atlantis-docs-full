.. raw:: html

   <div class="WordSection1">

**XSLT - XSL**\  (Extensible Stylesheet Language) is een stylingtaal
voor XML. XSLT staat voor “XSL Transformations”. XSLT wordt gebruikt om
XML-documenten om te zetten in andere formaten, zoals HTML. 

Door de XSLT-editor kan de gebruiker op een makkelijke en effectieve
manier XSL-broncodes maken. Hierdoor hebben gebruikers op geen enkel
niveau te maken met coderen.

De gebruikers hoeven alleen de informatie en opties te gebruiken die
worden weergegeven om de structuren die zij wensen, te creëren en de
broncodegedeeltes in de XSLT-editor zullen de werkelijke XSL-broncodes
verschaffen, op basis van de informatie die verstrekt wordt in de
tekstverwerker.

Om de tekstverwerker in te schakelen, moet de gebruiker klikken op
“\ **Enable Editor”**. Door te klikken op deze kop, wordt de gebruiker
vooraf gewaarschuwd dat de gebruiker de broncode nu kan bewerken.

 

De tekstbewerker heeft de volgende bewerkfuncties:

·         \ **Vet**\  

Geeft de tekst een **sterke nadruk**, die meestal sterker/zwaarder is
dan de omliggende tekst. Sneltoetskoppeling is CTRL + B

·         \ **Cursief**\  

Zorgt ervoor dat de geselecteerde tekst *cursief* gemaakt wordt. De
richting is meestal schuin naar rechts. Sneltoetskoppeling is CRTL + I

·         \ **Doorhalen**\  

[STRIKEOUT:Voegt een lijn toe ]\ in het midden van de geselecteerde
tekst [STRIKEOUT:]

·         \ **Onderstrepen**\  

Voegt een lijn toe onder de geselecteerde tekst. De sneltoets koppeling
is CTRL + U

·         \ **Lijsten – **\ De knop toevoegen/verwijderen van
opsommingstekens en de nummeringsknop maken de tekst op met
opsommingstekens of nummeringslijsten.

·         \ **Citaatblok –**\  Dit is een citaat in een schriftelijk
document dat afkomstig is van de hoofdtekst als een paragraaf of een
tekstblok en wordt typisch visueel onderscheiden door gebruik te maken
van alinea en een ander lettertype of juist een kleiner lettertype.

·         \ **Tekstuitlijning – **\ Deze set van vier knoppen past de
tekst aan om ofwel links uitgelijnd te worden, gecentreerd tussen de
marges, rechts uitgelijnd te worden of om de tekst uit te vullen
(gelijkmatig over de pagina te verdelen). 

 

·         \ **Link – **\ Hiermee kan de gebruiker een hyperlink
toevoegen aan het XSLT-bewerkingsgebied. Het systeem accepteert de naam
van de hyperlink. De gebruiker kan kiezen uit een versterkte URL, linken
naar een andere link of het verstrekken van emailgegevens. Elke optie
voegt een hyperlink toe met een versterkte naam, die u kunt bereiken
door er dubbel op te klikken. 

·         \ **Ontkoppelen – **\ Hiermee kan de gebruiker de hyperlink
verwijderen en de tekst leeg maken. ****

·         \ **Controlepunt – **\ Dit kan, indien nodig, gebruikt worden
om een anker of een controlepunt toe te voegen. De gebruiker kan het
overal ter plekke plaatsen. Om het anker te gebruiken moet de gebruiker
wel een naam aan het anker toevoegen. ****

·         \ **Uitbreiden –**\  De maximaliseerknop zal de tekstbewerker
uitbreiden waardoor de tekstbewerker in de volledige breedte en hoogte
van de browser wordt weergegeven.

**·         Paragraafformaat – Hiermee kan de gebruiker de geselecteerde
test die in het XSLT-bewerkgebied wordt gebruikt, veranderen in
verschillende beschikbare lettertypestijlen die in de lijst zijn
weergegeven. **

**·         Lettergrootte – Hiermee kan de gebruiker de lettergroottes
van de geselecteerde tekst wijzigen.  **

**·         Tekstkleur – Hiermee kan de gebruiker de tekstkleur van de
geselecteerde tekst wijzigen. **

**·         Achtergrondkleur – Hiermee kan de gebruiker de
achtergrondkleur van de geselecteerde tekst in een andere kleur
wijzigen. **

**·         Plakken als platte tekst – Hiermee kunnen teksten geplakt
worden in de XSLT-bewerkruimte. De tekst die in deze ruimte is geplakt,
verliest hiermee de eerste opmaak en wordt als platte tekst ingeplakt.
**

**·         Plakken vanuit Word – Hiermee kunnen teksten geplakt worden
in de XSLT-bewerkruimte. De tekst die in deze ruimte is geplakt behoud
de eerder toegepaste opmaakt. **

**·         Afbeelding toevoegen – Hiermee kan de gebruiker afbeeldingen
toevoegen aan het XSLT-bewerkgebied. De gebruiker kan een afbeelding
toevoegen door de URL van die afbeelding te gebruiken, of door
alternatieve tekst toe te voegen met de juiste breedte, hoogte, kaders,
Hspace, Vspace en uitlijning. In het voorbeeld vak kan de gebruiker
controleren hoe de afbeelding eruit zal zien in de XSLT-bewerkruimte
voordat deze wordt toegevoegd aan de XSLT-bewerker. **

**·         Tabel toevoegen – Hiermee kan de gebruiker een tabel
toevoegen aan de XSLT-bewerkruimte. De gebruiker kan het aantal rijen en
het aantal kolommen definiëren die nodig zijn, kiezen waar de koptekst
geplaatst wordt en de grootte van de tabel. Verder kan de gebruiker ook
nog de uitlijning van de tabel, de breedte, hoogte, cel afstand en
witruimtes tussen de cellen aanpassen. **

·         \ **Speciale tekens/symbolen invoegen – Hiermee kan de
gebruiker een speciaal teken of symbool toevoegen aan de
XSLT-bewerkruimte. De gebruiker kan kiezen uit de beschikbare opties. 
**\ 

**·         Inspringen van de tekst – **\ Deze twee toetsen regelen het
niveau van het inspringen van de tekst, zowel naar rechts als naar
links. Elke klik springt de tekst een stukje in, in de geselecteerde
richting. \ ****

**·         Ongedaan maken – Hiermee kan de gebruiker wijzigingen, stap
voor stap, ongedaan maken. **

**·         Overdoen – Dit stelt de gebruiker in staat wijzigingen
opnieuw te veranderen, nadat de gebruiker ervoor gekozen had, de
wijzigingen ongedaan te maken. **

**·         Pagina-einde – Met het gebruik van deze functie, zal de
tekst in twee pagina’s worden opgesplitst. **

·         \ **Bron –**\  Deze knop geeft de HTML-broncode weer, die in
de tekstbewerker geformatteerd is.

 

**Invoegen**\  – De gebruiker kan een enkele waarde, lijst of relatie
aan de XSLT-bewerkruimte toevoegen.

  \ ****

·         \ **Enkele waarde**\  – Als de gebruiker een enkele waarde wil
toevoegen aan de XSLT-editor, moet deze functie gebruikt worden.

o   \ **Tekst waarde**\  – Door deze optie te kiezen, kan de gebruiker
een enkele tekst waarde toevoegen aan de XSLT-editor. De gebruiker kan
een willekeurige tekst typen in de tekstruimte en vervolgens een tekst
naar keuze toevoegen. Deze optie kan men gebruiken om een eigen tekst
toe te voegen.

o   \ **Veld**\  – Dit is de standard optie wanneer een gebruiker een
enkele waarde wil toevoegen aan de XSLT-editor. Door deze optie te
kiezen, kan de gebruiker een tabel of een specifiek veld van een tabel
toevoegen aan de XSLT-editor. De gebruiker kan ofwel zoeken naar een
tabel door dit in te voeren in het tekstveld ofwel door het kiezen
tussen de opties onder het tekstveld. Deze opties zijn hiërarchisch
weergegeven, waarbij de naam van de tabel bovenaan te zien is. De
afzonderlijke velden kan men inzien door de tabel uit te vouwen. 

o   \ **Functie**\  – Door deze optie te kiezen, kan de gebruiker een
functie toevoegen uit de lijst. De toe te voegen opties zijn afhankelijk
van het gekozen metagegevens-model. Wanneer de gebruiker een functie uit
de lijst selecteert, worden alle parameters van deze functie apart
weergegeven. De gebruiker kan de invoergegevens naar eigen smaak
aanpassen. De opties waartussen gekozen kan worden zijn *Tekst, veld* en
*functie* of het toevoegen van data aan de parameters.

 

·         \ **Lijst**\  – Door deze lijst kan de gebruiker een lijst
toevoegen aan de XSLT-bewerkruimte. De gebruiker dient een tabel te
selecteren uit de lijst van de beschikbare tabellen in de keuzelijst.
Aan de hand van de geselecteerde tabel, zal er een lijst worden
toegevoegd aan de XSLT-bewerkruimte.

·         \ **Relatie**\  – Relatie is hier een soort lijst maar is
bepaald op basis van de relaties van het metagegevens-model waarvoor
XSLT-editor wordt geopend.

**Voorwaarden** -

·         \ **Mits **\ - “\ **Mits**\ ” wordt gebruikt als een bepaalde
voorwaarde eerst **behaald** dient te zijn alvorens een volgende actie
volgt. De gebruiker vult een waarde in, kiest een conditionele
voorwaarde en geeft de MITS waarde op waaraan voldaan dient te worden.

·         \ **Mits of anders**\  –  Dit voegt een apart ‘of anders’ veld
toe wanneer het wordt toegevoegd aan de XSLT-editor. De gebruiker kan
hier aangeven wat er dient te gebeuren als de MITS voorwaarde **niet
gehaald** wordt.

·         \ **Of anders mits**\  – De gebruiker kan hier een ‘Of anders,
mits’ clausule toevoegen aan een MITS voorwaarde als er nog een
voorwaarde is die de gebruiker gecontroleerd wil hebben.

**Statisch**\  – ‘Statisch’ biedt vooraf gedefinieerde sjablonen die
toegevoegd kunnen worden aan de XSLT-editor. De gebruiker kan dit
sjabloon tijdens het toevoegen nog bewerken alvorens het toe te voegen
aan de XSLT-editor.

**Bijwerken**\  – De gebruiker kan de Bijwerken-knop gebruiken om de
opgeslagen XSLT-code bij te werken.

**Voorvertoning**\  – De gebruiker kan op het Oog- of
Voorvertoning-icoon klikken om een voorvertoning te zien van de
gegenereerde XSLT-code, aan de hand van de gemaakte veranderingen in de
XSLT-editor.

**Opslaan**\  – Wanneer de gebruiker op de Opslaan-knop drukt, zullen
enkel de verandering die gemaakt zijn in de XSLT-editor toegepast worden
op de BRON CODE.

**Reset**\  – Deze knop draait alle gemaakte verandering terug.

**Bron Code**\  – De gebruiker kan wisselen naar het BRON CODE scherm om
zo de complete broncode in te zien die gegenereerd is door de
veranderingen die gemaakt zijn in de XSLT-omgeving.

 

.. raw:: html

   </div>
