.. raw:: html

   <div class="WordSection1">

**Formulieren **\ 

 

·         Met de formulierenbouwer kan de gebruiker formulieren maken
voor metagegevensmodellen. In ieder formulier zijn verschillende
elementen geïntegreerd.

·         Aanvankelijk wordt aan elk metagegevensmodel een reeks
formulieren toegewezen. Desondanks kan de gebruiker, indien nodig, ook
zijn eigen formulieren ontwikkelen met behulp van de formulierenbouwer.

·         Het formulier wordt over het algemeen gebouwd als in raster-
en tabelformaat.

 

**Formulierenbouwer **\ 

 

·         De formulierenbouwer maakt het voor gebruikers makkelijker om
velden te slepen en neer te zetten in het systeem omdat het bestaat uit
rijen en kolommen.

·         Het formulier, veld, veldgroep, samenvoegingen en
metagegevensmodellen zijn onderdelen van de formulierenbouwer.

 

**Formulierenlijst**\ 

 

·         Gebruikers kunnen formulieren bekijken door op de formulieren
in specifieke metagegevens-modellen te klikken.

·         Een lijst met alle formulieren wordt weergegeven, waar de
gebruiker, in de rechterbovenhoek in de formulierenlijst, ook nieuwe
formulieren kan toevoegen.

·         De gebruiker kan naar formulieren zoeken in de bovenste
zoekbalk en formulieren bewerken of verwijderen doorgebruik te maken van
de **actie** pictogrammen.

 

**Een formulieren maken**\ 

 

·         De gebruiker moet het formulier een naam toewijzen, een
sjabloonnaam opgeven, een tabel selecteren waaruit de gegevens voor het
formulier gehaald worden en het formulier aan bepaalde gebruikersgroepen
toewijzen.

·         De gebruiker moet de volgende gegevens invoeren voordat nieuwe
formulieren aangemaakt kunnen worden:

·         \ **Naam – **\ Dit is de naam van het bestand, of in andere
woorden, de hier ingevoerde woorden zijn een representatie van het
formulier.

·         \ **Sjabloonnaam**\  – Hier kan de gebruiker een sjabloonnaam
voor het formulier opgeven. Het formulier wordt gemaakt als sjabloon,
die verder gebruikt/aangepast kan worden in Aggregatie.

·         \ **Tabelnaam**\ – Dit bestaat uit databasetabellen waar
gegevens worden opgeslagen. Tabellen veranderen volgens het gekozen
metagegevensmodel.

·         \ **Gebruikersgroepen**\  – Dit bestaat uit lijsten van
gebruikersgroepen die al in het systeem zijn geconfigureerd. De
gebruiker kan hier een of meer van de beschikbare gebruikersgroepen
selecteren.

·         \ **Markeren als standaard**\ – Hiermee wordt het formulier
als “standaard” gemarkeerd, voor de geselecteerde metagegevensmodellen.

 

****

 

**
**

**Een formulier toevoegen (tabel) in de editor**

·         De gebruiker kan een element van het formulier slepen en
neerzetten in de raster-lay-out. Dit is bedoeld als richtlijn en kan
worden gebruikt als limiet om te zien hoeveel verschillende elementen er
aan een formulier kunnen worden toegevoegd.

·         De gebruiker kan elementen uit het formulieren slepen en
uitbreiden naar de vereiste wijdte en breedte in zinnen van rijen en
kolommen.

·         Geen enkel element kan buiten de spanwijdte van het formulier
worden geplaatst.

·         De formuliereigenschappen worden rechts in het gedeelte
**eigenschappen** weergegeven. ****

**Elementen van de formulierbouwer **

****

 

·         \ **Hoofdformulier**\ 

-  **Rij**\ : Dit geeft het aantal rijen in het hoofdformulier aan.

-  **Kolommen**\ : Dit geeft het aantal kolommen in het hoofdformulier
   aan.

 

·         \ **Velden **\ 

-  **Veldnamen**\ : Dit biedt een lijst met velden. Beschikbare velden
   in de lijst zullen afhankelijk zijn van de gekozen tabel tijdens het
   maken van het formulier.

-  **Veldtype**\ : Dit veld is belangrijk voor de velden binnen de
   samenvoeging (aggregatie). Beide zorgen ervoor dat het veld ingevuld
   kan worden zoals aangegeven in de aggregatie-labels. Alleen een
   invoer zorgt ervoor dat een veld binnen de aggregatie ingevuld kan
   worden, zonder in de aggregatie-labels zichtbaar te zijn.

-  **Rij**\ : Dit geeft aan in welke rij het formulier wordt
   weergegeven.

-  **Van kolom**\ : Dit geeft aan in welke kolom het veld moet beginnen.

-  **Naar kolom**\ : Dit geeft aan in welke kolom het veld moet
   eindigen.

-  **Orderkop**\ : Dit geeft aan wat de orderkop van een veld in een
   aggregatie is.

-  **Tekst voor label**\ : Dit geeft de labelwaarde van het veld aan.
   Als hier geen input wordt gegeven, wordt de standard label voor dit
   veld gebruikt.

-  **Verplicht**\ : Als dit aangevinkt is, betekent het dat dit veld
   verplicht ingevuld moet worden.

-  **Alleen-lezen**\ : Als dit aangevinkt is, wordt dit veld getoond als
   “Alleen-lezen” in het formulier.

-  **Clear in copy**\ : Dit geeft aan of de gegevens in het veld
   verwijderd worden als deze gekopieerd worden.

-  **Tekstblok**\ : Als dit aangevinkt is, wordt het tekstveld
   weergegeven als een zogenaamd “multiline” tekstblok.

-  **Hoogte multiline**\ : Dit geeft aan wat de hoogte is van het
   multiline tekstblok. Dit veld is optioneel en accepteert
   pixelwaarden.

-  **Breedte**\ : Dit geeft de breedte van het veld in de kolomcel aan.
   Dit veld is optioneel en accepteert pixelwaarden en
   percentagewaarden.

-  **Toon Thesaurus Detail Popup**\ : Als dit veld is aangevinkt, kan de
   gebruiker het veld alleen invullen met een waarde uit thesaurus, als
   ere en thesaurus beschikbaar is.

-  **Afhankelijk van**\ : Dit geeft aan dat gegevens die in dit veld
   zijn ingevoerd, afhankelijk zijn van andere gegevens uit een ander
   veld. Dit werkt alleen voor thesaurus velden.

-  **Datum type**\ : Dit geeft aan of het datumveld de vroegste datum of
   de laatste datum van de periode heeft.

-  **Datumgroepering**\ : Dit veld accepteert de naam voor de groep. Dit
   zorgt ervoor dat alle datumvelden binnen de periode vallen, zodat de
   datumvelden niet eerder kunnen zijn dan de vroegste en niet later
   kunnen zijn dan de laatste datum.

 

·         \ **Veldgroepen **\ 

-  **Naam**\ : Hier kan de naam, die de veldgroep vertegenwoordigt,
   ingevoerd worden.

-  **Vanaf rij**\ : Dit geeft aan uit welke rij de veldgroep moet
   beginnen.

-  **Naar rij**\ : Dit geeft aan in welke rij de veldgroep moet
   eindigen.

-  Bestelling: Dit geeft de volgorde van de veldgroepen aan.

 

·         \ **Aggregatie (Samenvoeging)**

-  **Naam**\ : Hier kan de naam van de aggregatie worden ingevoerd.

-  **Rij**\ : Dit geeft aan in welke rij de aggregatie in het formulier
   wordt weergegeven.

-  **Vanuit kolom**\ : Dit geeft aan in welke kolom de aggregatie moet
   beginnen

-  **Naar kolom**\ : Dit geeft aan in welke kolom de aggregatie moet
   eindigen.

-  **Sjabloon**\  – Hier kan de gebruiker een sjabloon kiezen, die is
   aangemaakt om toegepast te worden op de aggregatie.

-  **Enkele weergave **\ – Als dit wordt aangevinkt, zorgt u ervoor dat
   de velden niet worden herhaald.

·         \ **Tabbladen**

-  **Naam**\ : Hier kan de naam van het tabblad worden ingevoerd.  

-  **Vanaf rij**\ : Dit geeft aan vanaf welke rij het tabblad moet
   beginnen.

-  **Naar rij**\ : Dit geeft aan vanaf welke rij het tabblad moet
   eindigen.

-  **Volgorde**\ : Dit geeft de volgorde van het tabblad aan.

 

·         \ **Metagegevens-model **

-  **Naam**\ : Dit voorziet in een lijst met metagegevens modellen,
   waaruit de gebruiker kan kiezen om een metagegevens model te
   selecteren.

-  **Rij**\ : Dit geeft aan in welke rij het metagegevensmodel in het
   formulier wordt weergegeven.

-  **Vanaf kolom**\ : Dit geeft aan vanuit welke kolom het metagegevens
   model moet beginnen.

-  **Naar kolom**\ : Dit geeft aan achter welke kolom het metagegevens
   mode moet eindigen.

-  **Tekst voor label:**\  Dit geeft de labelwaarde van het metagegevens
   model weer. Als hier geen input wordt gegeven, zal ere en stadaard
   label voor het veld worden gebruikt.

-  **Verplicht**\ : Als dit is aangevinkt, betekent het dat de
   metagegevens model verplicht ingevuld moet worden.

-  **Alleen-lezen**\ : Als dit is aangevinkt, wordt het metagegevens
   model gezien als alleen-lezen op het formulier.

-  **Verwijder in kopie:**\  Dit geeft aan dat de gegevens in het
   metagegevens model worden verwijderd als deze worden gekopieerd.

-  **Tekstblok**\ : Als dit is aangevinkt, wordt het metagegevens model
   in een multiline tekstblok weergegeven.

-  **Hoogte tekstblok**\ : Dit geeft de hoogte van het tekstblok weer.
   Dit veld is optioneel en accepteert pixelwaarden.

-  **Breedte**\ : Dit geeft de breedte van het metagegevens model in de
   kolomcel weer. Dit is optioneel en accepteert pixelwaarden en
   percentagewaarden.

-  **Relatietype**\  –  Dit geeft een lijstaan met gerelateerde
   metagegevens-modellen van het geselecteerde metagegevens-model,
   waarvan de gebruiker een metagegevens model kan kiezen. Het
   geselecteerde metagegevens-model is degene waarvoor het formulier
   wordt gemaakt.

-  **Veelvuldige relatie – **\ Indien aangevinkt, kunnen er meerdere
   relaties worden toegestaan.

-  **CM-velden **\ – Dit geeft een lijst met cm-velden die beschikbaar
   zijn voor het geselecteerde metagegevens-model. De geselecteerde
   metagegevens-model is waarvoor het formulier wordt gemaakt.

-  **Veldnamen**\ : Dit biedt een lijst met velden. Beschikbare velden
   in de lijst zullen afhankelijk zijn van de gekozen tabel tijdens het
   maken van het formulier.

-  **Veldtype**\ : Dit veld is belangrijk voor de velden binnen de
   samenvoeging (aggregatie). Beide zorgen ervoor dat het veld ingevuld
   kan worden zoals aangegeven in de aggregatie-labels. Alleen een
   invoer zorgt ervoor dat een veld binnen de aggregatie ingevuld kan
   worden, zonder in de aggregatie-labels zichtbaar te zijn. **Rij**:
   Dit geeft aan in welke rij het formulier wordt weergegeven.

-  **Van kolom**\ : Dit geeft aan in welke kolom het veld moet beginnen.

-  **Naar kolom**\ : Dit geeft aan in welke kolom het veld moet
   eindigen.

-  **Orderkop**\ : Dit geeft aan wat de orderkop van een veld in een
   aggregatie is.

-  **Tekst voor label**\ : Dit geeft de labelwaarde van het veld aan.
   Als hier geen input wordt gegeven, wordt de standard label voor dit
   veld gebruikt.

-  **Verplicht**\ : Als dit aangevinkt is, betekent het dat dit veld
   verplicht ingevuld moet worden.

-  **Alleen-lezen**\ : Als dit aangevinkt is, wordt dit veld laten zien
   als “Alleen-lezen”.

-  **Knippen**\ : Dit geeft aan of de gegevens in het veld verwijderd
   worden als deze gekopieerd worden.

-  **Tekstblok**\ : Als dit aangevinkt is, wordt het tekstveld
   weergegeven als een multiline tekstblok.

-  **Hoogte tekstblok**\ : Dit geeft aan wat de hoogte is van het
   multiline tekstblok. Dit veld is optioneel en accepteert
   pixelwaarden.

-  **Breedte**\ : Dit geeft de breedte van het veld in de kolomcel aan.
   Dit veld is optioneel en accepteert pixelwaarden en
   percentagewaarden.

-  **Toon Thesaurus Detail Popup**\ : Als dit veld is aangevinkt, kan de
   gebruiker het veld alleen invullen met een waarde uit thesaurus, als
   ere en thesaurus beschikbaar is.

-  **Afhankelijk van**\ : Dit geeft aan dat gegevens die in dit veld
   zijn ingevoerd, afhankelijk zijn van andere gegevens uit een ander
   veld. Dit werkt alleen voor thesaurus velden.

-  **Datum type**\ : Dit geeft aan of het datumveld de vroegste datum of
   de laatste datum van de periode heeft.

 

**Tabbladen toevoegen**\ 

·         De gebruiker kan door op de tab-optie te klikken, tabbladen
slepen en neerzetten in de gewenste rij en deze over de rijen
uitspreiden.

-  De tabblad eigenschappen die rechts in het scherm worden weergegeven
   zijn: naam, vanaf rij, naar rij en volgorde.

·         De naam van het tabblad wordt weergegeven aan de linkerkant
van het formulier.

·         Een tweede tabblad moet grenzen aan het laatste tabblad van
het formulier.

·         Als de tab-grootte gewijzigd wordt door het aantal rijen aan
te passen, veranderen de tabbladen hieronder.

**Velden aan een tabblad toevoegen **\ 

·         De gebruiker kan een veld in een tabblad toevoegen en deze
tussen de vereiste kolommen spreiden.  

·         Het veld wordt gemarkeerd met een kleur waardoor gebruikers
gemakkelijk velden van elkaar kunnen onderscheiden en van de
verschillende elementen in het formulier.

·         De velden hebben een lijst met verschillende eigenschappen
zoals: veldnaam (vooraf geconfigureerd), veldtype (vooraf
geconfigureerd) en rij (vooraf geconfigureerd).

·         Formulier kolom – De “naar kolom” wordt automatisch
weergegeven, omdat deze binnen de tabel zelf gewijzigd kan worden.

·         Velden kunnen worden aangeduid als: voor invoer, voor weergave
of beide.

·         De volgorde van de presentatie in de koptekst van een
aggregatietabel kan worden aangeduid als een veld dat voor
“Alleen-lezen” bestemd is. ****

****

 

**Een aggregatie in het formulier toevoegen**\ 

 

-  De gebruiker kan een aggregatie in het formulier slepen en plaatsen.
   Van de bovenste sectie op het raster kan de positie van de aggregatie
   indien nodig worden veranderd.

**Veldgroepen toevoegen**\ 

-  Bovenin de formulierenbouwer kan de gebruiker kan een veldgroep in
   het formulier toevoegen; dit kan worden gesleept en geplaatst en de
   spanwijdte hiervan kan veranderd worden.

·         De naam van de veldgroep wordt altijd weergegeven aan de
rechterkant van het scherm.

 

**Toevoegen van een metagegevens-model in het formulier**

 

-  De gebruiker kan gerelateerde metagegevens-modellen in het formulier
   toevoegen.

-  De gerelateerde metagegevens-modellen kunnen geselecteerd worden via
   het uitvouw-menu met gerelateerde metagegevens-modellen en
   relatienamen.

-  Ook de presentative kan worden aangeduid als een enkele of meerdere
   relatie.

-  De gebruiker kan het CM-veld selecteren waarmee een verwant object op
   het formulier gepresenteerd wordt, vanuit een lijst dat beschikbaar
   is voor CM-velden en rollen voor andere velden.

 

.. raw:: html

   </div>
