.. raw:: html

   <div class="WordSection1">

**Content Management systeem**

****

 

In het linker venster worden de metagegevens-modellen waar de gebruiker
voor ingeschreven weergegeven. Door met de muis over een
metagegevens-model te zweven, worden de opties voor dat specifieke
metagegevens-model weergegeven, waaronder: relaties, annotatievelden,
formulieren, dynamische velden, CM-velden en E-depot. De gebruiker zal
geen nieuwe metagegevens-modellen kunnen toevoegen in het systeem, omdat
deze vooraf zijn geconfigureerd en beschikbaar zijn op basis van de
plannen die zijn aangevraagd.

**Gereedschapstip:**\  Voor het gemak om klikbare links te begrijpen,
kan de gebruiker de muis over elke knop laten zweven. Dit geeft dan de
namen van de knoppen weer.

**Mobiele weergave:**\  In mobile of tablet weergave, moet de gebruiker
het scherm aanraken om de metagegevens-modellen te bekijken.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pinvast-knop:
** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\ Zoals
de naam suggereert, wordt de pinvast-knop gebruikt om pagina’s op te
slaan. De opgeslagen pagina’s worden in een lijst weergegeven die in de
kop van het Atlantis dashboard te vinden is. ****

·         \ **Samenvouw-knop:**\  Met deze knop kan de gebruiker het
menu samenvouwen. Om het menu samen te vouwen, moet de gebruiker op de
linker pijl-knop drukken. Zodra het venster samengevouwen is, zal de
pijl naar rechts draaien. Door op de rechterpijl te drukken, kan de
gebruiker het menu weer uitvouwen.

.. raw:: html

   </div>

·         \ **Zoeken:**\  Dit veld staat de gebruiker toe
metagegevens-modellen in de lijst te zoeken. ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

·         \ **Bewerk-knoppen: **\ klip op het potloodpictogram naast de
naam van het metagegevens-model om de velden te bewerken.

.. raw:: html

   </div>

 

Hoe kan ik velden van metagegevens-modellen bewerken?  \ **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         Klik op het **potlood-**\ pictogram om de velden te bewerken

·         \ **Naam:**\  Bewerk de naam van het metagegevens-model.

·         \ **Webshopbeheer:**\  Selecteer de naam van het beheer in de
vervolgkeuzelijst waarin de instellingen voor bestellingen, applicaties
en de winkelwagen staan en waar objecten voor dit metagegevens-model
worden verwerkt.

.. raw:: html

   </div>

·         \ **Pictogram:**\  De gebruiker kan een afbeelding kiezen voor
een metagegevens-model vanuit het lokale systeem door op dit veld te
klikken.

·         \ `**Presentatie
profiel** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Presentation%20Profiles.docx>`__\ **:**\ 
Er verschijnt een vervolgkeuzelijst die verschillende soorten
presentatieprofielen weergeeft. De gebruiker is verplicht het
presentatieprofiel toe te wijzen dat geassocieerd wordt met
metagegevens-model.

·        
\ `**Segmenten** <../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Segments.docx>`__\ **:
**\ Hiermee wordt de labeling van bepaalde metagegevens-modellen
objecten beschreven. Segmenten zijn toegewezen aan
metagegevens-modellen. Selecteer het segment uit de vervolgkeuzelijst en
geef het toe aan het betreffende metagegevens-model.

 

**Selectievakjes:**

+--------------------------------------------------------------------------+
| .. rubric:: §  Navigeerbaar: Markeer als het metagegevens-model          |
|    beschikbaar is voor navigatie.                                        |
|    :name: navigeerbaar-markeer-als-het-metagegevens-model-beschikbaar-is |
| -voor-navigatie.                                                         |
|                                                                          |
| .. rubric:: §  Inclusief gekoppelde documenten: Markeer indien een       |
|    navigatieboom, de navigatie bevat gekoppelde documenten.              |
|    :name: inclusief-gekoppelde-documenten-markeer-indien-een-navigatiebo |
| om-de-navigatie-bevat-gekoppelde-documenten.                             |
|                                                                          |
| .. rubric:: §  Categorie schema: Objecten van het metagegevens-model     |
|    vormen een hiërarchische categorie.                                   |
|    :name: categorie-schema-objecten-van-het-metagegevens-model-vormen-ee |
| n-hiërarchische-categorie.                                               |
|                                                                          |
| .. rubric:: §  Zoekboom: Markeer als het metagegevens-model beschikbaar  |
|    is voor systematisch zoeken.                                          |
|    :name: zoekboom-markeer-als-het-metagegevens-model-beschikbaar-is-voo |
| r-systematisch-zoeken.                                                   |
|                                                                          |
| .. rubric:: §  Bestelomschrijving: Markeer als de documenten die in dit  |
|    metagegevens-model zijn opgenomen, voor reproductie beschikbaar zijn. |
|    :name: bestelomschrijving-markeer-als-de-documenten-die-in-dit-metage |
| gevens-model-zijn-opgenomen-voor-reproductie-beschikbaar-zijn.           |
|                                                                          |
| .. rubric:: §  Aanvraag beschrijving: Markeer als de gebruiker een het   |
|    metagegevens-model wil aanvragen.                                     |
|    :name: aanvraag-beschrijving-markeer-als-de-gebruiker-een-het-metageg |
| evens-model-wil-aanvragen.                                               |
|                                                                          |
                                                                          
+--------------------------------------------------------------------------+

 

·         \ **Auteursrechtelijk beschermde pictogrammen:**\  Door op dit
veld te klikken, kan de gebruiker het bestand met het symbool/pictogram
markeren als auteursrechtelijk beschermd. De gebruiker kan in een lokale
computer bladeren en zoeken naar het pictogram.

·         \ **Reset **\ 

§  Door te klikken op de **reset** knop, verschijnt het
bevestigingsvenster.

§  \ **Ja**\ : Door te klikken op ja, wordt u teruggebracht naar de
laatst opgeslagen positie.

§  \ **Annuleren**\ : Door te klikken op annuleren, wordt de
desbetreffende actie beëindigd.  

 

·         \ **Opslaan**\ 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.5in;margin-right:
   0in">

§  Deze knop slaat alle nieuwe en niet opgeslagen wijzigingen op. ****

****

 

.. raw:: html

   </div>

 

·         \ **Afsluiten**

§  Door te kikken op de **afsluit-**\ knop, verschijnt het
bevestigingsvenster: ****

§  \ **Ja: **\ Door te klikken op ja, wordt de pagina afgesloten.\ ****

§  \ **Annuleren: **\ Door te klikken op annuleren, blijft u op dezelfde
pagina.

.. raw:: html

   </div>
