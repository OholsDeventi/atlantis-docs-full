.. raw:: html

   <div class="WordSection1">

**Instellingen**

Dit is de pagina met de Systeem Instellingen van de Atlantis applicatie.
Het icoon van de systeem instellingen (tandwiel) vindt men op de header
van het Atlantis dashboard. Door op dit systeem icoon te klikken gaat de
gebruiker naar de instellingspagina.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Toevoeg knop: **\ Klik op de plus knop (+) om een
instelling toe te voegen.

.. raw:: html

   </div>

·         \ **Bewerk knop:**\  Klik op de **Potlood** knop om de
instellingen aan te passen.

·         \ **Verwijder knop: **\ Klik op de **Prullenbak** knop om een
instelling te verwijderen.\ ****

§  \ **Ja: **\ Als u op **Ja** klikt, wordt de instelling definitief
verwijderd.  \ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Annuleren**\ : Als u op **Annuleren** klikt, stopt het
verwijder-proces.  \ ****

.. raw:: html

   </div>

 

 

De gebruiker kan de systeem instellingen in de hieronder genoemde velden
aanpassen:

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Toevoegen – Aanpassen **

.. raw:: html

   </div>

**IP Range**

·         \ **IP\_Start: **\ Voeg het eerste IP-adres van het systeem
toe via waar de Atlantis applicatie toegankelijk is.

·         \ **IP\_End: Voeg**\  het laatste IP-adres van het systeem toe
via waar de Atlantis applicatie toegankelijk is.

·         \ **Uitsluiten: **\ Klik hier om een bepaald IP-adres uit te
sluiten.\ ****

·         \ **Sluit knop: **\ Klik hier om de rij te sluiten. ****

Opmerking: De aangepaste registers zullen worden opgeslagen en de rij
zal worden afgesloten als er tijdens het **aanpassen** op de sluit knop
wordt gedrukt. ****

****

 

**Segmenten**

·         Selecteer het segment uit de keuzelijst.\ ****

·         De gebruiker kan verschillende segmenten toevoegen of bewerken
voor de IP-adressen die men hierboven kan aangeven.\ ****

 

**Thesaurus instellingen**

·         Selecteer de taal uit de keuzenlijst die gebruikt moet worden
voor de synoniemenlijst-functies.

 

****

 

**Systeem Instellingen**

·         \ **Taal: **\ Selecteer de systeemtaal in de keuzelijst.

·         \ **Publiceer enkel gemodereerde documenten: **\ Klik hier om
ervoor te zorgen dat het systeem enkel gemodereerde documenten
publiceert.

·         \ **Geef automatisch “Niet gemodereerd” aan na wijzigingen**\ 

·         \ **Converteer automatisch het unieke identificatiekenmerk
naar hoofdletters**\ : Klik hier om de unieke identificatiekenmerken
automatisch naar hoofdletters om te zetten.

·         \ **Publiceer enkel gemodereerd commentaar:**\  Klik hier om
enkel gemodereerd commentaar te publiceren.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Toestaan modereren stamgegevens:**\  Klik hier om het
modereren van stamgegevens toe te staan.

.. raw:: html

   </div>

 

 

**Reset Knop**

Door op de **Reset** knop te drukken verschijnt een conformatie-scherm.
Dit scherm bevat de volgende opties:

·         \ **Ja**\ : Klik op de Ja knop om terug te keren naar de
laatst opgeslagen instellingen.\ ****

·         \ **Annuleren**\ : Klik op de Annuleren knop om de huidige
actie te staken.\ ****

**Opslaan knop                             **

Klik hier om alle nieuwe en niet opgeslagen veranderingen op te slaan.

 

 

 

 

 

.. raw:: html

   </div>
