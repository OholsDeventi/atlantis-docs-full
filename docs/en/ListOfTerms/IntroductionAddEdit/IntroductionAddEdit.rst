.. raw:: html

   <div class="WordSection1">

**List of terms**

The user can navigate to the current screen from the **global menu** on
the dashboard. Clicking on the link will direct the user to the page
“\ `**List Of
Terms** <http://dev-deventit.staging.easternenterprise.com/#/list-of-terms>`__\ ”.
Every Item listed on the page as follows: Table name.Column name:
‘ACT\_ACTOR.BRONNEN’ – is linked to the corresponding column name from
the database and a metadata model assigned for the item. All configured
terms on the page will be displayed in the left pane of the screen. The
user can click on the ‘Ⓣ’ icon of the particular selection list and can
see the corresponding terms for that selection list in the right pane
section.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:
** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of the Atlantis Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Search:**\  This field allows a user to search and select
the table name from the list.\ ****

 

·         \ **Add Button:**\  Click the **+** button to add terms.

·         \ **Delete: **\ Click the **Trash** icon to delete the terms
record.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

§  \ **Cancel**\ : Clicking the Cancel button will terminate the delete
action.\ ****

·         \ **Edit Button:**\  Click the **Pencil** button to edit terms
(Table name.Column name) record.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Tag button: **\ Click the **** Ⓣ button to tag the
selected item/term (Table name.Column name).

.. raw:: html

   </div>

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add-Edit Action (Click on ‘+’) for the following:**

.. raw:: html

   </div>

**• Table name – Column name:**\  Add a selection list item to the
pre-existing list of Tables & Columns in the database. The user can also
search for the related table name and expand it to select the
corresponding column name.

**• Non-existing term allowed:**\  The flag (check) allows to tag the
non-existing items listed in Tables & Columns.

**• Store non-existing:**\  The flag (check) allows to store the
non-existing items list of Tables & Columns in the database.

**• Metadata model:**\  Select the metadata models from the drop down
list.

****

 

**Reset Action**

Clicking the **Reset** button will display a confirmation pop-up window.
The pop-up window contains the following options:

·         \ **Yes**\ : Clicking the Yes button will take you back to the
last saved position.\ ****

·         \ **Cancel:**\  Clicking the Cancel button will terminate the
current action.\ ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white">

**Close button: **\ Click the **close** button to close the row while
adding or editing.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
