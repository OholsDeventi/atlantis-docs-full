#############
Authorization
#############


Introduction
~~~~~~~~~~~~


.. raw:: html

   <div class="WordSection1">

**Authorization**

On Atlantis Dashboard, the ‘Authorization’ thumbnail is visible in blue
color. A system administrator allows users to access the system and
provides the appropriate privileges for them. This screen displays two
sides: 1) user groups, and 2) users, respectively. Authorization
consists of assigning pre-defined user rights to \ `user
groups <UserGroup.docx>`__\ . By assigning users to user groups, users
are provided with the respective user rights.

**Tool Tip: **\ For the ease of understanding of clickables user can
hover the mouse pointer on each button which will display the name of
the button.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Group: **\ The User Group is a set of people who have similar
interests and goals towards the system. They have the same set of
permissions. The list of user groups is seen on the pane the most left.

.. raw:: html

   </div>

·         \ `**Pin-up button:
** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Like
the name suggests, the Pin-Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

 

·         \ **Collapse Pane Button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

-  **Preview Button: **\ It is the **Eye** icon on the user group pane.
   With the eye button all the users registered in the system are
   listed. Clicking on a user group will list all the users assigned to
   the \ `User name-group name <UserName-Group%20Name.docx>`__\ .

 

·         \ **Add Button: **\ Click on **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for user group in the
search space.

 

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : Click on **Yes** button will perform the delete action.

§  \ **No**\ : Click on **NO** button will terminate the delete action.

 

·         \ **Edit Button: **\ Click on **Pencil** icon to edit the user
group records.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:28.35pt;margin-right:0in">

·         \ **R Icon: **\ The R Icon stands for **Rights** button.

.. raw:: html

   </div>

 

 

 

 

How can I add a user group?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**\ 

.. raw:: html

   </div>

·         Click on **+** icon to perform the add actions.

·         \ **Name:**\  Provide the name of the user group in the text
field.

·         \ **Definition:**\  Describe the purpose of the user group in
the text field.

·         \ **New User:**\  The checkbox informs if newly registered
users are assigned to this group by default.

·         \ **Reset Action**\ 

§  A click on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : Aclick on the Cancel button will terminate the
respective action.

 

·         \ **Save Action**\ 

§  A click on the **save** button will save the changes made.

 

·         \ **Close button: **\ A click on the **close** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Click on the cancel button and you will remain on the
same page.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Name – Group Name: **\ The list with the heading ‘User Name –
Group Name’ displays the list of individual users along with their group
name, if they are associated with any of the groups.

.. raw:: html

   </div>

 

·         \ **Collapse Pane Button:**\  Click the **left arrow** button
to collapse the pane. Once the pane will collapse, the same arrow will
turn towards right. Click on the **right arrow** to expand the pane.

 

·         \ **Add Button: **\ Click on the **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for a user name in the
search space.

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

**Edit Button:**\  Click on the **Pencil** icon to edit the user name
records.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**\ 

.. raw:: html

   </div>

·         Click on **+** icon to perform the add actions.

·         \ **Name:**\  Provide the name of the user group in the text
field.

·         \ **Definition:**\  Describe the purpose of the user group in
the text field.

·         \ **New User:**\  The checkbox informs if newly registered
users are assigned to this group by default.

·         \ **Reset Action**\ 

§  A click on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : A click on the Cancel button will terminate the
respective action

 

·         \ **Save Action**\ 

·         This button saves any new or unsaved changes.\ ****

 

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ By clicking on the Cancel button you will remain on
the same page.\ ****

.. raw:: html

   </div>

 

 

 

 

What does the R icon stand for?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**R Icon**\ The R icon stands for various **Rights** of a user or group
of users. This will display the different rights available for the User
groups. User can either **select or deselect** rights for other user or
group.

 

.. raw:: html

   </div>

·         \ **Search: **\ User can also search for specific rights in
the search bar.\ ****

****

 

·         \ **Close button: **\ A click on the **cancel** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the yes button will close the page.\ ****

§  \ **Cancel: **\ Click on the cancel button and you will remain on the
same page.\ ****

·         \ **Reset Action:**\ 

Clicking on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : A click on the Cancel button will terminate the
respective action.

 

·         \ **Save Action:**\ 

§  A click on the **save** button will save the changes made.\ ****

****

 

·         \ **Close button: **\ A click on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the cancel button will keep you on the
same page.\ ****

****

 

****

 

 

 

How can I add individual users to authorize with different rights?

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

 

.. raw:: html

   </div>

-  The user can click on the **Add+** icon to add new users to the
   system.

-  **Username\*:**\  This field requires a distinct name of the user to
   avoid confusion. The field is unique and mandatory.

-  **Title:**\  This is the salutation of the user.

-  **Name:**\  This field requires the last name of the user.

-  **First Name:**\  This field requires the first name of the user.

-  **Address:**\  This field requires the full address of the user.

-  **Country:**\  Insert the country name of the address in the text
   field.

-  **Password\*:**\  Enter password for the user account. The field is
   unique and mandatory.

-  **Confirm Password:**\  Re-enter the above-mentioned password

-  **Postal Code:**\  Enter the postal number of the user, associated
   with above-mentioned address.

-  **Domicile:**\  Attach the residence proof of the user.

-  **Telephone Number:**\  Enter the preferred contact number of the
   user.

**E-mail address\*:**\  Enter the preferred e-mail address of the user.
The email address should be in abc@xyz format and the text field is
mandatory.

CHECK BOXES:\ ****

-  **Active:**\  User account active or not.

-  **On Account: **\ User can order reproduction and get invoices
   instead of direct payment

-  **Not registered:**\  For the user accounts of those who access
   without login.\ ****

-  **Send Login Credentials:**\  Share the login credentials with
   respective user, set by administrator, via provided email address.

to add segments and user group click on \ **+ **\ add button

-  **Segments:**\  Select the desired segment from the drop down list
   for the user, which define the scope of user access. The user can
   select multiple segments which will display as different chip.

·         \ **User Group:**\  Select the user group if a user is
supposed to be added in any group. User can only select one group from
the dropdown list.\ **Save Action**

§  A click on the **save** button will save the changes made.\ ****

§  Once the record is saved, it will display the **trash** button.\ ****

****

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will delete the added segment.

·         \ **Close button**\ : A **** ick on the **close** icon will
show the close confirmation pop-up window\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Click on the Cancel button and you will remain on the
same page.\ ****

.. raw:: html

   </div>

****

 

****

 

**Global Reset and Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Reset Action**\ 

.. raw:: html

   </div>

§  A click on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : A click on the Cancel button will terminate the
respective action.

 

·         \ **Save Action**\ 

§  A click on the **save** button will save the changes made.\ ****

**Close button **

·         A click on the **cancel** icon will show the close
confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>


UserGroups
~~~~~~~~~~

Add&Edit
--------


.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Group: **\ The User Group is a set of people who have similar
interests and goals towards the system. They have the same set of
permissions. The list of user groups is seen on the leftmost pane.

.. raw:: html

   </div>

·         \ **Collapse Pane Button:**\  Click the **left arrow** button
to collapse the pane. Once the pane will collapse the same arrow will
turn towards right. Click on the **right arrow** to expand the pane.

-  **Preview Button: **\ This is the **Eye** icon on the user group
   pane. With the eye button all users who are registered in the system
   are listed. Clicking a user group will list all the users assigned to
   the \ `User name-group name <UserName-Group%20Name.docx>`__\ .

 

·         \ **Add Button: **\ Click on **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for a user group in the
search space.

 

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

 

·         \ **Edit Button: **\ Click on the **Pencil** icon to edit the
user group records.

 

·         \ **R Icon: **\ The R Icon stands for the **Rights** button.

 

How can I add a user group?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**\ 

.. raw:: html

   </div>

·         Click on **+** icon to perform the add actions.

·         \ **Name:**\  Provide the name of the user group in the text
field.

·         \ **Definition:**\  Describe the purpose of the user group in
the text field.

·         \ **New User:**\  The checkbox informs if newly registered
users are assigned to this group by default.

·         \ **Reset Action**\ 

§  A click on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : Clicking on the Cancel button will terminate the
respective action

 

·         \ **Save Action**\ 

§  A click on the **save** button will save the changes made.

 

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   </div>


UserRights
----------


.. raw:: html

   <div class="WordSection1">

What does the R icon stand for?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**R Icon **\ stands for various **Rights** of a user or group of users.
This will display the different rights available for the User groups.
User can either **select or deselect** rights for other user or group.

 

.. raw:: html

   </div>

·         \ **Search: **\ User can also search for specific rights in
the search bar.\ ****

****

 

·         \ **Close button: **\ A click on the **cancel** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the yes button will close the page.\ ****

§  \ **Cancel: **\ Click on the cancel button and you will remain on the
same page.\ ****

 

·         \ **Reset Action:**\ 

Clicking on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : A click on the Cancel button will terminate the
respective action.

 

·         \ **Save Action:**\ 

§  A click on the **save** button will save the changes made.\ ****

****

 

·         \ **Close button: **\ A click on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>


Username-GroupName
~~~~~~~~~~~~~~~~~~


.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Name – Group Name: **\ The list with the heading 'User Name -
Group Name' displays the list of individual users along with their group
name, if they are associated with any of the groups.

.. raw:: html

   </div>

 

·         \ **Collapse Pane Button:**\  Click the **left arrow** button
to collapse the pane. Once the pane collapses the same arrow will turn
towards right. Click on the **right arrow** to expand the pane.

 

·         \ **Add Button: **\ Click on the **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for a user name in the
search space.

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

 

·         \ **Edit Button:**\  Click on the **Pencil** icon to edit the
user name records.

 

 

How can I add an individual user to authorize with different rights?

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

 

.. raw:: html

   </div>

-  The user can click on the **Add+** icon to add a new user to the
   system.

-  **Username\*:**\  This field requires a distinct name of the user to
   avoid confusion. The field is unique and mandatory.

-  **Title:**\  This is the salutation of the user.

-  **Name:**\  This field requires the last name of the user.

-  **First Name:**\  This field requires the first name of the user.

-  **Address:**\  This field requires the full address of the user.

-  **Country:**\  Insert the country name of the address in the text
   field.

-  **Password\*:**\  Enter password for the user account. The field is
   mandatory.

-  **Confirm Password:**\  Re-enter the above mentioned password

-  **Postal Code:**\  Enter the postal number of the user associated
   with above mentioned address.

-  **Domicile:**\  Attach the proof of residence of the user.

-  **Telephone Number:**\  Enter the preferred contact number of the
   user.

-  **E-mail address\*:**\  Enter the preferred e-mail address of the
   user. The field is mandatory

 

CHECK BOXES:\ ****

-  **Active:**\  User account active or not.

-  **On Account: **\ User can order reproductions and get invoices
   instead of direct payment

-  **Not registered:**\  For the user accounts of those who access
   without login.\ ****

-  **Send Login Credentials:**\  Share the login credentials with
   respective user set by the administrator, via provided email address.

to add segments and user group click on \ **+ **\ add button

-  **Segments:**\  Select the desired segments from the drop down list
   for the user which define the scope of user access. The user can
   select multiple segments which will display as different chip.

-  **User Group:**\  Select the user group if a user is supposed to be
   added in any group. A user can only select one group from the
   dropdown list.\ ****

·         \ **Save Action**

§  A click on the **save** button will save the changes made.\ ****

§  Once the record is saved, it will display the **trash** button.\ ****

****

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. Clicking on the trash icon will delete the added segment.

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   </div>


