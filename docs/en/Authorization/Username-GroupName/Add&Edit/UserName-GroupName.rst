.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Name – Group Name: **\ The list with the heading 'User Name -
Group Name' displays the list of individual users along with their group
name, if they are associated with any of the groups.

.. raw:: html

   </div>

 

·         \ **Collapse Pane Button:**\  Click the **left arrow** button
to collapse the pane. Once the pane collapses the same arrow will turn
towards right. Click on the **right arrow** to expand the pane.

 

·         \ **Add Button: **\ Click on the **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for a user name in the
search space.

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

 

·         \ **Edit Button:**\  Click on the **Pencil** icon to edit the
user name records.

 

 

How can I add an individual user to authorize with different rights?

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

 

.. raw:: html

   </div>

-  The user can click on the **Add+** icon to add a new user to the
   system.

-  **Username\*:**\  This field requires a distinct name of the user to
   avoid confusion. The field is unique and mandatory.

-  **Title:**\  This is the salutation of the user.

-  **Name:**\  This field requires the last name of the user.

-  **First Name:**\  This field requires the first name of the user.

-  **Address:**\  This field requires the full address of the user.

-  **Country:**\  Insert the country name of the address in the text
   field.

-  **Password\*:**\  Enter password for the user account. The field is
   mandatory.

-  **Confirm Password:**\  Re-enter the above mentioned password

-  **Postal Code:**\  Enter the postal number of the user associated
   with above mentioned address.

-  **Domicile:**\  Attach the proof of residence of the user.

-  **Telephone Number:**\  Enter the preferred contact number of the
   user.

-  **E-mail address\*:**\  Enter the preferred e-mail address of the
   user. The field is mandatory

 

CHECK BOXES:\ ****

-  **Active:**\  User account active or not.

-  **On Account: **\ User can order reproductions and get invoices
   instead of direct payment

-  **Not registered:**\  For the user accounts of those who access
   without login.\ ****

-  **Send Login Credentials:**\  Share the login credentials with
   respective user set by the administrator, via provided email address.

to add segments and user group click on \ **+ **\ add button

-  **Segments:**\  Select the desired segments from the drop down list
   for the user which define the scope of user access. The user can
   select multiple segments which will display as different chip.

-  **User Group:**\  Select the user group if a user is supposed to be
   added in any group. A user can only select one group from the
   dropdown list.\ ****

·         \ **Save Action**

§  A click on the **save** button will save the changes made.\ ****

§  Once the record is saved, it will display the **trash** button.\ ****

****

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. Clicking on the trash icon will delete the added segment.

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   </div>
