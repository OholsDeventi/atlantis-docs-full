.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**User Group: **\ The User Group is a set of people who have similar
interests and goals towards the system. They have the same set of
permissions. The list of user groups is seen on the leftmost pane.

.. raw:: html

   </div>

·         \ **Collapse Pane Button:**\  Click the **left arrow** button
to collapse the pane. Once the pane will collapse the same arrow will
turn towards right. Click on the **right arrow** to expand the pane.

-  **Preview Button: **\ This is the **Eye** icon on the user group
   pane. With the eye button all users who are registered in the system
   are listed. Clicking a user group will list all the users assigned to
   the \ `User name-group name <UserName-Group%20Name.docx>`__\ .

 

·         \ **Add Button: **\ Click on **+ button** to add the user
group records.

 

·         \ **Search Button: **\ User can search for a user group in the
search space.

 

 

·         \ **Delete Button: **\ The **trash** icon is the delete
button. A click on the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

 

·         \ **Edit Button: **\ Click on the **Pencil** icon to edit the
user group records.

 

·         \ **R Icon: **\ The R Icon stands for the **Rights** button.

 

How can I add a user group?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**\ 

.. raw:: html

   </div>

·         Click on **+** icon to perform the add actions.

·         \ **Name:**\  Provide the name of the user group in the text
field.

·         \ **Definition:**\  Describe the purpose of the user group in
the text field.

·         \ **New User:**\  The checkbox informs if newly registered
users are assigned to this group by default.

·         \ **Reset Action**\ 

§  A click on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : Clicking on the Cancel button will terminate the
respective action

 

·         \ **Save Action**\ 

§  A click on the **save** button will save the changes made.

 

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   </div>
