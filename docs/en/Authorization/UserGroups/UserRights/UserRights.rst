.. raw:: html

   <div class="WordSection1">

What does the R icon stand for?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**R Icon **\ stands for various **Rights** of a user or group of users.
This will display the different rights available for the User groups.
User can either **select or deselect** rights for other user or group.

 

.. raw:: html

   </div>

·         \ **Search: **\ User can also search for specific rights in
the search bar.\ ****

****

 

·         \ **Close button: **\ A click on the **cancel** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ A click on the yes button will close the page.\ ****

§  \ **Cancel: **\ Click on the cancel button and you will remain on the
same page.\ ****

 

·         \ **Reset Action:**\ 

Clicking on the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : A click on Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : A click on the Cancel button will terminate the
respective action.

 

·         \ **Save Action:**\ 

§  A click on the **save** button will save the changes made.\ ****

****

 

·         \ **Close button: **\ A click on the **close** icon will show
the close confirmation pop-up window.\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
