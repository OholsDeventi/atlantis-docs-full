.. raw:: html

   <div class="WordSection1">

**Content Management `System <>`__**\ `[PvD1] <#_msocom_1>`__\  \ ****

****

 

In the pane on the left, this screen will display the metadata models
the user has subscribed to.These metadata models are listed on the left
side of the screen. ` <>`__\ `Hovering the mouse
over <>`__\ `[PvD2] <#_msocom_2>`__\  \ `[AC3] <#_msocom_3>`__\   any
metadata model will display options for that particular metadata model,
like: \ `Relations <Relation.docx>`__\ ` <>`__\ ` <>`__\ ,
\ `Annotation <Annotation%20Field.docx>`__\  fields, Forms,
\ `Dynamic <Dynamic%20Field.docx>`__\  fields,
\ `CM-fields <CM%20Field.docx>`__\  and \ `E-depot <E-Depot.docx>`__\ .
\ `[PvD4] <#_msocom_4>`__\  \ `[AC5] <#_msocom_5>`__\  \ The user won't
be able to add new metadata models into the system, as they are
pre-configured and available on the basis of the plan subscribed to.

**Tool Tip:**\  For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

**Mobile View:**\  In mobile or tablet view the user needs to touch the
screen on Metadata Model.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pin up button:
** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on the right arrow to
expand the pane.

.. raw:: html

   </div>

·         \ **Search:**\  This field allows a user to search a metadata
model from the list.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

·         \ **Edit Button: **\ Click on the **pencil** icon next to the
name of a metadata model to edit the
fields\ ` <>`__\ `[PvD6] <#_msocom_6>`__\  .

 

.. raw:: html

   </div>

 

How can I edit t\ *he fields of the metadata model*?\ **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         Click on the **Pencil** button to **edit** the fields.

·         \ **Name:**\  Edit the name of the metadata model.

.. raw:: html

   </div>

·         \ `**Web shop
administration** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Webshop.docx>`__\ **:**\ 
Select the name of the administration from the dropdown list for which
settings for orders, applications and handling of the shopping cart are
set and processed for objects of this metadata
`model <>`__\ `[PvD7] <#_msocom_7>`__\  .

·         \ **Icon:**\  The user can choose an image for any metadata
model from the local system by clicking on this field.

·         \ `**Presentation
Profile** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Presentation%20Profiles.docx>`__\ **:**\ 
A dropdown list will appear, which will display the different kinds of
`presentation profiles <>`__\ `[PvD8] <#_msocom_8>`__\  . Of the user is
required to assign the respective presentation profile associated with
the metadata model.

·        
\ `**Segments** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Segments.docx>`__\ **:
**\ This describes the tagging of particular metadata model objects.
Segments are assigned to metadata models. Select the segment from the
dropdown list and assign it to the respective metadata model.

 

**Checkboxes:**

+--------------------------------------------------------------------------+
| .. rubric:: §  Navigable: Flag if the metadata model is available for    |
|    navigation.                                                           |
|    :name: navigable-flag-if-the-metadata-model-is-available-for-navigati |
| on.                                                                      |
|                                                                          |
| .. rubric:: §  Including Linked Documents: Flag if in a navigation tree, |
|    the navigation will include linked documents.                         |
|    :name: including-linked-documents-flag-if-in-a-navigation-tree-the-na |
| vigation-will-include-linked-documents.                                  |
|                                                                          |
| .. rubric:: §  Category Scheme: Objects of the metadata model form a     |
|    hierarchical category scheme.                                         |
|    :name: category-scheme-objects-of-the-metadata-model-form-a-hierarchi |
| cal-category-scheme.                                                     |
|                                                                          |
| .. rubric:: §  Search Tree: Flag if the metadata model is available for  |
|    systematic searching.                                                 |
|    :name: search-tree-flag-if-the-metadata-model-is-available-for-system |
| atic-searching.                                                          |
|                                                                          |
| .. rubric:: §  Description Orderable: Flag if the documents included in  |
|    this metadata model are available for reproduction.                   |
|    :name: description-orderable-flag-if-the-documents-included-in-this-m |
| etadata-model-are-available-for-reproduction.                            |
|                                                                          |
| .. rubric:: §  Description Requestable: Check the box if the user wants  |
|    to request for the metadata model.                                    |
|    :name: description-requestable-check-the-box-if-the-user-wants-to-req |
| uest-for-the-metadata-model.                                             |
|                                                                          |
|                                                                          |
+--------------------------------------------------------------------------+

 

·         \ **Copyright Protected icon:**\  Clicking this field will
allow the user to tag the file with the copyrighter’s symbol/icon. The
user can browse the icon from a local machine.

 

 

·         \ **Reset Action**\ 

§  Clicking the **Reset** button will show the confirmation pop-up
window.

§  \ **Yes**\ : Clicking the Yes button will take you back to the last
saved position.

§  \ **Cancel**\ : Clicking the Cancel button will terminate the
respective action.

 

·         \ **Save Action**\ 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.5in;margin-right:
   0in">

§  This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

 

·         \ **Close button**

§  A click on the **close** icon will show the close confirmation pop-up
window:\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__Where is the help button for this screen?

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_2" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_2','_com_2')"
   onmouseout="msoCommentHide('_com_2')">

` <>`__
 `[PvD2] <#_msoanchor_2>`__How is this done/work omn tablets and Phone?

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_3" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_3','_com_3')"
   onmouseout="msoCommentHide('_com_3')">

` <>`__
 `[AC3] <#_msoanchor_3>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_4" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_4','_com_4')"
   onmouseout="msoCommentHide('_com_4')">

` <>`__
 `[PvD4] <#_msoanchor_4>`__Links to the help pages for the respect
functions

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_5" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_5','_com_5')"
   onmouseout="msoCommentHide('_com_5')">

` <>`__
 `[AC5] <#_msoanchor_5>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_6" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_6','_com_6')"
   onmouseout="msoCommentHide('_com_6')">

` <>`__
 `[PvD6] <#_msoanchor_6>`__Link to the help page for editing the fields
of the metadata model definition.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_7" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_7','_com_7')"
   onmouseout="msoCommentHide('_com_7')">

` <>`__
 `[PvD7] <#_msoanchor_7>`__Should there be a link to the help page for
the administration page?

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_8" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_8','_com_8')"
   onmouseout="msoCommentHide('_com_8')">

` <>`__
 `[PvD8] <#_msoanchor_8>`__A link to the Presentation Profile help page

 

Links to the help where concepts are explained should be added wherever
the concept is used.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
