.. raw:: html

   <div class="WordSection1">

***CM Fields***

Content Management (CM) fields are used to configure various
functionalities in the system. They are used for various utilizations
like searching, sorting, filtering and presenting data in a detailed
manner. By this CM fields define how database information is used to
implement specific fields for searching, etc.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `Pin up button:
 <../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Collapse pane button:**\  Click on the **left arrow**
button to collapse the pane. Once the pane collapses, the same arrow
will turn towards right. Click on the **right arrow** to expand the
pane.

·         \ **Add button:**\  Click on the **+** button to add CM
fields.

**Filter Icon:**\  The filter function contains the various headers
within which the user wants to display the cm field. The types of CM
fields to be presented in the table can be selected by checking or
unchecking them. Existing CM fields are presented to the user in a
tabular format. The table can be filtered by the filter icon above the
table.

The filter contains the header, consisting of:

·         \ **Serial**\ : Serial number of the CM field

·         \ **Name: **\ Name of the CM Field\ ****

·         \ **Public: **\ Flag if CM field is for use in the public
environment.\ ****

·         \ **Management: **\ Flag if the CM field is for use in the
management environment.

·         \ **Sort: **\ Flag if the CM field is for sorting search
results.

·         \ **Search: **\ Flag if the CM field is for searching.

·         \ **Global: **\ Flag if the CM field is for all the metadata
models.

·         \ **Result: **\ Flag if the CM field is a search result
column.

·         \ **Icon: **\ Flag if the user wants to include icon in CM
field.

·         \ **Detail**\ : Flag if the CM field needs to be shown in
detail.

·         \ **Direct: **\ Flag if CM fields are added directly through
metapath. If the content of a CM field consists of just one or more
fields from the metadata models, the registration through direct field
applies.

·         \ **Filter: **\ Flag if the CM field is for filtering search
results.

·         \ **Xslt: **\ Go to the XSLT Editor to define/edit the CM
field definition.

**Sort Function on header:**

·         Each of the property column headers can be clicked to sort the
table on that particular column. Clicking again will turn the sorting.
\ ****

·         Each column has a **SORT** icon button beside its name.\ ****

·         Click on the sort icon to see records either in **ascending
order** or in **descending order**. ****

·         \ **Delete Button: **\ The **Trash** icon is the delete
button. Clicking the trash icon will show the confirmation pop-window.

§  \ **Yes**\ : Clicking the **Yes** button will perform the delete
action.

§  \ **No**\ : Clicking the **Cancel** button will terminate the delete
action.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Edit Button: **\ Click on the **Pencil** icon to edit the
CM field’s records.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
