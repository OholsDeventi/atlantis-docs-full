.. raw:: html

   <div class="WordSection1">

How can I add or edit CM Fields?\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Add or Edit Actions**

.. raw:: html

   </div>

·         Click on the **+** button to **add** or on the **Pencil**
button to **edit CM** fields.

·         \ **Name:**\  Name of the CM field. The text field is
mandatory.

·         \ **Label: **\ Insert the text the user wants to display.

·         \ **Serial number:**\  The number which is used to influence
the order of presentation in, for example, a search screen, sorting,
result column, etc.

·         \ **Weight: **\ Insert the priority number of the CM field. It
decides the ascending and descending order of a CM field.  \ ****

·         \ **Checkboxes:**

§  \ **Direct Fields: **\ a CM field can be a direct field when it
comprises of one or more fields from a metadata model. These can be
selected from a list of available MetaPaths.\ ****

-          A check mark on **Direct Field** will open another form to
add a MetaPath.\ ****

-          \ **MetaPath**\ **: **\ Select the MetaPath from the drop
down list.\ ****

-          \ **MetaField**\ **:**\  Select the MetaField from the drop
down list.\ ****

-          \ **Is\_From**\ **:**\  Flag if the metamodel is from another
path.\ ****

-          \ **ISTM: **\ Flag if the metamodel is to be added from
another path.\ ****

-          \ **Close button:**\  Click the close button to close the
MetaPath fields.\ ****

-          \ **Save: **\ The MetaPaths can be saved by the global SAVE
button at the end of the page.\ ****

****

 

-          \ **Delete Button: **\ The **Trash** icon is the delete
button. Clicking the Trash icon will show the confirmation pop-up
window.\ ****

o   \ **Yes**\ : Clicking the **Yes** button will perform the delete
action.\ ****

o   \ **No**\ : Clicking the **Cancel** button will terminate the delete
action.\ ****

****

 

****

 

-          \ **Save Action**

o   Clicking the **save** button will save the changes made. Please
note: the changes cannot be saved until all the related fields are
filled properly.\ ****

o   Once the changes are saved  the Pencil icon will be displayed to
**edit** and the Trash icon to **delete**.\ ****

****

 

§  \ **Sort Field: **\ Specifies whether the field will be used for
sorting or not.\ ****

§  \ **Search field: **\ Specifies whether the field will be used for
searching or not.\ ****

§  \ **General: **\ Specifies if the CM field is general.\ ****

§  \ **Public: **\ Flag if the CM field can be viewed by all the
users.\ ****

§  \ **Management: **\ Flag if the CM field is restricted to only
administrated users.\ ****

§  \ **Ascending: **\ Flag to display the CM field in ascending
order.\ ****

§  \ **Result Field:**\  Flag to display the result header in the CM
field.\ ****

§  \ **Icon Field:** Flag if icon field needs to be include in CM
field.\ ****

§  \ **Location Field:** Flag if the location of the CM field needs to
be included.\ ****

§  \ **Context Field: **\ Flag if the context needs to be included in
the CM field.\ ****

§  \ **Sort Filter On Name: **\ Flag if the sorting needs to be done on
the name of the CM field.\ ****

§  \ **Date: **\ Flag to include the date.\ ****

§  \ **Numeral:**\  Flag to add a number field.\ ****

§  \ **Text: **\ Flag to add a text field.\ ****

§  \ **Full Text: **\ Flag to add full text in the CM field.\ ****

§  \ **Relevance Field: **\ Flag to include those fields which are
relevant to the current CM field.\ ****

§  \ **Shortened Description: **\ Flag if required that the description
is short.\ ****

§  \ **Form Until And Including:**\  Flag if the user needs to include
the path.\ ****

§  \ **Standard Alphabetical: **\ This flag is used to navigate
alphabetically.\ ****

§  \ **Switch off Highlighting: **\ Flag to switch off the
highlighting.\ ****

§  \ **Filter:**\  Flag to include the filter function.\ ****

****

 

·         \ **Selection Query: **\ In this field the user needs to type
the table name and can select the column by expanding the table.\ ****

·         \ **Help Text:**\  Detailed information regarding the CM
fields is displayed when the mouse-pointer is hovered by the user.

 

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

****

 

****

 

****

 

**Save Action**

·         A click on the **save** button will save the changes made.
Please note: the changes will not be saved until all the related fields
are filled properly.\ ****

·         Once the changes are saved the Pencil icon will be displayed
to edit and the Trash icon to delete.\ ****

****

 

**Close button**

•        Clicking the **close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
