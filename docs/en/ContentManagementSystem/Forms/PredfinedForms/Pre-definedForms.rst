.. raw:: html

   <div class="WordSection1">

**Pre-Defined Forms**\ 

 

·         In the beginning, every metadata model will be assigned with a
set of forms. However, if needed, the user can also create their own
forms using the form builder.

·         The form is generally laid out as a grid and table format.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Pin up button:**\   As the name suggests, the Pin Up button is used
for pinning the pages. The pinned pages are displayed under the pinned
lists which are placed in the header of Atlantis Dashboard.

.. raw:: html

   </div>

**Name**\ : This will be the name of the form or in other words input
entered here will represent the form.

 

**URL**\ : This represents the URL of the pre-defined form.

 

**Standard:**\  This will mark the form as ‘Default’ for the selected
metadata model.

****

 

****

 

**Reset Action**

·         Click on **Reset** button will show the confirmation pop up
window.\ ****

·         \ **Yes**\ : Click on Yes button will take back to the last
saved position.\ ****

·         \ **Cancel**\ : Click on Cancel button will terminate the
respective action. ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the pencil button
and trash button.\ ****

****

 

**Close button**

•        Clicking on **close** icon will show the close confirmation pop
up window\ ****

§  \ **Yes: **\ Clicking on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking on cancel button will remain on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
