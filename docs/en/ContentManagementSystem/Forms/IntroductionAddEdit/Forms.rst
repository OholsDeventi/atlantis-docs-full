.. raw:: html

   <div class="WordSection1">

**Forms**\ 

 

·         The form builder allows the user to create forms on metadata
models. Every form has various elements integrated into it.

·         Initially, every metadata model will be assigned with a set of
forms. However, if needed, the user can also create his own forms using
the form builder.

·         The form is generally laid out as a grid and table format.

 

**Form Builder **\ 

 

·         The form builder makes it easier for users to drag and drop
fields on the form builder which consists of rows and columns.

·         The form, field, field group, aggregation and metadata models
are the elements of the form builder.

 

**Form List**\ 

 

·         Users can view forms by clicking on the forms within a
particular metadata model.

·         A list of all the forms will be shown, where the user can add
a new form from the top right section of the form list.

·         The user can search for forms from the top search barand edit
or delete a form by the action icons.

 

**Creating a Form**\ 

 

·         The user needs to assign a name for the form, enter a template
name, select a table from which the data need to be fetched for the form
and assign the form to particular user groups.

·         The user has to enter the following details before starting to
create new form(s)

·         \ **Name**\  – This will be the name of the form or in other
words the input entered here will represent the form.

·         \ **Template Name**\  – Here the user can provide a template
name for the form. The form to be build will be created as a template
which can be further used in Aggregation.

·         \ **Table Name**\  – This will consist of database tables
where the data will be stored. Tables will change according to the
chosen metadata model.

·         \ **User Group**\  – This will consist of lists of the user
groups already configured in the system. The user can select one or many
of the available user groups, according to his choice.

·         \ **Mark as Default**\  – This will mark the form as ‘Default’
for the selected metadata model.

 

 

**Adding a Form (Table) within the editor**\ 

·         The user can drag and drop a form element on the grid layout.
This works like a guideline and can be used as limits up to which the
elements can be added within that form.

·         The user can drag and expand the form element to a required
width or height in terms of rows and columns.

·         No elements can be added outside the span of the form.

·         The form properties are displayed on the right in the
**Properties** section.

**Elements of the Form Builder**

****

 

·         \ **Main Form**\ 

-  **Row**\ : This denotes the number of rows in the main form.

-  **Column**\ : This denotes the number of rows in the main form.

·         \ **Fields**\ 

-  **Field Name**\ : This will provide a list of fields. Available
   fields in the list will depend upon the chosen table while creating
   the form.

-  **Field Type**\ : This field is important for fields within an
   aggregation. Both ensure that the field can be filled in as shown in
   the aggregation labels. Only input ensures that the field can be
   populated within the aggregation, but not shown in the aggregation
   labels.

-  **Row**\ : This denotes in which row the field will be displayed in
   the form.

-  **From column**\ : This denotes from which column the field should
   begin.

-  **To column**\ : This denotes after which column the field should
   end.

-  **Order Header**\ : This will indicate what will be the header order
   of the field in an aggregation.

-  **Text for Label**\ : This will represent the label value for the
   field. If no input is provided, the default label is used for the
   field.

-  **Mandatory**\ : If checked, it means the field is required to be
   filled.

-  **Read Only**\ : If checked, the field will be shown as read only in
   the form.

-  **Clear in copy**\ : This denotes if the data in the field get
   deleted if copied.

-  **Text block**\ : If checked, the text field will be shown as a
   multiline text block.

-  **Height multiline**\ : This denotes what will be the height of the
   multiline text block. This field is optional and accepts pixel
   values.

-  **Width**\ : This denotes the width of the field in the column cell.
   This is optional and accepts pixel values and percentage values.

-  **Show Thesaurus Detail Popup**\ : If checked the user can fill the
   field by a value from a thesaurus only, if a thesaurus is available.

-  **Depending On**\ : It indicates that data entered in the field are
   dependent on other data of another field. It only works for thesaurus
   fields.

-  **Date Type**\ : This indicates whether the date field has the
   earliest date or the last date of the period.

-  **Date grouping**\ : This accepts the name for the group. This
   ensures that the date fields fall within the period so that date
   fields cannot be earlier than the earliest and later than the last.

·         \ **Field Group**\ 

-  **Name**\ : Here the name for the field group which represents the
   field group can be entered.

-  **From row**\ : This denotes from which row the field group should
   begin.

-  **To row**\ : This denotes after which row the field group should
   end.

-  Order: This denotes the order of the field group.

·         \ **Aggregation**

-  **Name**\ : Here the name for the aggregation can be entered.

-  **Row**\ : This denotes in which row the aggregation will be
   displayed in the form.

-  **From column**\ : This denotes from which column the aggregation
   should begin.

-  **To column**\ : This denotes after which column the aggregation
   should end.

-  **Template**\  – From here the user can choose a template which has
   already been created to be applied on the aggregation.

-  **Single Display **\ – If checked it will make sure that the fields
   are not repeated.

·         \ **Tabs**

-  **Name**\ : Here the name for the tab which will represent the tab
   can be entered.

-   

-  **From row**\ : This denotes from which row the tab should begin.

-  **To row**\ : This denotes on which row the tab should end.

-  **Order**\ : This denotes the order of the tab.

 

·         \ **Metadata Model**

-  **Name**\ : This will provide a list of metadata models from which
   the user can select any metadata model.

-  **Row**\ : This denotes in which row the metadata model will be
   displayed in the form.

-  **From column**\ : This denotes from which column the metadata model
   should begin.

-  **To column**\ : This denotes after which column the metadata model
   should end.

-  **Text For label:**\  This will represent the label value for the
   metadata model. If no input is provided, the default label is used
   for the field.

-  **Mandatory**\ : If checked, it means the metadata model is required
   to be filled.

-  **Read Only**\ : If checked, the metadata model will be shown as read
   only in the form.

-  **Delete in copy**\ : This denotes if the data in the metadata model
   gets deleted if it is copied.

-  **Text block**\ : If checked, the metadata model will be shown as a
   multiline text block.

-  **Height multiline**\ : This denotes what will be the height of the
   multiline text block. This field is optional and accepts pixel
   values.

-  **Width**\ : This denotes the width of the metadata model in the
   column cell. This is optional and accepts pixel values & percentage
   values

-  **Relation Type**\  – This will provide a list of related metadata
   models of the selected metadata model from which the user can choose
   one metadata model. The selected metadata model is the one for which
   the form is being created.

-  **Multiple Relation – **\ If checked, multiple relations can be
   allowed.\ ****

-  **CM Fields**\  – This will provide a list of CM fields available for
   the selected metadata model. The selected metadata model is the one
   for which the form is being created.

**Adding Tabs**\ 

·         The user can click on the tab option and drag and drop it to
the desired row and span it across rows.

-  The tab properties displayed on the right are: Name, From row, To row
   and Order.

·         The name of the tab will be displayed on the left side of the
form.

·         A second tab has to be adjacent to the last tab of the form.

·         If the tab size is changed by adjusting the number of rows,
the tabs below it will change accordingly.

****

**Adding fields to a Tab **\ 

·         The user can add a field within a tab and span it to the
columns required.

·         The field will be highlighted with a color that will allow
users to distinguish fields from other elements on the form.

·         The fields have a list of properties such as: Field Name
(pre-configured), Field Type (pre-configured) and Row (pre-configured)

·         The From column – To column is automatically displayed as this
can be changed within the table itself.

·         Fields can be denoted as for input, for display or both.

·         The order of the presentation in the header of an aggregation
table can be denoted if a field is for show only or both.

****

**Adding an aggregation to a form**\ 

 

-  The user can drag and drop an aggregation from the top section onto
   the grid and position or span as required.

**Adding Field Groups**\ 

 

-  The user can add a field group to the form from the top of the form
   builder; it can be spanned or dragged and dropped.

·         The name of the field group will always be displayed on the
right side of the screen.

 

**Adding a metadata model to the form**\ 

 

-  The user can add a related metadata model into the form.

-  The related metadata model can be selected from the drop down list of
   related metadata models and relationship names.

-  Also the presentation can be denoted as a single or multiple
   relation.

-  The user can select the CM field by which the related object will be
   presented on the form from the list of available CM fields and
   role(s) for the fields.

 

.. raw:: html

   </div>
