.. raw:: html

   <div class="WordSection1">

***E-Depots***

E-Depot shows the status of the application.
`**E-Depots** <>`__\ `**XSLT
editor**\ `[PvD1] <#_msocom_1>`__\   <../XSLT%20Editor/XSLT%20Editor.docx>`__\ :
It’s is an editor which allows a user to do changes or develop and add
new functionality in the E-Depot XSLT.

**E-Depot Account:** Check the box so that the entries developed in XSLT
will reflect in e-depots only.

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__There should be a link to the help file
describing the XSLT Editor funcitons.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
