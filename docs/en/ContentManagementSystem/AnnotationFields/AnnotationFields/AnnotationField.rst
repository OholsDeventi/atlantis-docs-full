.. raw:: html

   <div class="WordSection1">

***Annotation Field***

A user can add comments/annotations to objects of the desired metadata
model. For this, annotation fields are added and associated with the
**related metadata model in the left pane**. The user needs to hover
over the meta-models shown on the left side and select the 'annotation
fields' from the menu list. This action will display the list of the
annotation fields for a particular metadata model. The user may add
comments for the same. The description of the annotation field may be
anything, like the name of a document or person, a number, etc.

 

·         \ `Pin up button:
 <../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

****

 

·         \ **Close button: **\ Clicking the **close** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

****

 

·         \ **Search:**\  This field allows a user to search and select
an annotation field from the list.

 

·         \ **Add Button: **\ Click on the **+ button** to add the
annotation field records.

 

 

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;background:white">

**Add Action**

.. raw:: html

   </div>

·         Click on the **+** icon to add annotation fields.

·         \ **Description:**\  Insert the description or comments in the
**text field**. This text field is mandatory.

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : Clicking the **Yes** button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Click the **Cancel** button will terminate the
respective action. ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the Pencil button
and Trash button.\ ****

**Edit Action**

·         Click on the **Pencil button** to edit annotations.\ ****

·         Edit the **text field** of description of annotations.\ ****

****

 

**Delete Action**

·         Clicking the **Trash** icon will show the confirmation pop-up
window.\ ****

·         \ **Yes**\ : Clicking the **Yes** button will perform the
delete action.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **No**\ : Clicking the\ **Cancel** button will terminate the
delete action.\ ****

.. raw:: html

   </div>

 

**Close button**

•        Clicking the **close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Clicking the **Yes** button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the **Cancel** button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
