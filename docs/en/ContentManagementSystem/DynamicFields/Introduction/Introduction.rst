.. raw:: html

   <div class="WordSection1">

***Dynamic Field***

The dynamic field is created to add additional fields to the tables of
the metadata model. Their representation on forms depends on the field
type which is defined at creation.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:
** <../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Collapse pane button: **\ Click the **left arrow** button
to collapse the pane. Once the pane collapses, the same arrow will turn
towards right. Click the **right arrow** to expand the pane.

·         \ **Add Button: **\ Click the **+ button** to add the dynamic
field records.

·         \ **Search:**\  This field allows a user to search and select
the dynamic fields from the list.

·         \ **Delete Button: **\ The **trash** icon is the delete
button. Clicking the Trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : Clicking the **Yes** button will perform the delete
action.

§  \ **No**\ : Clicking the **Cancel** button will terminate the delete
action.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Edit Button: **\ Click the **Pencil** icon to edit the
dynamic field records.

 

.. raw:: html

   </div>

Dynamic fields are per selected metadata model. Add/edit dynamic fields
becomes available by clicking the “Dynamic Fields” option in the hoover
menu for a metadata model.

 

.. raw:: html

   </div>
