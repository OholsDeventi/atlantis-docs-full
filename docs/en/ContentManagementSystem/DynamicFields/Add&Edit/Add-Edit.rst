.. raw:: html

   <div class="WordSection1">

How ***can I add dynamic field***?\ ******

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

*Add Action: *

.. raw:: html

   </div>

·         \ *Click the **+** button to add the records of the dynamic
field. \* all fields are mandatory*

-  **Table:**\  The user can select the table from the drop down list
   which are populated from the backend. The field will be added in the
   selected table.

-  **Column name:**\  Provide the name of a new column in the table. No
   spaces are allowed in the column names.

-  **Field type:**\  The drop down list will show 4 type of fields, as
   follows:

§  \ **String:**\  This field creates a normal text field for the user.

§  \ **Bool:**\  Bool stands for Boolean and this indicates a field
containing a yes/no value in the form of 1 or 0. This field is always
one character long.

§  \ **Date:**\  This field will save a date. This field is always 8
characters long (yyyymmdd).

§  \ **Numeric:**\  This is a numeric field and can only contain
numbers. It’s not possible to insert numbers with commas here. This
field has a maximum length of 38 characters.

 

-  **Length Field: **\ Add **** the character length for the
   field.\ ****

-  **Help Text:**\  In this field the user is supposed to provide
   precise information about the new dynamic field which will be
   displayed when a user will hover over it. The maximum length is 50
   characters.

-  **Header Text:**\  This is the text used in the grid header, in case
   this is a field within an aggregation. Maximum length is 50
   characters.

-  **Label Text:**\  This text is displayed as a label above the field
   in the form. Maximum length is 50 characters.

-  **Error Text:**\  This text is displayed with the red exclamation
   mark if the field is missed or incorrectly filled e.g. a required
   field that has been left blank. The maximum length is 50 characters.

 

**Reset Action**

·         Clicking the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : Clicking the Yes button will take you back to the
last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
respective action. ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

****

 

**Close button**

•        Clicking the **close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
