.. raw:: html

   <div class="WordSection1">

***Relations***

It depicts the different ways in which a metadata model is related to
others. To see the different relations, the users can select a metadata
model on the left side of the screen. ”Relations” gets highlighted when
a user hovers the mouse on desktop view or `touches on mobile
view <>`__\ `[AC1] <#_msocom_1>`__\  \ on the nodes or a specific
relation. The nodes (circles in orange and yellow) are the metadata
models. The relations can be of 2 types; above (blue) or below (green).
Next to his, a relationship can be hierarchical (arrow). Hierarchical
relations can be considered as primary relations which are always
presented when navigating related objects. Navigating on hierarchical
relations is optional.

There can also be an ownership relation between metadata models (bold).
This implies that objects of the owned metadata models can’t exist
without objects of the owning metadata models.

Any change made in the hierarchy or position (above/below) of relations,
may reflect to changes in navigation through the connected data objects.

`**Pin up button:
** <../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.

+--------------------------------------------------------------------------+
| **Orange**\  - Depicts the selected node. Selecting a node breaks it up  |
| into - "add relation above"                                              |
|                                                                          |
|                  and "add relation below".                               |
|                                                                          |
|                                                                          |
|                                                                          |
| **Blue**\       - Depicts the relation which is above the selected       |
| (current) node.                                                          |
|                                                                          |
|                                                                          |
|                                                                          |
| **Green**\    - Depicts the relation which is below the selected         |
| (current) node.                                                          |
|                                                                          |
|                                                                          |
|                                                                          |
| **Arrow**\    - Depicts a hierarchical relationship                      |
|                                                                          |
|                                                                          |
|                                                                          |
| **Bold**\        - Depicts an ownership relation                         |
|                                                                          |
|                                                                          |
+--------------------------------------------------------------------------+

 

**Arrows:**\  On the relation page the user can see the four arrows
towards the lower left end. The user can shift the relation map for
better view by clicking on the respective directional arrow.

**Zoom in – Zoom out button:**\  On the relation page the user can see
the zoom in and zoom out button towards the lower right end of the page.
The user can enlarge  the relation map by clicking the zoom in button.
The user can diminishthe relation map by clicking the zoom out button.
Scrolling of the mouse wheel can also give the zoom in – zoom out
effect.

**Standard view:**\  On the relation page the user can see the standard
view button, just above the zoom in button towards the lower right end
of the page. The user can click the standard view button to display the
relation map in default view.

**Drag function:**\  The user can drag the relation map in the desired
direction by dragging it through left click of the mouse.

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[AC1] <#_msoanchor_1>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
