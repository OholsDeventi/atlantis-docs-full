.. raw:: html

   <div class="WordSection1">

How can I edit the relations of metadata models?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add-Edit Action**

.. raw:: html

   </div>

·         Users can add the relationship by clicking on the orange color
node.\ ****

·         A first click on any node will depict the selection of the
node in orange color.\ ****

·         Then again a click on the selected node will display:\ ****

·         \ **+**\  **> and +<** Add buttons: Click one of the **Add**
buttons to add a relationship above or below the current metadata
model.\ ****

·         Users can edit a relationship by clicking the relationship
lines and select the **pencil** icon.\ ****

·         Click the **Pencil** button to edit the relation
records.\ ****

·         \ **Delete Button: **\ The **trash** icon is the delete
button. Clicking the trash icon will show the confirmation pop-up
window.

§  \ **Yes**\ : Clicking the **Yes** button will delete the relation.

§  \ **No**\ : Clicking the **Cancel** button will terminate the delete
action.

 

·         To close the edit pop-up the user can click the **close**
button in the right corner\ **.**

`\*\ ***To change the current meta data model:*** *Click the
node.* <>`__\ `[PvD1] <#_msocom_1>`__\  \ ******

-  Click the **Pencil** button to edit the relation records

-  **Metadata models:**\  As the metadata model are pre-configured, the
   user can chose any of the available models from the drop down list.
   If the user wants a new metadata model in the list, `the
   plan <>`__\ `[PvD2] <#_msocom_2>`__\  \ should be changed
   accordingly. This is a mandatory field.

-  **Tab sheet forth:**\  Select the tab sheet from the drop-down list.
   This tab sheet is presented for the metadata model selected on the
   left.

-  **Tab sheet back:**\  Select the tab from drop-down list. This tab
   sheet is presented for the metadata model selected on the right.

-  **URL forth:**\  This link is to a specific screen replacing the
   standard search form when selecting the metadata model objects to
   connect to. Connections are made from objects of metadata model
   selected on the right to objects of the metadata model selected on
   the left.

-  **URL back:**\  This link is to a specific screen replacing the
   standard search form when selecting metadata mode objects to connect
   to. Connections are made from objects of the metadata model selected
   on the left to objects of the metadata model selected on the right.

-  **Relation name forth:**\  Name of the relation from the metadata
   model selected on the right to the metadata model selected on the
   left.

-  **Relation name back:**\  Name of the relation from the metadata
   model selected on the right to the metadata model selected on the
   left.

-  **Max number forth:**\  Total number of relations from the metadata
   model selected on the right to the metadata model selected on the
   left. 0 implies unlimited number of relations. This is a mandatory
   field.

-  **Max number back:**\  Relations from the metadata model selected on
   the left to the metadata model selected on the right. 0 implies
   unlimited number of relations. This is mandatory field.

 

·         \ **Checkboxes**

§  \ **Is Hierarchical: **\ Flag if the relation is in hierarchy.\ ****

§  \ **Is Owner: **\ Flag if the node is a parent node.\ ****

§  \ **Copy Forth: **\ Flag if the relation can be copied forward.\ ****

§  \ **Index Forth:**\  Flag for a forward index.\ ****

§  \ **Use for Navigation:**\  Flag if the connection with other
metadata models can also be seen.\ ****

§  \ **Is Multimedia:**\  Flag if it carries a multimedia file.\ ****

§  \ **Hide Relation In Tab: **\ Flag if the user wants to hide the
relation tab.\ ****

§  \ **Copy Back:**\  Flag if the user wants to display the back
relation.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.5in;margin-right:0in">

§  \ **Index Back: **\ Flag for a backward index.\ ****

****

 

.. raw:: html

   </div>

******

 

******

 

******

 

**Reset Action**

·         Clicking the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : Clicking the Yes button will take you back to the
last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
respective action. ****

****

 

**Save Action**

·         This button saves any new or unsaved changes.\ ****

****

 

******

 

**Close button**

•        Clicking the **close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

**Cancel: **\ Clicking the Cancel button will keep you on the same page.

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__This should be a link to the help page
describing this function.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_2" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_2','_com_2')"
   onmouseout="msoCommentHide('_com_2')">

` <>`__
 `[PvD2] <#_msoanchor_2>`__Link to the plan changing help page.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
