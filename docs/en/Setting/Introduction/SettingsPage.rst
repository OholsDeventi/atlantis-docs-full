.. raw:: html

   <div class="WordSection1">

**Settings**

This is the System Settings page of the Atlantis application. The system
setting icon (gear-wheel) can be found on the Atlantis dashboard’s
header. Clicking on the settings icon will take the user to the settings
page.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Add button: **\ Click on the plus (**+**) **** button to
add the settings.

.. raw:: html

   </div>

·         \ **Edit button:**\  Click on the **Pencil** button to edit
the settings.

·         \ **Delete button: **\ Click on the **Trash** icon to delete
the system settings record.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on Cancel will terminate the current
action.\ ****

.. raw:: html

   </div>

 

 

The user can change the system settings in the fields mentioned below:

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add – Edit Action**

.. raw:: html

   </div>

**IP Range**

·         \ **IP\_Start**\ **: **\ Insert the first IP address of the
system to which the Atlantis application is accessible.

·         \ **IP\_End**\ **:  **\ Insert the last IP address of the
system till where the Atlantis application is accessible.

·         \ **Exclude: **\ Click this to exclude any particular
IP.\ ****

·         \ **Close button: **\ Clicking on the close button will close
the row. ****

Note: Clicking on the close button while **editing,** will save the
edited record and will also close the row.\ ****

****

 

**Segments**

·         Select the segments from the drop down list. ****

·         The user can add or edit different segments for the different
range of IPs which can be defined above.\ ****

 

**Thesaurus settings**

·         Select the preferred language from the dropdown list for the
thesaurus functions.

 

****

 

**System setting**

·         \ **Language:  **\ Select the preferred language from the
dropdown list to select the system language.

·         \ **Publish moderated document only:**\  Choosing this flag
will publish only the moderated documents in the system.

·         \ **Put on not moderated automatically after alteration:
**\ Translation Pending

·         \ **Convert unique identification in capitals
automatically**\ : Choosing this flag will convert the unique
identification labels in capitals.

·         \ **Publish moderated annotations only:**\  Choosing this flag
will publish only those annotations which are moderated.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Moderation on master data:**\  Choosing this flag will
allow moderation on the master data.

.. raw:: html

   </div>

 

 

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window. The pop-up window contains the following options:

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button                                **

·         This button saves any new or unsaved changes.\ ****

 

 

 

 

 

 

 

 

.. raw:: html

   </div>
