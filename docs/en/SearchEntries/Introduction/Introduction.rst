.. raw:: html

   <div class="WordSection1">

**Search Entries**

Metadata model objects can be searched by search entries. These are
offered to users of the management and public environment. Search
entries are hierarchical, offering users options to search the system
more general or more specific. 

Search entries can be marked as also available for the public. If not
marked, they are only for the management environment. Search entries for
public and management environments are colored in their own way to
distinguish them.

On every level of search entries, metadata models can be assigned to
determine the scope of the search by that search entry.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin up button: **\  Clicking on pin button will pin the
page and it will show under the pinned list displayed at header of the
dashboard of Atlantis.

.. raw:: html

   </div>

·         \ **Search:**\  This field allows a user to search object
entries from the list.\ ****

·         \ **Add button:**\  Click the plus (**+)** button to add the
download resolution fields.

·         \ **Delete: **\ Click on the **Trash** icon to delete the
download resolution fields.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on Cancel will terminate the delete
action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
