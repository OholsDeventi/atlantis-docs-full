.. raw:: html

   <div class="WordSection1">

**Search Entries**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

.. raw:: html

   </div>

·         Click on the **+** button to add the object in search
entries.\ ****

·         \ **Name:**\  This field is mandatory and it takes the search
entry name.

·         \ **Name search screen:**\  User may provide the name of a
specific search screen.

·         \ **Specific search screen: **\ User may provide a specific
search screen.

·         \ **Parent Group:**\  This field provides the drop down list
of parent group that means the group where user needs to link their
search entry.

·         \ **For The Public:**\  The checkbox will allow the public
view of a particular search entry otherwise it will be only visible in
the management environment only.

**Reset Action**

§  Click on **Reset** button will show the confirmation pop up
window.\ ****

§  \ **Yes**\ : Click on Yes button will take back to the last saved
position.\ ****

§  \ **Cancel**\ : Click on Cancel button will terminate the respective
action. ****

**Save Action**

·         Click on **save** button will save the changes which are
made.\ ****

**        **

**Close button**

•        Click on **cancel** icon will show the close confirmation pop
up window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

.. raw:: html

   </div>

****

 

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Parent Group: **

.. raw:: html

   </div>

In the left pane the screen displays the search tree of parent group.
All parent group is represented by **+** button. Once user click on
**+** button it expands into child entries of the parent group.

In order to do grouping of search entries user needs to drag and drop
the search entry over to the desired one.

Grouping of metadata model is also done by drag and drop action over to
the desired metadata.

User needs to **left** click on the parent group cell which will display
menu in order to **edit button, delete entry, add metamodel** and **add
new child entry.**

 

**Edit Action**

·         Click on **pencil** button to edit the search entry.

·         \ **Name:**\  This field is mandatory and it takes the search
entry name.

·         \ **Name search screen:**\  User may provide the name of a
specific search screen.

·         \ **Specific search screen: **\ User may provide a specific
search screen.

·         \ **Parent Group:**\  This field provides the drop down list
of parent group that means the group where user needs to link their
search entry.

·         \ **For The Public:**\  The checkbox will allow the public
view of a particular search entry otherwise it will be only visible in
the management environment only.

 

·         \ **Reset Action**\ 

§  Click on **Reset** button will show the confirmation pop up
window.\ ****

§  \ **Yes**\ : Click on Yes button will take back to the last saved
position.\ ****

§  \ **Cancel**\ : Click on Cancel button will terminate the respective
action. ****

·         \ **Save Action**

§  Click on **save** button will save the changes which are made.\ ****

**        **

·         \ **Close button**

§  Click on **cancel** icon will show the close confirmation pop up
window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

**Delete Action**

•        Click on **trash** icon to delete the search entry
record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Click on cancel button will terminate the delete
action.\ ****

.. raw:: html

   </div>

****

 

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**MetaModel**

.. raw:: html

   </div>

·         Click on Metamodel button will land up to the **Assign
metamodel** page.\ ****

·         The users are needed to assign metadata models with the parent
group. The metadata model will determine its respective objects that
needs to be searched.\ ****

****

 

·         \ **Reset Action**\ 

§  Click on **Reset** button will show the confirmation pop up
window.\ ****

§  \ **Yes**\ : Click on Yes button will take back to the last saved
position.\ ****

§  \ **Cancel**\ : Click on Cancel button will terminate the respective
action. ****

·         \ **Save Action**

§  Click on **save** button will save the changes which are
made.\ **      **

·         \ **Close button**

§  Click on **cancel** icon will show the close confirmation pop up
window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

.. raw:: html

   </div>

****

 

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**New Entry**

.. raw:: html

   </div>

This entry will allow a user to add child search entry into the selected
parent group.

·         \ **Name:**\  This field is mandatory and it takes the search
entry name.

·         \ **Name search screen:**\  User may provide the name of a
specific search screen.

·         \ **Specific search screen: **\ User may provide a specific
search screen.

·         \ **Parent Group:**\  This field provides the drop down list
of parent group that means the group where user needs to link their
search entry.

·         \ **For The Public:**\  The checkbox will allow the public
view of a particular search entry otherwise it will be only visible in
the management environment only.

·         \ **Reset Action**\ 

§  Click on **Reset** button will show the confirmation pop up
window.\ ****

§  \ **Yes**\ : Click on Yes button will take back to the last saved
position.\ ****

§  \ **Cancel**\ : Click on Cancel button will terminate the respective
action. ****

·         \ **Save Action**

§  Click on **save** button will save the changes which are made. ****

·         \ **Close button**

§  Click on **cancel** icon will show the close confirmation pop up
window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
