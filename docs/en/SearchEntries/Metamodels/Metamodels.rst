.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**MetaModel**

.. raw:: html

   </div>

·         Click on Metamodel button will land up to the **Assign
metamodel** page.\ ****

·         The users are needed to assign metadata models with the parent
group. The metadata model will determine its respective objects that
needs to be searched.\ ****

****

 

·         \ **Reset Action**\ 

§  Click on **Reset** button will show the confirmation pop up
window.\ ****

§  \ **Yes**\ : Click on Yes button will take back to the last saved
position.\ ****

§  \ **Cancel**\ : Click on Cancel button will terminate the respective
action. ****

·         \ **Save Action**

§  Click on **save** button will save the changes which are
made.\ **      **

·         \ **Close button**

§  Click on **cancel** icon will show the close confirmation pop up
window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
