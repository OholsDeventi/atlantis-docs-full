﻿.. raw:: html

   <div class="WordSection1">

**Index**

This is section will display the actual records indexes. Each record in
the index will follow the format of the assigned index type. To view the
details the user needs to click on the index which is shown in the list
towards the left side of the screen. The user can reach the index type
from the \ `**Global Menu** <../Global%20Menu.docx>`__\ **.**\  The icon
of the Global Menu is on the rightmost corner of the screen (button
right of the profile picture).

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pin up button:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on the right arrow to
expand the pane.

 

·         \ **Add button:**\  Click on **+** to add index records.

****

 

·         \ **Search:**\  This field allows a user to search and select
the indexes from the list.\ ****

·         \ **Delete Action**

Click on the **Trash** icon to delete the records of indexes.\ ****

§  \ **Yes: **\ A click on Yes will delete the record.\ ****

§  \ **Cancel**\ : A click on the Cancel button will terminate the
delete action.\ ****

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Edit button: **\ Click on **Pencil** icon to edit the
indexes records.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
