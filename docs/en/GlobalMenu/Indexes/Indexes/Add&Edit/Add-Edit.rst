.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Add or Edit action**

.. raw:: html

   </div>

·         Click on the add **+** button or the **Pencil** button to
perform the desired action.

·         \ **\*All fields are mandatory.**

·         \ **Title:**\  This field requires the title of the index.

·         \ **Place:**\  This field requires the place of the records of
the index.

·         \ **Kind:**\  The drop-down list allows the user to select the
respective index type of the index.

·         \ **Start date:**\  Select the start date of the records from
the date picker.

·         \ **End date:**\  Select the end date of the records from the
date picker.

****

 

**      Reset Action**

·         Clicking on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action.\ ****

****

 

**      Save Action**

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

****

 

**Close button**

•        A click on the **Cancel** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
