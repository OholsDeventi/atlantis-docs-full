.. raw:: html

   <div class="WordSection1">

**Index Type**

`Indexes <Index.docx>`__\  are secondary accesses to archives. Through
indexes, access to archives is given by a different viewpoint like
persons, addresses, descriptions of acts and registers.

Every index type has some fixed fields, like identification, date,
persons, locations and publicity restraints. For every index type, 20
free fields can be added, named and given a type like Number, Date,
Yes/No or Text. The user can reach the index type from the \ `**Global
Menu** <../Global%20Menu.docx>`__\ **.**\  The icon of the Global Menu
is on the rightmost corner of the screen (button right of the profile
picture).

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

·         \ `**Pin up button: ** <../Pin%20Button.docx>`__\  Like the
name suggests, the Pin-Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on the right arrow to
expand the pane.

 

·         \ **Search:**\  This field allows a user to search and select
the index type from the list.\ ****

****

 

·         \ **Add button: **\ Click on **+ button** to add records of
index type.\ ****

****

 

·         \ **Delete button:**\  The **Trash** icon is the delete
button. A click on the Trash icon will show the confirmation pop-up
window.\ ****

§  \ **Yes**\ : A click on the **Yes** button will perform the delete
action.

§  \ **No**\ : A click on the **NO** button will terminate the delete
action.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

·         \ **Edit button: **\ Click on the **Pencil** icon to edit the
index type.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
