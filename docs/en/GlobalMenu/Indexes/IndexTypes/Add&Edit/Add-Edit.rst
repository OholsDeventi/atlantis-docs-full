.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add or Edit action**

.. raw:: html

   </div>

·         A list of preconfigured index types will be seen towards the
left side of the screen

·         Click either on the add **+** button or the **Pencil** button
to perform the required action

·         \*All fields are mandatory.

·         \ **Metadata Model:**\  Select the metadata model from the
drop-down list

·         \ **Description:**\  Insert the description of the index type.

·         \ **Standard Typing:**\  Select the publicity constraint from
the drop-down list.

·         \ **Requestable**\ : Flag if documents from this index type
can be requested for inspection.

 

·         \ **Orderable**\ : Flag if a reproduction of the documents
from this index type can be ordered.

·         \ **Field Number:**\  A user can select as many as 20 fields
with the index from the drop down list.

·         \ **Field Name:**\  Insert the field name in the text field.

·         \ **Field Type:**\  Type of the free field, like Number, Date,
Yes/No or Text

·         \ **+ Icon: A c**\ lick on the **+** icon at the end of every
row will allow the user to add a new row. With the **+** button at the
end of a free field, a new free field can be added up to 20 free
fields.\ ****

·         \ **Delete button: **\ Click on the **Trash** icon to delete
the row. With the trash button for every free field, a free field can be
deleted.\ ****

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action.\ ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

****

 

**Close button**

•        A click on the **Close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
