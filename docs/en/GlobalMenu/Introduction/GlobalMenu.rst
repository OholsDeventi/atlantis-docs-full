.. raw:: html

.. raw:: html

   <div class="WordSection1">

\*\*Global Menu\*\*\\ ·         The global menu icon can be found on the
extreme right of the screen. By clicking the icon, the user can see
various functionalities that are offered by the Atlantis application.
The Global menu allows the user to navigate to the items that are
individually set up and that are not linked to the Dashboard. ·        
The global menu is divided into 5 parts: \*\*1.      General\*\* ¨     
Download Resolution ¨      Copyright Holders ¨      Presentation
Profiles ¨      Annotation Types ¨      Media Types ¨      Packaging
¨      Tabs ¨      Segments ¨      Open Data ¨      Translations  
\*\*2.      Theme Configuration\*\* ¨      Themes   \*\*3.     
Webshop\*\* ¨      Administrations ¨      Discounts ¨      Reproduction
¨      Publication   \*\*4.      Thesaurus\*\* ¨      Configuration
¨      Browser \*\*\*\*   \*\*5.      Indexes\*\* ¨      Index Types
¨      Indexes \*\*Which parts are available depends on licenses and/or
user rights.\*\*\\ .. raw:: html

.. raw:: html

   </div>
