.. raw:: html

   <div class="WordSection1">

**Themes**

Like the name suggests, this function allows the user to change the look
and feel of the user interface. The users can configure the theme by
navigating to these screens from the Global Menu. The templates are
predefined and cannot be deleted by the user, although the user may
replicate the predefined theme and edit it manually. All the
preconfigured themes will be displayed in the left pane of the screen.
The theme which is highlighted in green color depicts the current theme.
The user can apply the theme only by clicking on ‘Apply' button. The
user can also change the font size and font style of the system.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin Up button**\ : Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

.. raw:: html

   </div>

 

·         \ **Collapse Pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

·         \ **Add Button:**\  Click on **+** button to add themes
records.

 

·         \ **Search:**\  This field allows a user to search and select
the theme from the list.

 

·         \ **Edit Button:**\  Click on **Pencil** button to edit themes
records.\ ****

·         \ **Delete: **\ Click on **Trash** icon to delete the
theme.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

§  \ **Cancel**\ : Click on cancel button will terminate the delete
action.\ ****

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **All fields are mandatory to fill or edit in themes.**

.. raw:: html

   </div>

 

.. raw:: html

   </div>
