.. raw:: html

   <div class="WordSection1">

****

 

**How can I Add or Edit the themes?**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Button (+) or Edit button (Pencil)**

.. raw:: html

   </div>

•        \ **Name:**\  Enter the name of the theme.

•        \ **Logo:**\  A click over this field will allow user to browse
the icon from local machine.

 

 

**Color**

•        \ **Primary Color:**\  Any selection over this field will
change the color of the header and buttons.

•        \ **Secondary Color:**\  Any selection over this field will
change the color of the breadcrumbs.

•        \ **Tertiary Color:**\  Any selection over this field will
change the color of columns.

•        \ **Icon Color:**\  Any selection over this field will change
the color of icons.

•        \ **Font Color: **\ Any selection over this field will change
the color of fonts.

.. rubric:: Typography
   :name: typography

•        \ **Primary Font:**\  Select the font style from the drop-down
list. This style of fonts will reflect in header and buttons.

•        \ **Primary Font Size:**\  Select the font size from the
drop-down list. This size of fonts will reflect in header and buttons.

•        \ **Secondary Font Size:**\  Select the font size from the
drop-down list. This size of fonts will reflect on breadcrumbs.

•        \ **Tertiary Font Size:**\  Select the font size from the
drop-down list. This size of fonts will reflect in columns.

•        \ **Body Text Font Size:**\  Select the font size from the
drop-down list. This size of fonts will reflect in the main content of
the system.

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the options below:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

****

 

**Save Button**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
