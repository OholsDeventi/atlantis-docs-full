.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Shipping Method**

.. raw:: html

   </div>

This section will display the delivery method of an item and its cost.
**** The section informs about how the item/article will be delivered to
the user and it also displays the cost involved for the method of
delivery. The administrator will land on the list of items and is able
to search for the specific shipment method.\ ****

The administrator can either add (with the **+** button) new surcharges,
edit them (with the **pencil** button) or delete them (with the
**trash** button). Add/edit of surcharges presents the following fields:

 

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

·         \ **Add Button:** Click on the **+** button to add a shipping
method record.\ ****

·         \ **Search:**\  This field allows a user to search for
shipping methods from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit a
shipping method record.\ ****

**Delete Action**

·         Click on the **Trash** button in order to delete the
record.\ ****

·         \ **Yes: **\ A click on Yes will delete the record.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the delete action.\ ****

**Add Action**\ 

•        Click on **+** to add the fields of the shipping method.

•        \ **Description:**\  The field displays the means of delivery.
i.e., if it regards a digital item then delivery would be via email etc.

•        \ **Amount:**\  This field displays the total cost to be
incurred for the delivery of an item.

**Reset Action**

•        A click on the **reset** button will take you to the last saved
position.

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the Pencil button
and Trash button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the description and
amount.\ ****

**Close button**

§  A click on the **cancel** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
