.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add or Edit actions**

.. raw:: html

   </div>

·         Click on the **+** button or the **Pencil** button to **add**
or **edit** a new webshop administration, in order to connect it to a
metadata model.\ ****

·         \ **Application Name:**\  The name of the application for
which the administration is created is required here.

·         \ **Receiver Order:**\  The e-mail address of the receiver of
the order is required in this field.

·         \ **Sender Order:**\  The e-mail address of the sender of the
reply to the order is required in this field.

·         \ **Subject Order:**\  The subject regarding the reply on the
order is required in this field.

·         \ **Receiver Application:**\  The e-mail address of the
receiver of the application for inspection is required in this field.

·         \ **Sender Application:**\  The e-mail address of the sender
of the reply to the application for inspection is required in this
field.

·         \ **Subject of the application:**\  Subject regarding the
reply on the  request for inspection is required in this field.

·         \ **To scanning on demand:**\  The e-mail address of the
person the request for scanning on demand is sent to is required in this
field.

·         \ **From scanning on demand:**\  The e-mail address of the
sender of the reply on the request for scanning on demand is required in
this field.

·         \ **Subject scanning on demand:**\  Subject of the e-mail of
the reply on the request for scanning on demand is required in this
field.

·         \ **Maximum Request:**\  Maximum number of items that can be
requested is required in this field.

·         \ **Maximum Order:**\  Maximum number of items to order is
required in this field.

 

**Reset Action**

·         Clicking on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

**Save Action**

·         The Save button saves any new or unsaved changes.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

****

 

.. raw:: html

   </div>

****

 

****

 

**Close button**

•        A click on the **cancel** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
