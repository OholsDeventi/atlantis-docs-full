.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Invoice Number**

.. raw:: html

   </div>

This screen displays the list of invoice numbers with the date the
invoicing has started with this number. Specific invoice numbers can
also be searched.

The administrator can either add (with the **+** button) new invoice
numbers, edit them (with the **Pencil** button) or delete them (with the
**Trash** button).

 

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

·         \ **Add Button:** Click on the **+** button to add an invoice
number record.

·         \ **Search:**\  This field allows a user to search for day
parts from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit an
invoice number record.\ ****

**Delete Action**

·         Click the **Trash** button in order to delete the record/row
of record.\ ****

·         \ **Yes: **\ Clicking on Yes will delete the record.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
delete action.\ ****

****

 

**Add Action**

•        Click on the **+** button to add the records.

•        \ **Start Date:**\  The date picker can be used to pick the
date when an invoice is sent or generated.

•        \ **First Invoice Number:**\  Provide the number of the first
invoice sent.

•        \ **Last Invoice Number:**\  Presents the last invoice number
used. This field is read-only.

**Reset Action**

•        A click on the **reset** button will take you to the last saved
position.

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the Pencil button
and Trash button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the date and the First
Invoice Number.\ ****

**Close button**

§  Clicking the **close** icon will show the close confirmation pop-up
window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
