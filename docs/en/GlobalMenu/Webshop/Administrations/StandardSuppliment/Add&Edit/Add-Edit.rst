.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

.. raw:: html

   </div>

•        \ **Description:**\  This field will allow the user to display
the supplement’s name and its description.

•        \ **Amount:**\  This field will display the amount of the
descripted item. The amount can also vary with the help of the increment
and decrement button.

•        \ **Is Digital:**\  The check mark on this field informs if the
supplement is to be applied on items which are marked as digital.

•        \ **Per item:**\  This field displays if the supplement is to
be applied per item in the shopping cart or for the whole order.

•        \ **Is Fixed Surcharged:**\  This field informs if the price is
fixed surcharged or not.

**Reset Action**

•        A click on the **reset** button will take you to the last saved
position.

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the Pencil button
and Trash button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the previously added
fields in standard supplements.\ ****

**Close button**

§  Clicking the **close** icon will show the close confirmation pop-up
window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

**Cancel: **\ Clicking the Cancel button will keep you on the same page.

.. raw:: html

   </div>

.. raw:: html

   </div>
