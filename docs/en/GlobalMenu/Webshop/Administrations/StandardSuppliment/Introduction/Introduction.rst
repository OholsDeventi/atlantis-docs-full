.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Standard Supplements**

.. raw:: html

   </div>

`This section provides amounts and descriptions of items which are added
to the shopping cart in various
situations. <https://bitbucket.org/rkdahiya/atlantis-help-manual/src/c8baccc755f234e98ee9e4a01a3350820441c9c4/Webshop/Standard-Suppliments.md?at=master&fileviewer=file-view-default>`__\  The
screen displays the list of standard supplements.

The administrator can either add new supplements (with the **+**
button), edit them (with the **pencil** button) or delete them (with the
**trash** button).

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

·         \ **Add Button:** Click on **+** button to add a standard
supplement record.\ ****

·         \ **Search:**\  This field allows a user to search for
standard supplements from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit a
standard supplement record.\ ****

·         \ **Delete: **\ Click on the **Trash** icon to delete the
supplement record.\ ****

§  \ **Yes: **\ A click on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

**Cancel: **\ A click on the Cancel button will terminate the delete
action.

.. raw:: html

   </div>

.. raw:: html

   </div>
