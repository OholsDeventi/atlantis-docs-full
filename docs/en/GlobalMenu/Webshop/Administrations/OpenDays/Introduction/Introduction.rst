.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Open Days**

.. raw:: html

   </div>

The screen will display the days of the following week. By marking the
checkbox  of the applicable daysthe administrator can define on which
days theorganization is open for requests for inspection.

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

****

 

•        Check the box for the days per week the reading room is open.

 

·         \ **Reset Action**

§  A click on the **reset** button will take you to the last saved
position.\ ****

·         \ **Save Action**

§  A click on the **save** button will save the changes made.\ ****

·         \ **Close button**

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
