.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Day Parts**

.. raw:: html

   </div>

After defining the start and end time this screen will display the day
parts  the organization is available. For example, when the organization
is available for half a day.

 

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

·         \ **Add Button:** Click on the **+** button to add a day parts
record.\ ****

·         \ **Search:**\  This field allows a user to search for day
parts from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit a
day parts record.\ ****

·         \ **Delete: **\ Click on the **Trash** icon to delete the day
part record.\ ****

§  \ **Yes: **\ A click on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

**Cancel: **\ A click on the Cancel button will terminate the delete
action.

.. raw:: html

   </div>

.. raw:: html

   </div>
