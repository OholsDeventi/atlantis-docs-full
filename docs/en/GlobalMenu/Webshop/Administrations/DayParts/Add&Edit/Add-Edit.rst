.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

.. raw:: html

   </div>

•        \ **Name:**\  Provide the name of the organization for which
the available time has been parted.

•        \ **Start Time:**\  Provide the start time of the opening of
the organization.

•        \ **End Time:**\  Provide the end time when the organization
will close.

•        \ **Whole day:**\  If there is any change in schedule, the
administrator may check the box which symbolizes that the organization
will open for the whole day.

**Reset Action**

·          A click on the **reset** button will take you to the last
saved position.\ ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

**Close button**

§  Clicking the **close** icon will show the close confirmation pop-up
window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
