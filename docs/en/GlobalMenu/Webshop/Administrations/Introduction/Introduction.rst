.. raw:: html

   <div class="WordSection1">

**Webshop**\ 

The webshop offers a number of functions to enable the user to order
reproductions, request documents for inspection and apply for scanning
on demand (if it’s available according to the license). By a shopping
cart these requests are collected and handled from there.

If financial transactions are involved, this can be handled manually or
by electronic payment. Also various options are available concerning
shipment, discounts, open days, holidays, etc.

The webshop consists within the **Global** **Menu** screen.

 

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

**Administrations**

The webshop administration connects a set of options for the webshop.
Different administrations can be created so different options can apply
to various metadata models or environments if the web shop is used by
third party applications. Which options are connected can be called upon
by hoovering an administration. This will show a menu with all options
available for an administration.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.

.. raw:: html

   </div>

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

 

·         \ **Add button: **\ Click on the **+** button to add webshop
administration records.

 

·         \ **Search:**\  This field allows a user to search web shop
administration connections from the list.\ ****

·         \ **Edit button: **\ Click on the **Pencil** button to edit
the webshop administration records.\ ****

·         \ **Delete: **\ Click on the **Trash** icon to delete the
administration record.\ ****

§  \ **Yes: **\ A click on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : A click on the Cancel button will terminate the
delete action.\ ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
