.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Holiday**

.. raw:: html

   </div>

`This screen, setup by the administrator, informs about the holidays of
the organization to notify the customer about the blocked
dates <https://bitbucket.org/rkdahiya/atlantis-help-manual/src/7fd44e91233ec0662550dae7c43a4b0d1e6aba9f/Webshop/Webshop-Administration-Holiday-Edit-Add.md?at=master&fileviewer=file-view-default>`__\ 
for requests for inspection.

The screen will display the list of holidays and the user can also
search for specific days. The administrator can either add (with the
**+** button) new dates or edit (with the **pencil** button) the
existing ones as blocked days. Also holidays can be deleted (with the
**trash** button).

 

·         \ `**Pin up button:
** <../../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

·         \ **Add Button:**\  Click on the **+** button to add a holiday
record.\ ****

·         \ **Search:**\  This field allows a user to search holidays
from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit a
holiday record.\ ****

**Delete Action**

·         Clicking on the **Trash** button in order to delete the record
or row of records.\ ****

·         \ **Yes: **\ Clicking on the Yes button will delete the
record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Cancel**\ : Clicking on the cancel button will terminate
the delete action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
