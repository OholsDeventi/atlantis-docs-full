.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

.. raw:: html

   </div>

•        \ **Description**\ : The name of the holiday is required in
this field.

•        \ **Date picker**\ : The date picker displays the date of the
holiday.

**Reset Action**

·         Clicking the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : Clicking the Yes button will take you back to the
last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
respective action. ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

·         Once the record is saved, it will display the Pencil button
and Trash button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the description and/or
the date picker record for a holiday.\ ****

**Close button**

§  Clicking the **cancel** icon will show the close confirmation pop-up
window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel: **\ Clicking the Cancel button will keep you on the same
page.\ ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
