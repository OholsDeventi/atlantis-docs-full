.. raw:: html

   <div class="WordSection1">

**Discounts**\ 

Discounts are for sale of either the reproduction of items or for
publication. The discounted amount can be a percentage or a fixed
amount, maybe depending on the number of items ordered.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Add button: **\ Click on **+** button to add discount
records.

.. raw:: html

   </div>

·         \ **Edit button: **\ Click on **Pencil** button to edit the
discount records.\ ****

·         \ **Delete button: **\ Click on the **Trash** icon to delete
the discount record.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on the Cancel button will terminate the
delete action.\ ****

.. raw:: html

   </div>

 

**Add or Edit Action**

·         Click on the **+** button or the **Pencil** button to add or
edit the records of discounts.\ ****

·         \ **Reproduction category:**\  In this field the user
[STRIKEOUT: ]\ needs to select the category of the item he wants to be
reproduced with the discount applying to it. The category can be
selected from a predefined drop down list of categories.

·         \ **From:**\  This field requires the start amount of items
for which the price is reduced.

·         \ **Until:**\  Insert the end amount of items for which the
price can be reduced.

·         \ **Price reduction: **\ Insert the total amount which will be
reduced as discount.

·         \ **Reduction Percentage:**\  Insert the total discount in
terms of a percentage.

**Reset Action**

•        A click on the **reset** button will take you to the last saved
position.

**Save Action**

·         This button saves any new or unsaved changes.

·         Once the record is saved, it will display the Pencil button
and the Trash button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the discount
fields.\ ****

**Delete Action**

•        Click on the **Trash** button in order to delete the
record.\ ****

•        \ **Yes: **\ Clicking on Yes will delete the record.\ ****

•        \ **Cancel**\ : Clicking on the Cancel button will terminate
the delete action.\ ****

 

 

****

 

 

.. raw:: html

   </div>
