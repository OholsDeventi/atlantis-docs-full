.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Cost Action**

.. raw:: html

   </div>

·         Click on the **cc** button to land upon the publication cost
page. The publication cost defines the cost incurred for the respective
publication category which is shown in a list in the pane.\ ****

·         \ **Pin up button: **\  Clicking the Pin button will pin the
page and it will show under the header of Atlantis Dashboard.\ ****

·         \ **Close button**

Clicking **Cancel** icon will show the close confirmation pop-up
window\ ****

§  \ **Yes: **\ A click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

·         \ **Search:**\  This field allows a user to search the
publication cost on the list.\ ****

**Add Action**

·         Click on the **+** button to add the records for the costing
of the publication category.

·         \ **Amount:**\  The cost of the publication depends on the
number of times the reproduction is published.

·         \ **Minimum Amount:**\  This field displays the minimum amount
of money for publishing.

·         \ **From:**\  The number of publications from which the cost
of item will start.

·         \ **Until:**\  The maximum number of publications for which
the amount counts.

·         \ **Block size:**\  The size of the file with its unit. Is
used for video reproductions.

·         \ **Each Unit:**\  The check box indicates that the amount is
per unit (block size) of the item.

**Reset Action**

•        A click on the **reset** button will take you to the last saved
position.

**Save Action**

·         A click on the **save** button will save the changes
made.\ ****

·         Once the record is saved, it will display the **Pencil**
button and **Trash** button.\ ****

**Edit Action**

•        Click on the **Pencil** button to edit the previously added
fields in publication costs.\ ****

**Delete Action**

•        Click on the **Trash** button in order to delete the
record.\ ****

•        \ **Yes: **\ A click on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

•        \ **Cancel**\ : A click on the Cancel button will terminate the
delete action.\ ****

.. raw:: html

   </div>

 

 

.. raw:: html

   </div>
