.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;background:white">

**Metadata button: **\ Clicking on the **M** button will allow the user
to assign the metadata model for the reproduction category. Select the
metadata model from the **drop down** list. The user can also remove the
assigned metadata by clicking on the **close** button on chip.

**Reset Action**

.. raw:: html

   </div>

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

**Save Action**

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

****

 

.. raw:: html

   </div>

****

 

****

 

**Close button**

•        Clicking on the **Close** icon will show the close confirmation
pop up window\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

§  \ **Cancel: **\ Clicking on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
