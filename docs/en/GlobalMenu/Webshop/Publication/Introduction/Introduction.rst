.. raw:: html

   <div class="WordSection1">

**Publication**

Reproductions can be ordered for publication. The publication categories
available are displayed in the left pane of the page as a list view. The
user can add, edit or delete the category by the **+**, **Pencil** or
**Trash** buttons respectively. The publication category is defined by
its name.

Next to this, publication costs can be assigned. For this, the user
clicks on the publication category to open the pane with the publication
costs. In this pane the user can add, edit or delete the costs by the
**+**, **Pencil** or **Trash** buttons respectively.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin up button: **\  As the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

.. raw:: html

   </div>

·         \ **Collapse Pane button:**\  Click on the **Left arrow**
button to collapse the pane. Once the pane  is collapsed, the same arrow
will turn towards right. Click on the **Right arrow** to expand the
pane.

·         \ **Add Button:** Click on the **+** button to add publication
records.

·         \ **Search:**\  This field allows a user to search the
publication category from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit
publication record.\ ****

·         \ **Cost Action: **\ Click on the **cc** button for the
costing of publication.\ ****

·         \ **Metadata button: **\ Click on the **M** icon to assign the
metadata model.\ ****

·         \ **Delete: **\ Click on the **Trash** icon to delete the
publication record.\ ****

§  \ **Yes: **\ A Click on the Yes button will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking the Cancel button will terminate the delete
action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
