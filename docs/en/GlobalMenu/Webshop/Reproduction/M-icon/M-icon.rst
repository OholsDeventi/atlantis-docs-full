.. raw:: html

   <div class="WordSection1">

**Metadata button: **\ Clicking on **M** button will allow user to
assign the metadata model for the reproduction category. Select the
metadata model from the **drop down** list. User can also remove the
assigned metadata by clicking on **close** button on chip.

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

**Save Action**

This button saves any new or unsaved changes.

 

.. raw:: html

   </div>
