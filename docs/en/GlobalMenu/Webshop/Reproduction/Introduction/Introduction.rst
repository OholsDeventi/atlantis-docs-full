.. raw:: html

   <div class="WordSection1">

**Reproduction**

This section provides the reproduction categories. There may be various
types of reproductions to be made on order. This screen will come up
with the available kinds of reproductions a user may add to his shopping
cart.

It categorizes the ways in which an item can be reproduced, for example:
direct download, the colored photograph of any item, scan copy of any
item etc. Categories are seen on the left side of the web shop
reproduction screen and the user can add, edit or delete the category by
the **+**, **Pencil** or **Trash** buttons respectively.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Collapse pane button:**\  Click on the **Left arrow**
button to collapse the pane. Once the pane is collapsed, the same arrow
will turn towards right. Click on the **Right arrow** to expand the
pane.

.. raw:: html

   </div>

·         \ **Pin-up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\ ****

·         \ **Add Button:** Click on the **+** button to add
reproduction records.

·         \ **Search:**\  This field allows a user to search the
reproduction category from the list.\ ****

·         \ **Edit Button:**\  Click on the **Pencil** button to edit a
reproduction record.\ ****

·         \ **Metadata button: **\ Click on the **M** icon to assign the
metadata model.\ ****

·         \ **Delete: **\ Click on the **Trash** icon to delete the
copyright holder record.\ ****

§  \ **Yes: **\ A click on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on the Cancel button will terminate the
delete action.\ ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
