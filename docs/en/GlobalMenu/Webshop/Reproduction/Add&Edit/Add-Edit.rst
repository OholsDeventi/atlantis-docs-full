.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Action**

.. raw:: html

   </div>

·         Click on **+** button to add the records for
reproduction.\ ****

·         \ **Definition:**\  This field requires the name of the
reproduction category.

·         \ **Video Format:**\  The desired video format for a copy of a
video can be selected here.

·         \ **Amount:**\  This field requires the price of the
reproduction. The user can also increase or decrease the amount with the
help of the stepper.

·         \ **Include Metamodel: **\ Check the box if a metadata model
is required for the respective category.

·         \ **Is Digital:**\  Check the box, if the entity is digital.

·         \ **Is Highres:**\  Check if it concerns a high resolution
reproduction or not.

·         \ **Direct Download:**\  Check the box, if the reproduction
needs to be available for download directly.

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

This button saves any new or unsaved changes.

.. raw:: html

   </div>

****

 

.. raw:: html

   </div>
