.. raw:: html

   <div class="WordSection1">

**Thesaurus Configuration**

The Thesaurus Configuration relates thesaurus nodes with fields in the
database. The top concept can be selected either from the thesaurus in
Atlantis or an external thesaurus. To link to database fields, its table
and column name are assigned and a user can also assign a metadata model
to it.

****

 

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin-up
button** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\ **:
**\  Like the name suggests, the Pin-Up button is used for pinning the
pages. The pinned pages are displayed under the pinned lists which are
placed in the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on the right arrow to
expand the pane.\ ****

·         \ **Search:**\  This field allows a user to search the
thesaurus configuration from the list.\ ****

****

 

·         \ **Add Button: **\ Click on **+ button** to add the thesaurus
configuration records.

 

·         \ **Delete Button: **\ The **trash** icon is delete button.
Click on trash icon will show the confirmation pop window.

§  \ **Yes**\ : Click on **Yes** button will perform the delete action.

§  \ **No**\ : Click on **NO** button will terminate the delete action.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Edit Button: **\ Click on **Pencil** icon to edit the
configuration records.

.. raw:: html

   </div>

 

.. raw:: html

   </div>
