.. raw:: html

   <div class="WordSection1">

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

•        Click on the add **+** button or **Pencil** button to add or
edit.

.. raw:: html

   </div>

•        \ **External: **\ Check the box if the Top concept should come
from an external source.

•        \ **Top Concept: **\ Select the top concept from the thesaurus
nodes.

•        \ **Column Name: **\ Select the column name from the database.

•        \ **Metadata Model: **\ Select the related metadata model for
the top concept.

•        \ **Table Name: **\ This field will automatically take the
table name once we select the related column.

•        \ **Ignore Verbatim: **\ Check the box if user want to ignore
verbatim values of the database field when searched. Then only the
related thesaurus term will be searched.

•        \ **May Add New: **\ The flag send out the request to backend
in order to add new/extra information if it is edited further.

****

 

**Reset Action**

·         A click on the **Reset** button will show the confirmation
pop-up window.\ ****

·         \ **Yes**\ : A click on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : A click on the Cancel button will terminate
the respective action. ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         A click on the **save** button will save the changes
made.\ ****

****

 

.. raw:: html

   </div>

****

 

****

 

****

 

**Close button**

•        Clicking on the **close** icon will show the close confirmation
pop-up window.\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

§  \ **Cancel: **\ Clicking on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
