.. raw:: html

   <div class="WordSection1">

 

**Merge nodes**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         In order to merge the nodes, the user needs to click the
**merge** button displayed on top.\ ****

.. raw:: html

   </div>

·         Clicking the Merge button will open the duplicate list of the
nodes on the right pane of the screen.\ ****

·         The **merge node form** is **** visible between both nodes
lists\ **.**

·         Subsequently the user can select the node from both lists so
that nodes can be merged in the form.\ ****

·         The data of the selected nodes are populated on the merged
node form.\ ****

·         \ **Add button: **\ Click the **+** button to show the drop
down buttons to choose the Semantic and the Relation. ****

·         \ **Semantic: **\ The user can select the semantic from the
drop down list to assign the nodes with a semantic relation or to merge
nodes.\ ****

·         \ **Relation: **\ The user can link multiple items listed by
using this function. Select the relation from the drop down list.\ ****

·         \ **Reset button: **\ Clicking the Reset button will take you
to the last user saved configuration settings. Please note this is not
to be used to reset to ‘Factory Default Settings’.\ ****

·         \ **Save button: **\ The Save button will not be clickable
until you select the semantic and relationship between 2 nodes. After
the selection click the **save** button in order to save changes.\ ****

·         \ **Edit button: **\ Once the changes are saved an **edit**
button (**pencil** button) will be displayed. ****

·         \ **Trash button: **\ Once the changes are saved a **trash**
button will be displayed. Clicking the Trash button will delete the
merged nodes.\ ****

·         \ **Merge: **\ Once the previous changes are made the
**merge** button will become enabled. Clicking the **merge** button will
merge the nodes.\ ****

·         \ **When 2 nodes of a thesaurus are merged, the relations for
them should be added by the user, otherwise it won't work.**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

****

 

 

.. raw:: html

   </div>
