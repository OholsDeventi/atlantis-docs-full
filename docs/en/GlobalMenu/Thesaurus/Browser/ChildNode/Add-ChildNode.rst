.. raw:: html

   <div class="WordSection1">

**Add child node:**\  After right clicking the mouse on a parent node in
the top menu, click on this button to add a child node. The action will
open a new window.\ ****

§  \ **Node type: **\ Select the node type from the drop down
list.\ ****

§  \ **Relation type:**\  Select the relation type from the drop down
list.\ ****

§  \ **Order number:**\  Insert the preference number in the text
field.\ ****

§  \ **Concept ID:**\  Insert the concept id of the node.

§  \ **Details:**\  Details of nodes appear if added. If not, it will
display “No Detail connected.

§  \ **Segments:**\  Select the segments listed in the dropdown list.
The segments will be selected as a chip which can be deleted by clicking
on the **Close** button on the chip itself.

§  \ **Another window can expand after adding a node.**\ 

§  \ **Add button:**\  Click the **+** button to add a relationship
between nodes.

§  \ **Semantic:**\  Select the segments from the dropdown list.

§  \ **Relation:**\  Select the relation from the dropdown list.

§  \ **Merge button:**\  Clicking the Function button will expand the
related nodes. Select the node from the list.

§  \ **Reset Action:**\  Clicking the **Reset** button will display a
confirmation pop-up window. The pop-up window contains the following
options:

**Yes**\ : Clicking the **Yes** button will take you back to the last
saved position.\ ****

**Cancel**\ : Clicking the **Cancel** button will terminate the current
action.

§  \ **Save Action: **\ This button saves any new or unsaved
changes.\ ****

 

.. raw:: html

   </div>
