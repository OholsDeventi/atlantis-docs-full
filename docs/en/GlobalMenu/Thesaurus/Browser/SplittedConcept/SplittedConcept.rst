.. raw:: html

   <div class="WordSection1">

**Split: **\ Clicking on split will open the splitted concept window.
\*All the fields mentioned under the edit merge section. ****

§  \ **Add button:**\  Click the **+** button to add a relationship to
the nodes.\ ****

§  \ **Semantic:**\  Select the segments from the dropdown list.\ ****

§  \ **Relation:**\  Select the relationship from the dropdown
list.\ ****

§  \ **Merge button:**\  Clicking the Function button will expand the
related nodes. Select the node from the list.\ ****

§  \ **Reset Action:**\  Clicking the **Reset** button will display a
confirmation pop-up window. The pop-up window contains the following
options:\ ****

o   \ **Yes**\ : Clicking the Yes button will take you back to the last
saved position.\ ****

o   \ **Cancel**\ : Clicking the Cancel button will terminate the
current action.

§  \ **Save Action: **\ This button saves any new or unsaved changes.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

 

.. raw:: html

   </div>

.. raw:: html

   </div>
