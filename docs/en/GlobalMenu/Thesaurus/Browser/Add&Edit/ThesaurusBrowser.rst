.. raw:: html

   <div class="WordSection1">

**Thesaurus Browser**

 

**Merge nodes**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         In order to merge the nodes, the user needs to click the
**merge** button displayed on top.\ ****

.. raw:: html

   </div>

·         Clicking the Merge button will open the duplicate list of the
nodes on the right pane of the screen.\ ****

·         The **merge node form** is **** visible between both nodes
lists\ **.**

·         Subsequently the user can select the node from both lists so
that nodes can be merged in the form.\ ****

·         The data of the selected nodes are populated on the merged
node form.\ ****

·         \ **Add button: **\ Click the **+** button to show the drop
down buttons to choose the Semantic and the Relation. ****

·         \ **Semantic: **\ The user can select the semantic from the
drop down list to assign the nodes with a semantic relation or to merge
nodes.\ ****

·         \ **Relation: **\ The user can link multiple items listed by
using this function. Select the relation from the drop down list.\ ****

·         \ **Reset button: **\ Clicking the Reset button will take you
to the last user saved configuration settings. Please note this is not
to be used to reset to ‘Factory Default Settings’.\ ****

·         \ **Save button: **\ The Save button will not be clickable
until you select the semantic and relationship between 2 nodes. After
the selection click the **save** button in order to save changes.\ ****

·         \ **Edit button: **\ Once the changes are saved an **edit**
button (**pencil** button) will be displayed. ****

·         \ **Trash button: **\ Once the changes are saved a **trash**
button will be displayed. Clicking the Trash button will delete the
merged nodes.\ ****

·         \ **Merge: **\ Once the previous changes are made the
**merge** button will become enabled. Clicking the **merge** button will
merge the nodes.\ ****

·         \ **When 2 nodes of a thesaurus are merged, the relations for
them should be added by the user, otherwise it won't work.**

****

 

 

 

**After merging the nodes a new form will be displayed.**

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Add button: **\ Click the **+** button to add a node
item.\ ****

.. raw:: html

   </div>

·         \ **Language: **\ Click on the dropdown list to set the
preferred language.\ ****

·         \ **Term: **\ Insert the term in the text field. This field is
mandatory.\ ****

·         \ **Preferred: **\ Flag indicates the preferred node
item.\ ****

·         \ **Default: **\ Flag indicates that the node item is selected
by default.\ ****

·         \ **Order: **\ Insert the order in the text field.\ ****

·         \ **Cancel: **\ Click the Cancel button to terminate the
action.\ ****

·         \ **Add button: **\ The button will only be clickable when all
the fields are filled. Clicking on this field will add a node
item.\ ****

·         \ **Edit button:**\  Once a node item has been added the row
will display a **pencil** button in order to edit the record. ****

§  \ **Save: **\ This button saves any new or unsaved changes.\ ****

·         \ **Delete: **\ Click the **Trash** icon to delete the node
item row.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the selected record.\ ****

§  \ **Cancel**\ : Clicking the Cancel button will terminate the delete
action.

                  After adding a node item, the user can see another
form below.

·         \ **Add button:**\  Click the **+** button to add further
details for the node the user is creating.

·         \ **Language:**\  Click the Drop down button to select the
preferred language.

·         \ **Note:**\  Insert the note in the text field.

·         \ **Save:**\  This button will save new or unsaved changes.

·         \ **Edit button:**\  Click the **Pencil** button to edit the
note.

·         \ **Delete button:**\  Click the **Trash** button to delete
the note.

 

 

·         \ **Node type:**\  Click the drop down list to select the
type. The options are:  Domain, Concept group or Concept.

·         \ **Oder Number:**\  Insert the order number of the node in
the text field.

·         \ **Concept ID:**\  Insert the concept id of the node.

·         \ **Details:**\  Details of nodes appear if added. If not, it
will display “No Detail connected”.

·         \ **Segments:**\  Select the segments from the dropdown list.
The segments will be selected as a chip, which can be deleted by
clicking the **Close** button on the chip itself.

o   \ **Chip: When the user clicks on the Segment Field, he sees a list
of pre-configured segments. After selecting any of the segments listed,
a ‘chip’ appears.**\ 

 

**Another window form can expand after adding a node.**\ 

·         \ **Add button:**\  Click the **+** button to add a
relationship between nodes.

·         \ **Semantic:**\  Select the segments from the dropdown list.

·         \ **Relation:**\  Select the relation from the dropdown list.

·         \ **Merge button:**\  Clicking the function button will expand
the related nodes. Select the node from the list.\ ****

**Reset Action**

Clicking on the **Reset** button will display a confirmation pop-up
window. The pop-up window contains the below options:

·         \ **Yes**\ : Clicking the **Yes** button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
current action.\ ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

 

 

All the nodes seen on the left pane also preserve individual functions.
Hover the mouse on any of the nodes and right-click.

**     Edit button: **\ Click the **pencil** button to edit the node
record. The fields are the same as mentioned above.

After merging the nodes a new form will be displayed.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Add button: **\ Click the **+** button to add a node
item.\ ****

.. raw:: html

   </div>

·         \ **Language: **\ Click the dropdown list to set the preferred
language.\ ****

·         \ **Term: **\ Insert the term in the text field. This field is
mandatory.\ ****

·         \ **Preferred: **\ Flag indicates the preferred node
item.\ ****

·         \ **Default: **\ Flag indicates that the node item is selected
by default.\ ****

·         \ **Order: **\ Insert the order in the text field.\ ****

·         \ **Cancel: **\ Click the Cancel button to terminate the
action.\ ****

·         \ **Add button: **\ The button will enable only when all the
fields are filled. Clicking on this field will add a node item.\ ****

·         \ **Edit button:**\  Once a node item is added the row will
display a **pencil** button in order to edit the record. ****

§  \ **Save: **\ This button saves any new or unsaved changes.\ ****

·         \ **Delete: **\ Click the **Trash** icon to delete the node
item row.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the selected record.\ ****

§  \ **Cancel**\ : Clicking the Cancel button will terminate the delete
action.\ ****

 

 

                  \ **After adding a node item, the user can see another
form below:**

·         \ **Add button:**\  Click the **+** button to add further
details for the node the user is creating.

·         \ **Language:**\  Click on drop down to select the preferred
language.

·         \ **Note:**\  Insert the note in the text field.

·         \ **Save:**\  This button will save new or unsaved changes.

·         \ **Edit button:**\  Click the Pencil button to edit the note.

·         \ **Delete button:**\  Click the Trash button to delete the
note.

 

 

·         \ **Node type:**\  Click the dropdown list to select the type.
The options are:  Domain, Concept group or Concept.

·         \ **Oder Number:**\  Insert the order number of the node in
the text field.

·         \ **Concept ID:**\  Insert the concept id of the node.

·         \ **Details:**\  Details of nodes appear if added. If not, it
will display “No Detail connected”).

·         \ **Segments:**\  Select the segments from the dropdown list.
The segments will be selected as a chip which can be deleted by clicking
the **Close** button on the chip itself.

o   \ **Chip: **\ When the user clicks the Segment Field, he sees a list
of pre-configured segments. After selecting any of the segments listed, 
a ‘chip’ appears.

 

**            Another window form can expand after adding a node.**\ 

·         \ **Add button:**\  Click the **+** button to add a
relationship between nodes.

·         \ **Semantic:**\  Select the segments from the dropdown list.

·         \ **Relation:**\  Select the relation from the dropdown list.

·         \ **Merge button:**\  Clicking the function button will expand
the related nodes. Select the node from the list.

****

 

**Reset Action**

Clicking the **Reset** button will display a confirmation pop-up window.
The pop-up window contains the following options:

·         \ **Yes**\ : Clicking the **Yes** button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
current action.\ ****

****

 

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

****

 

 

After merging the nodes a new form will be displayed.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.5in;margin-right:0in">

·         \ **Add button: **\ Click the **+** button to add a node
item.\ ****

.. raw:: html

   </div>

·         \ **Language: **\ Click the dropdown list to set the preferred
language.\ ****

·         \ **Term: **\ Insert the term in the text field. This field is
mandatory.\ ****

·         \ **Preferred: **\ Flag indicates the preferred node
item.\ ****

·         \ **Default: **\ Flag indicates that the node item is selected
by default.\ ****

·         \ **Order: **\ Insert the order in the text field.\ ****

·         \ **Cancel: **\ Click the Cancel button to terminate the
action.\ ****

·         \ **Add button: **\ The **+** button will enable only when all
the fields are filled. Clicking on this  button will add a node
item.\ ****

·         \ **Edit button:**\  Once a node item is added the row will
display a **Pencil** button in order to edit the record. ****

§  \ **Save: **\ This button saves any new or unsaved changes.\ ****

·         \ **Delete: **\ Click the **Trash** icon to delete the node
item row.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the selected record.\ ****

§  \ **Cancel**\ : Clicking  Cancel button will terminate the delete
action.\ ****

             After adding a node item, the user can see another form
below:

·         \ **Add button:**\  Click the **+** button to add further
details for the node the user is creating.

·         \ **Language:**\  Click on the dropdown list to select the
preferred language.

·         \ **Note:**\  Insert the note in a text field.

·         \ **Save:**\  This button will save new or unsaved changes.

·         \ **Edit button:**\  Click the **Pencil** button to edit the
note.

·         \ **Delete button:**\  Click the **Trash** button to delete
the note.

 

 

·         \ **Node type:**\  Click the dropdown list to select the type.
The options are: Domain, Concept group or Concept.

·         \ **Oder Number:**\  Insert the order number of the node in
the text field.

·         \ **Concept ID:**\  Insert the concept id of the node.

·         \ **Details:**\  Details of nodes appear if added. If not, it
will display”No Detail connected.

·         \ **Segments:**\  Select the segments from the dropdown list.
The segments will be selected as a chip which can be deleted by clicking
the **Close** button on the chip itself.

o   \ **Chip: **\ When the user clicks on Segment Field, he sees a list
of pre-configured segments. After he has selected any of the segments
listed, a ‘chip’appears.

 

**Another window form can expand after adding a node.**\ 

·         \ **Add button:**\  Click the **+** button to add a
relationship between nodes.

·         \ **Semantic:**\  Select the segments from the dropdown list.

·         \ **Relation:**\  Select the relation from the dropdown list.

·         \ **Merge button:**\  Clicking the Function button will expand
the related nodes. Select the node from the list.

**Reset Action**

Clicking the **Reset** button will display a confirmation pop-up window.
The pop-up window contains the below options:

·         \ **Yes**\ : Clicking the **Yes** button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking the Cancel button will terminate the
current action.\ ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

 

 

**Reorder alphabetically:**\  Clicking this button will rearrange the
nodes alphabetically.\ ****

****

 

 

****

 

.. raw:: html

   </div>
