.. raw:: html

   <div class="WordSection1">

**Thesaurus Browser**

On the ‘\ `Thesaurus
Browser <http://dev-deventit.staging.easternenterprise.com/#/thesaurus-browser>`__\ ’
page the user sees a list of ‘Domain’, ‘Concept Group’ and ‘Concept’. If
the user clicks on ‘+’, it expands to show a list or tree of concepts
and flags (items listed with a solid box icon). Any item with a ‘+’
sign, on clicking reveals sub-items linked to the parent item.

The left pane will show a list of all the nodes that are already present
in the system. The **legend** displayed on the top of the node list is
used to assign a particular **node** (any item listed with ‘+’ can be
understood as a parent node and any sub-items within it are child
nodes):

**Blue:**\  Concept group

**Red:**\  Domain

**Black:**\  Concept

**Tool Tip: **\ A user can hover the mouse over each button in order to
display additional details related to that button.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of the \ `**Atlantis
Dashboard** <http://dev-deventit.staging.easternenterprise.com/#/dashboard>`__\ .\ ****

.. raw:: html

   </div>

·         \ **Merge button:**\  Click the merge button (displayed in the
tree structure) to merge the nodes.\ ****

·         \ **Add root node button: **\ Click the **+** button to add
nodes. (Note: the same page can be available from the action/fields
mentioned below while merging a node.) ****

·         \ **Drop down:**\  Click the dropdown button to select the
preferred language.\ ****

·         \ **Search: **\ The user can search for a particular node item
or sub-item from the list.\ ****

·         \ **Delete: **\ Click the **Trash** icon to delete the
copyright holder record.\ ****

§  \ **Yes: **\ Click the Yes button to delete the record.\ ****

§  \ **Cancel**\ : Click the Cancel button to terminate the delete
action.\ ****

·         \ **Close button: **\ A click on the **cancel** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: A **\ click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ Click the Cancel button to remain on the same
page.\ ****

 

.. raw:: html

   </div>
