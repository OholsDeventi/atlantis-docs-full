.. raw:: html

.. raw:: html

   <div class="WordSection1">

\*\*Segments\*\* Like the name suggests, this section bifurcates the
Metadata model objects into different segments. It makes different
segments in the database. Segments are used for authorization by
limiting the user rights of the user groups to designated segments on a
per user basis. A drop-down list of segments is shown when a user
adds/edits metadata models in the Content Management System or when
editing a metadata model object. This section is a repository of
segments. The list of segments is displayed on the left side of the
screen. \*\*Tool Tip: \*\*\\ For the ease of understanding of the
clickables, the user can hover on each button. This will display the
name of the buttons.     .. raw:: html

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
      border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
      mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
      padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \\ \*\*Collapse pane button:\*\*\\ This button enables the
user to collapse the pane. To collapse the pane, click on the left arrow
button. Once the pane is collapsed, the same arrow button will change
its orientation i.e. it turns towards right. Click on right arrow to
expand the pane. .. raw:: html

.. raw:: html

   </div>

·         \\ \*\*Pin up button: \*\*\\ Like the name suggests, the Pin
Up button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.\\ \*\*\*\* ·         \\ \*\*Search:\*\*\\ This field allows a
user to search and select the segment from the list.\\ \*\*\*\*
·         \\ \*\*Add Button:\*\*\\ Click on \*\*+\*\* button to add
copyright holders’ details. \*\*\*\*   ·         \\ \*\*Edit
Button:\*\*\\ Click on \*\*Pencil\*\* button to edit segments record.\\
\*\*\*\* \*\*\*\*   ·         \\ \*\*Delete: \*\*\\ Click on
\*\*Trash\*\* icon to delete the segments record.\\ \*\*\*\* §  \\
\*\*Yes: \*\*\\ Click on yes will delete the record.\\ \*\*\*\* .. raw::
html

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
      padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \\ \*\*Cancel\*\*\\ : Click on cancel button will terminate the
delete action.\\ \*\*\*\* .. raw:: html

.. raw:: html

   </div>

  .. raw:: html

.. raw:: html

   </div>
