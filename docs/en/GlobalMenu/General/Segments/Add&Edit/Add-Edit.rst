.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add Button**

.. raw:: html

   </div>

·         Click on the plus (**+)** button to add and **pencil** button
to edit the records in the segments.\ ****

·         \ **Description:**\  Insert the name/description/definition of
the segment, as per convenience of user to add/edit.

 

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the below options:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

****

 

**Save Button**

This button saves any new or unsaved changes\ **         **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         Click on **save** button will save the changes made.\ ****

****

 

.. raw:: html

   </div>

****

 

****

 

****

 

**Close button**

•        Click on **cancel** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

 

.. raw:: html

   </div>
