.. raw:: html

   <div class="WordSection1">

**Media Types**

Like the name suggests, this module of the application informs the
administrator about the types of multimedia file formats like for
example MP3, MP4, DOCX, MOV etc. These media file types are present on
the left side of the screen. Administrator may either add a new type of
multimedia or can make changes in the existing one.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin up button: **\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists which are placed in the header of Atlantis
Dashboard.

.. raw:: html

   </div>

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

·         \ **Add button:**\  Click on **+** button to add media types
records.

 

·         \ **Search:**\  This field allows a user to search and select
the type of media from the given list.

 

 

·         \ **Edit Button:**\  Click on **Pencil** button to edit media
type’s records.\ ****

****

 

·         \ **Delete: **\ Click on **Trash** icon to delete the media
type’s record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

§  \ **Cancel**\ : Click on cancel button will terminate the
delete.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
