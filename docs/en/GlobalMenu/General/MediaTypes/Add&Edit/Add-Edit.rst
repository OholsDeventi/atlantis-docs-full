.. raw:: html

   <div class="WordSection1">

How can I add or edit media types?

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Clicking on the plus (**+)** button adds the media type
records.

.. raw:: html

   </div>

·         \ **Name:**\  This field consists of the multimedia file
extensions which describe their format.

·         \ **Icon:**\  A click on this field allows administrator to
browse media from the local machine. The icon is a symbolic
representation of the type of multimedia file which is chosen.

·         \ **Standard:**\  The checkbox represents the standard media
type. The default icon is applied in the absence of specific icon for
representation.

****

 

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the options below:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button                                **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

 

****

 

**Close button**

•        Click on **cancel** icon will show the close confirmation pop
up window\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

 

.. raw:: html

   </div>
