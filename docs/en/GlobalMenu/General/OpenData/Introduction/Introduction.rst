.. raw:: html

   <div class="WordSection1">

**Open Data**

This function is used to showcase the records in different formats so
that a third party can re-use them. Various sets of metadata model
objects can be created and displayed in the desired format, which can be
shared amongst the users. Data sets are configured with different
available formats by its extensions. `Clicking on any open data
set <>`__\ `[PvD1] <#_msocom_1>`__\  \  will display the various
extensions (which means various formats associated with the particular
open data set). User can also add a format. Once the user clicks on an
open data set, the right side of the screen will show the formats, which
are defined for the specific open data set.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin up button:**\  Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists, which are placed in the header of Atlantis
Dashboard.

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

·         \ **Add button:**\  Click on **+** button to add open data
records.

·         \ **Search:**\  This field allows a user to search and select
the open data record from the list.\ ****

·         \ **Edit button: **\ Click on **Pencil** button to edit the
open data record.\ ****

·         \ **Delete: **\ Click on **Trash** icon to delete the Open
data record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

§  \ **Cancel**\ : Click on cancel button will terminate the delete
action\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Set button: **\ Click on **S** icon to add set.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__We’d like this to work without hovering, by a
button “F” for formats. See Jira and change accordingly.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
