.. raw:: html

   <div class="WordSection1">

How can I add or edit open data set?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Click on the plus (**+)** button or Pencil button to add or
edit open data records.

.. raw:: html

   </div>

·         \ **Name:**\  Insert the format name of the file.

·         \ **Extension:**\  Insert the format extension name of the
file.

·         \ **Data set:**\  These are the metadata model objects which
user can select from the drop-down list in order to expose as open data.

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop up
window.

The pop-up window contains the below options:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

****

 

 

**     Delete Button**

·         Click on the **Trash** icon to delete the set data.\ ****

§  \ **Yes: **\ Clicking on yes will delete the record.\ ****

§  \ **Cancel**\ : Clicking on the cancel button will terminate the
delete action.\ ****

 

.. raw:: html

   </div>
