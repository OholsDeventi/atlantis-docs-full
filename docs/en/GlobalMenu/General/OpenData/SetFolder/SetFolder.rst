.. raw:: html

   <div class="WordSection1">

**Set (S-Icon)**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Set (S-Icon)**

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  This button enables the user to
**** collapse the pane.

·         \ **Search:**\  This field allows a user to search the set
from the list.\ ****

·         \ **Add button: **\ Click on **+** button to add set
data.\ ****

·         \ **Delete Button**

·         Click on the **Trash** icon to delete the set data.\ ****

§  \ **Yes: **\ Clicking on yes will delete the record.\ ****

§  \ **Cancel**\ : Clicking on the cancel button will terminate the
delete action.\ ****

·         \ **Edit button: **\ Click on **pencil** button to edit set
data.\ ****

****

 

·         \ `**XSLT: You can reach to XSLT editor from
here** <../XSLT/XSLT%20Editor.docx>`__\ ****

****

 

****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

**Add or Edit Action**

.. raw:: html

   </div>

·         Click on **+** button or Pencil button to add or edit
set.\ ****

·         \ **Name: **\ Insert the name of the set.\ ****

·         \ **Extension: **\ Insert the extension of the set.\ ****

·         \ **Close button: **\ Click on close button will close the add
action.\ ****

**         Reset Button**

             Clicking on the **Reset** button will display a
confirmation pop-up window.

             The pop-up window contains the options below:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action\ ****

**          Save Button**

·         This button saves any new or unsaved changes.\ ****

**     **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in">

****

 

.. raw:: html

   </div>

 

**     Delete Button**

·         Click on the **Trash** icon to delete the set data.\ ****

§  \ **Yes: **\ Clicking on yes will delete the record.\ ****

§  \ **Cancel**\ : Clicking on the cancel button will terminate the
delete action.\ ****

 

.. raw:: html

   </div>
