.. raw:: html

   <div class="WordSection1">

**XSLT - XSL** (Extensible Stylesheet Language) is a styling language
for XML. XSLT stands for XSL Transformations. XSLT is used to transform
XML documents into other formats like HTML.

By the XSLT editor the user can create XSL source codes in an easy and
effective way. They don’t have to deal with coding on any level.

The Users just have to use the information and options that are given to
create the structures they desire and the source code portions in the
XSLT editor will provide the actual XSL source code based on the
information provided in the rich text editor.

In order to enable the Rich Text Editor, the user will need to click on
the ‘\ **Enable Editor**\ ’ button. On clicking the button, the user
will be warned before using the Rich text editor as it enables them to
manipulate the source code.

 

Rich Text Editor has the following editing functionalities:

·         \ **Bold**\  

Renders the text with a \ **strong emphasis** which is usually heavier
than surrounding text. Keyboard shortcut is CTRL + B

·         \ **Italic**\  

*Italicizes*\  the selected text which usually appears slanted to the
right. Keyboard shortcut is CTRL + I

·         \ **Strikethrough**\  

[STRIKEOUT:Adds a line through]\  the middle of the selected text

·         \ **Underline**\  

Adds a line under the selected text. Keyboard shortcut is CTRL + U

·         \ **Lists - **\ The Insert/Remove Bulleted List button and the
Insert/Remove Numbered List button will format the text as a bullet list
or a numbered list.

·         \ **Block Quote –**\  This is a quotation in a written
document that is set off from the main text as a paragraph, or block of
text, and is typically distinguished visually using indentation and a
different typeface or smaller size font.

·         \ **Text Alignment - **\ This set of four buttons adjusts text
to be either aligned with the left margin, centered between margins,
aligned with the right margin,
or \ `justified <https://www.thoughtco.com/justification-alignment-in-typography-1078093>`__\ .

·         \ **Link – **\ This allows the user to add a hyperlink to the
XSLT editor area. It accepts a name of the Hyperlink. The user can
choose between provide URL, Link to another anchor or providing email
details. Each option will add a hyperlink with the name provided, which
can be accessed by double clicking on it.

·         \ **Unlink – **\ This allows the user to remove the hyperlink
and make it plain text again.\ ****

·         \ **Anchor – **\ This can be used to provide an anchor or a
checkpoint wherever necessary. The user can place it anywhere, by
choice,and has to provide a name to it in order to add the anchor. ** **

·         \ **Expand –**\  The Maximize button will expand the Rich Text
editor so that it will be presented in full width and height of the
browser.

**·         Paragraph Format – This allows the user to change the
selected text used in XSLT editor area to different available font
styles provided in the list.**

**·         Font Size – This will allow the user to change selected text
to a different font size.**

**·         Font Colour – Allows the user to change the font colour of
selected text into a different colour.**

**·         Background Colour – This allows the user to change the
background colour of selected text into a different colour.**

**·         Paste as Plain Text - This accepts text to be pasted to the
XSLT editor area. The text pasted in the area loses the formatting
applied to it initially and is pasted as a plain text.**

**·         Paste from Word – This accepts text to be pasted to the XSLT
editor area. The text pasted in the area keeps the formatting applied to
it initially. **

·         \ **Remove Formatting – This will clear the formatting applied
on the selected text.**\ 

**·         Insert Image – This allows the user to add images to the
XSLT editor area. The user can add an image using the URL of that image,
or provide alternated text for it in width, height, border, Hspace,
Vspace or alignment. In the preview box the user can check what the
image will look like in the XSLT editor area before adding it to the
XSLT editor.**

**·         Insert Table – This allows the user to add a table to the
XSLT editor area. The user can define the number of rows and the number
of columns needed and choose where to display the header, border size of
table, alignment of table, width, height, cell spacing and cell padding.
**

·         \ **Insert Special Character/Symbol – This allows the user to
add any special character or symbol to the XSLT editor area. The user
can choose from the options available. **\ 

**·         Text Indent - **\ These two buttons control the level of
indention of text, either right or left. Each click will shift text one
increment in the direction selected.\ ****

**·         Undo – This allows the user to undo changes one step at a
time.**

**·         Redo – **\ **This  allows**\ ** the user to redo changes
after the user has chosen undo. **

**·         Page Break – This will split the text into two pages
wherever applied. **

·         \ **Source –**\  This button will provide the HTML source code
of the formatting done in the Rich Text Editor area

 

**Insert** – The user can insert Single value, List or a Relation to the
XSLT editor area.

·         \ **Single Value** - If the user wants to insert a single
value to the XSLT editor, this option should be chosen.

o   \ **Literal** - By choosing this, the user can add a single text
value to the xslt editor. The user can type anything in the text box and
add a text according to his choice. This can be used to enter custom
text.

o   \ **Field** - This is the default chosen option when the user wants
to insert a single value in XSLT editor. By choosing this, the user can
add one table or a field from a specific table to the XSLT editor. The
user can either search for the table or field by typing it in the text
field or  can choose it from the available options present below the
text field. These options are shown in a hierarchal way where the table
name resides at the top node and the fields within a table can be
accessed by expanding these tables.

o   \ **Function** - By choosing this option, the user can choose and
enter a function from the available list. The functions list will depend
on the chosen metadata model. When the user selects a function from the
list, all the parameters available for the functions will be presented
to the user as a separate entity. The user can customize the input data
for the parameters as well. The options to choose from are *literal*,
*field* or *function* or enter data in parameters.

 

·         \ **List** – Through this list, the user can add a list to the
XSLT editor area. The user has to select a table from the available
tables in the drop down of the list. According to the selected table, a
list will be added to the XSLT editor area.

·         \ **Relation** – Relation here is sort of a list but is
defined on the basis of the relations of the metadata model for which
XSLT editor is opened with related metadata models.

**Condition** -

·         \ **If** - “\ **If**\ ” is used to provide a conditional
statement to the system which should follow if the reason or condition
provided is **true**. The user enters the value for the IF statement,
chooses a conditional operator and provides a testing or a checking
value which will be matched against the IF statement value.

·         \ **If Else** –  This will provide a separate else block also
when added to the XSLT editor, where the user can enter what should
happen in case the IF statement is **not true**.

·         \ **Else if** – the user can add an else-if clause to an if
statement when there is another condition the user wants to check.

**Static** - Static provides predefined templates which can be added to
the XSLT editor. While adding, the user can edit the predefined template
and then add it to the XSLT editor.

**Update** – The user can click on the update button to update the saved
XSLT code.

**Preview** – the user can click on the eye or preview icon to get a
glimpse of the generated XSLT code according to the changes made in the
XSLT editor area.

**Save** - Once the user clicks on the SAVE button only changes will be
made in the SOURCE CODE area according to the changes made in the XSLT
editor area.

**Reset** - This button will reset all the changes made.

**Source Code** - The user can switch to the SOURCE CODE tab to view the
complete source code generated by the changes made in the XSLT area

.. raw:: html

   </div>
