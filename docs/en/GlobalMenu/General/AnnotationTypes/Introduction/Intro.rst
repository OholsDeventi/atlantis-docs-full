.. raw:: html

   <div class="WordSection1">

***Annotation Types***

The Annotation type can be found under the **Global Menu** icon ****
(Top Right Corner\ **)** right below the **General** module\ **.** By
clicking on the Annotation type section, the user will be able to see
the list of predefined annotation types. Annotation types are cross
metadata models and can be selected to depict the type of annotation
provided through the
\ `annotation <Annotation%20Field.docx>`__\ ` <>`__\ 
fields\ `[PvD1] <#_msocom_1>`__\  \ .

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button** <../Pin%20Button.docx>`__\ **: **\  As
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists, which are placed in
the header of Atlantis Dashboard.\ ****

****

 

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

 

·         \ **Search:**\  This field allows a user to search and select
an annotation type from the list.

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Edit Button: **\ Click on **Pencil** button to edit the
records of annotation types.

.. raw:: html

   </div>

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__Should be a link to the help page describing
this

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
