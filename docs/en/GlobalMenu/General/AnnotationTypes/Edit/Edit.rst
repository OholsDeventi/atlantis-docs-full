.. raw:: html

   <div class="WordSection1">

***How can I edit Annotation types?***

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Edit Action**

.. raw:: html

   </div>

·         The user can click on the **Pencil** button to edit the
description of the annotation types.\ ****

·         \ **Description: **\ This **text** field describes the type of
annotation that user wants to select, It could be correction, addition
or information. This field is mandatory.\ ****

·         \ **Icon: **\ This is the pictorial representation of the
annotation type. By clicking on this field the user can browse and
upload any image as an icon from his local machine.\ ****

****

 

****

 

****

 

****

 

**Reset Action**

Clicking on the **Reset** button will display a confirmation pop up
window.

The pop-up window contains the below options:\ ****

·         \ **Yes**\ : Click on Yes button will take back to the last
saved position.\ ****

·         \ **Cancel**\ : Click on Cancel button will terminate the
respective action.\ ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

****

 

****

 

·         \ **Close button: **\ Clicking on the **Close** button will
display a pop-up window with below options\ ****

§  \ **Yes: **\ Click on yes button will close the page.\ ****

§  \ **Cancel: **\ Click on cancel button will remain on the same
page.\ ****

 

.. raw:: html

   </div>
