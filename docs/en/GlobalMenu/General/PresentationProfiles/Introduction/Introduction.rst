.. raw:: html

   <div class="WordSection1">

**Presentation Profiles**

****

 

The Presentation Profiles deal with the representation of various
multimedia files. It configures the attributes such as the thumbnail of
a document, pdf document, paintings and images the user requires (?).
Also presentation for the internet and the intranet with watermarks can
be set here. For downloading of multimedia files, this can be set for
various situations where downloads apply.

Presentation profiles are typically assigned to metadata models with
respect to the handling of multimedia files. It is found on the
**G**\ `**lobal** <Global%20Menu.docx>`__\  Menu under General settings.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Pin up button: **\ Like the name suggests, the Pin Up
button is used for pinning the pages. The pinned pages are displayed
under the pinned lists, which are placed in the header of Atlantis
Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Collapse Pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

 

·         \ **Add button:**\  Click on **+** button to add presentation
profiles records.

 

·         \ **Search:**\  This field allows a user to search the
presentation profiles from the list.\ ****

****

 

·         \ **Edit button: **\ Click on **Pencil** button to edit the
open data record.\ ****

****

 

·         \ **Delete: **\ Click on **Trash** icon to delete the
presentation profile record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Click on cancel button will terminate the delete
action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
