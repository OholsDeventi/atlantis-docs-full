.. raw:: html

   <div class="WordSection1">

How can I add or edit presentation profiles?

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Click on the plus (**+)** button to add or on the Pencil
button to edit the fields in the presentation profiles.\ ****

.. raw:: html

   </div>

-  **Name:**\  Name of the multimedia object is needed in this field.
   The field is mandatory.

-  **Default:**\  Flag that depict the presentation profile is to be
   used as the default presentation profile and no profile is assigned
   to a metadata model.

-  **Images – file type**\ : An extension like JPG, PNG, etc. in which
   the image will be presented on the Internet, Intranet or when
   presented an icon/thumbnail for the Image.

-  **Images – Quality**\ : The quality of the images (in % for JPEG, or
   a number for PNG) in terms of a quality/compression ratio or a
   compression factor. It depends on the chosen file type. The field is
   mandatory.

-  **Images – Width**\ : The maximum width in pixels for presentation.
   Image will be presented with respect to the aspect ratios. The field
   is mandatory.

-  **Images – Height**\ : The maximum height in pixels for presentation.
   Image will be presented with respect to the aspect ratios. The field
   is mandatory.

-  **Images – DPI**\ : The maximum number or dots per inch for
   presentation. Image will be presented with respect to the aspect
   ratios. The field is mandatory.

****

 

**Watermark:**

When showing images, a watermark can be placed.

-  **Image:**\  This field allows the user to browse an image from the
   local machine and set as watermark for the image.

-  **Placing Watermark:**\  This field specifies the position where a
   watermark is to be placed over the desired image. For example –
   Centre.

-  **Transparency of watermark:**\  Specify the level of watermark
   transparency to be shown over the image.

 

**Download resolutions:**

In different situations download of (a selected region of) images
applies. With download resolutions, different `download
resolutions <>`__\ `[PvD1] <#_msocom_1>`__\  \ can be assigned for
different situations.

****

 

****

 

****

 

**Add or Edit Action**

These checkboxes can be used in combination with each other, making the
download resolution applicable in different situations.

·         Click on **+** add button to add the fields of download
resolutions.\ ****

-  **Download Resolutions:**\  Users can select a specific download
   resolution from the drop down list.

-  **Internet:**\  Check this box if the download resolution applies to
   download of the image on the Internet.

-  **Intranet:**\  Check this box if the download resolution applies to
   download of the image on the Internet.

-  **Limited:**\  Check this box if the download resolution applies to
   download a limited presentation of the image.

-  **Unlimited:**\  Check this box if the download resolution applies to
   download an unlimited presentation of the image.

·         \ **Selection: **\ Check this box if the download resolution
applies to a selection of an image.

-  **Download:**\  Check this box if the download of an image is
   available for user.

 

 

-  **Reset Button**\ 

§  Clicking on the **reset** button will take you to the last saved
position.

 

·         \ **Save Button**\ 

§  Clicking on the **save** button will save the changes which are made.

§  Once the records are saved, it will display the pencil button and
trash button.

 

·         \ **Edit Button**\ 

§  Click on the **Pencil** button to edit the dates and both invoice
numbers.

 

·         \ **Delete Button**\ 

§  Click on the **Trash** button in order to delete the record/row of
record.

§  \ **Yes: **\ Clicking on yes will delete the record.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking the cancel button will terminate the delete
action.

.. raw:: html

   </div>

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__Should be a link to the help description of
Download resolutions.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
