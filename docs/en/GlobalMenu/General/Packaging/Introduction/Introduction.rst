.. raw:: html

   <div class="WordSection1">

**Packaging **

Like the name suggests, this section is used to configure all types of
packaging which are available for the physical items. This section is
needed when ordered objects   are tangible and are supposed to be packed
in containers like boxes. The user can provide dimensions to the
Containers.

To make it easier for the users, the left side of the screen will
display a list of preconfigured packaging types where the dimensions are
already defined. However, users may either add new packaging types or
can make changes to the existing ones.

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.\ ****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:** <Pin%20Button.docx>`__\ ** **\ As the
name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

****

 

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

 

·         \ **Add button: **\ Click on **+** button to add packaging
type’s records.

·         \ **Search:**\  This field allows a user to search and select
the type of packaging from the list.

 

·         \ **Edit button: **\ Click on **Pencil** button to edit the
packaging type’s records.

 

 

·         \ **Delete button: **\ Click on **Trash** icon to delete the
packaging record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Click on cancel button will terminate the
delete.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
