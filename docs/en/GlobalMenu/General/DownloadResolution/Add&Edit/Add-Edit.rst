.. raw:: html

   <div class="WordSection1">

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add – Edit action**

.. raw:: html

   </div>

·         \ **Description: **\ Insert the description of the resolution
in the text field.

·         \ **File type:**\  Insert the file extension in the text
field. The user needs to describe what kind of file it is.

·         \ **DPI: **\ Insert the **Dots per inch** required to showcase
the clarity of the item.\ ****

·         \ **Quality: **\ Insert the required quality of the item that
user needs to display.\ ****

·         \ **Horizontal resolution: **\ Insert the number of pixels
contained on a display monitor horizontally.\ ****

·         \ **Vertical resolution: **\ Insert the number of pixels
contained on a display monitor vertically.\ ****

****

 

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the following options:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button                                **

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

****

 

 

**Close button**

•        Clicking on the **close** icon will show the close confirmation
pop-up window\ ****

§  \ **Yes: **\ Clicking the Yes button will close the page.\ ****

§  \ **Cancel: **\ Clicking the Cancel button will terminate the current
action.\ ****

 

.. raw:: html

   </div>
