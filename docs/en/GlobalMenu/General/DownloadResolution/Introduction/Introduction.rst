.. raw:: html

   <div class="WordSection1">

**Download Resolution**

The Download Resolution can be found under the **Global Menu** icon ****
(Top Right Corner\ **)** right below the **General** module\ **.**  Like
the name suggests, the download resolution allows to set the display
resolution of the different items such as image, document etc. a user
can download. The resolution determines the quality of the item in which
it needs to be shown on screen.

**Tool Tip: **\ For the ease of understanding of clickables, users can
hover the mouse pointer on each button which will display the name of
the button.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up
button** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Pin%20Button.docx>`__\ **:
**\  Like the name suggests, the Pin Up button is used for pinning the
pages. The pinned pages are displayed under the pinned lists which are
placed in the header of the Atlantis Dashboard.\ ****

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  Click the **left arrow** button
to collapse the pane. Once the pane collapses the same arrow will turn
towards right. Click the **right arrow** to expand the pane.

·         \ **Add button:**\  Click the plus (**+)** button to add the
download resolution fields.

 

·         \ **Search:**\  This field allows a user to search and select
the resolutions from the list.

 

·         \ **Edit Button: **\ Click the **Pencil** button to edit the
download resolution fields.

 

·         \ **Delete: **\ Click on the **Trash** icon to delete the
download resolution fields.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on Cancel will terminate the delete
action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
