.. raw:: html

   <div class="WordSection1">

**Copyright holders**

 

A "copyright holder" is a person or a company who owns the exclusive
rights of a work. This screen provides the information about the
copyright holders. They decide the level of permission that is given on
their work as per the different copyright acts. Copyright holders is
seen in the \ `**Global Menu** <Global%20Menu.docx>`__\ ** **\ under the
**General section.**

****

 

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up button:
** <../../../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Content%20Managemnt%20System/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists which are placed in
the header of Atlantis Dashboard.\ ****

****

 

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  This button enables the user to
collapse the pane. To collapse the pane, click on the left arrow button.
Once the pane is collapsed, the same arrow button will change its
orientation i.e. it turns towards right. Click on right arrow to expand
the pane.

·         \ **Add Button:**\  Click on **+** button to add copyright
holders’ details.

****

 

·         \ **Search:**\  This field allows a user to search and select
the copyright holders from the list.\ ****

****

 

·         \ **Edit Button:**\  Click on **Pencil** button to edit
copyright holders’ details.\ ****

****

 

·         \ **Delete: **\ Click on **Trash** icon to delete the
copyright holder record.\ ****

§  \ **Yes: **\ Click on yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Click on cancel button will terminate the delete
action.\ ****

****

 

.. raw:: html

   </div>

 

.. raw:: html

   </div>
