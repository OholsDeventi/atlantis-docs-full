.. raw:: html

   <div class="WordSection1">

How can I Add/Edit copyright holder’s records?

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

Click on **+** button to add or the pencil button to edit copyright
holders’ details.\ ****

.. raw:: html

   </div>

 

-          Name:                                Name of the copyright
   holder

-          Address:                            Address of the copyright
   holder

-          Zip Code:                           Zip code of the copyright
   holder

-          State:                                 State of the copyright
   holder

-          City:                                   City of the copyright
   holder

-          Country:                        Country of the copyright
   holder

-          Telephone:                     Telephone of the copyright
   holder

-          Fax:                                    Fax of the copyright
   holder

-          Website:                           Website of the copyright
   holder

-          E-mail:                             E-mail of the copyright
   holder

-          Year of death:                  If the copyright holder is
   not alive, select the date of death

 

Between the copyright holder and the organization, agreements can be
registered for usage of the work of the copyright holder:

 

**Permission required for private use: **

·         If a reproduction is ordered for private use, permission
should be given by the copyright holder.

 

**Permission required for publication:**

·         If a reproduction is ordered for publication, permission
should be given by the copyright holder.

 

**Authorization to publish on the internet: **

·         The copyright owner has given permission to the organization
to publish his work on the Internet.

 

**Authorization to publish on third-party sites: **

·         The copyright owner has given permission to the organization
to pass his work to third parties for publication on the Internet.

 

**Authorization for third party reproduction: **

·         The copyright owner has given permission to the organization
to make reproductions of his work.

 

When the right to publish the work of the copyright holder is granted to
the organization, Creative commons rights can apply:

 

·         \ **BY:**\  All CC licenses require that others who use your
work in any way must give you credit the way you request, but not in a
way that suggests you endorse them or their use. If they want to use
your work without giving you credit or for endorsement purposes, they
must get your permission first.

 

·         \ **SA:**\  You let others copy, distribute, display, perform,
and modify your work, as long as they distribute any modified work on
the same terms. If they want to distribute modified works under other
terms, they must get your permission first.

 

·         \ **NC**\ : You let others copy, distribute, display, perform,
and (unless you have chosen No Derivatives) modify and use your work for
any purpose other than commercially, if they get your permission first.

 

·         \ **ND**\ : You let others copy, distribute, display and
perform only original copies of your work. If they want to modify your
work, they must get your permission first.

 

 

**Reset Action**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the options below:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

****

 

****

 

**Close button**

•        Click on the **cancel** icon will show a close confirmation pop
up window with the following options:\ ****

§  \ **Yes: **\ A click on the yes button will close the page.\ ****

§  \ **Cancel: **\ Click on cancel button and you will remain on the
same page.\ ****

 

.. raw:: html

   </div>
