.. raw:: html

   <div class="WordSection1">

**Translations**

“Translations” is the last option that can be found under the **Global
Menu** icon **** (Top Right Corner\ **)** right below the **General**
module\ **.** Like the name suggests, it is used to translate the keys
in English, Dutch, German and Flemish. The coding key is defined, which
is then translated to the above mentioned languages, which is to be
displayed on the tool tip.

**Key:**\  The developers have shared the coding keys of the various
functions of Atlantis. These keys are translated to the above mentioned
languages. Now the user can select any of those languages from the
**settings** page so that the function name and tool tip will be
presented in the selected language. 

**Tool Tip: **\ For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Search:**\  This field allows a user to search and select
the coding key from the list.

.. raw:: html

   </div>

·         \ **Add button:**\  Click on the plus (**+**) button to add
the coding keys in different languages.

·         \ **Edit button:**\  Click on the **Pencil** button to edit
the coding keys. Each language has its own edit (pencil) button so that
the user can also change the coding key specifically to any language.

·         \ **Delete button: **\ Click on the **Trash** icon to delete
the coding key.\ ****

§  \ **Yes: **\ Clicking the Yes button will delete the record.\ ****

§  \ **Cancel**\ : Clicking the Cancel button will terminate the current
action.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         \ **Pagination: **\ Pagination is done at the bottom of the
translation page. It allows the user to switch to other pages.

.. raw:: html

   </div>

****

 

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add – Edit Action**

.. raw:: html

   </div>

·         There are 5 text fields. **\*** All fields are
mandatory.\ ****

·         \ **Key:**\  Insert the coding key in text field.\ ****

·         \ **English:**\  Insert the coding key in the **English**
language.\ ****

·         \ **Dutch:**\  Insert the coding key in the **Dutch**
language.\ ****

·         \ **German: **\ Insert the coding key in the **German**
language.\ ****

·         \ **Vla**\ **:**\  Insert the coding key in the **Flemish**
language.\ ****

****

 

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window. The pop-up window contains the following options:

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes. \ ****

·         \ **\***\ For the edit action, only the Save button is
displayed.\ ****

****

 

.. raw:: html

   </div>

****

 

.. raw:: html

   </div>
