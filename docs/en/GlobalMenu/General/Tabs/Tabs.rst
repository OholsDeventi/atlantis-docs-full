.. raw:: html

   <div class="WordSection1">

**Tabs**

****

 

****

 

Tabs are used to organize and segregate the related metadata objects for
a designated metadata object. Tabs are defined and described by their
names. They represent the related metadata objects. If, for a specific
metadata object relationship, there are no tabs configured, the related
metadata objects are present in the tab “Relations”. A user can change
the tabs present for a metadata object relationship either by adding a
new tab and assigning it to the relationships between the metadata
objects or by making changes in the existing naming or assignment. To
access the Tabs, the user needs to click on the **G**\ `**lobal
Menu** <Global%20Menu.docx>`__\  under the General section. Once the
user clicks on tabs, the screen displays the preconfigured tabs.

**Tool Tip:**\  For the ease of understanding of the clickables, the
user can hover on each button. This will display the name of the
buttons.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

**Add or Edit Action**

.. raw:: html

   </div>

·         Click on the plus (+) button or Pencil button to add or edit
the records in the tabs.

·         \ **Description:**\  Defines the relation of the tab.

·         \ **Tab Sheet:**\  Insert the name of the tab in which user
wants to provide the source link.

·         \ **Checkbox**\ 

§  \ **Read Only**\ : Check this box if you want to give the ‘read only’
rights to the users when they see the relations of metadata models.

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the options below:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Button**

This button saves any new or unsaved changes.

**Delete Button**

•        Click on the **bin** icon to delete the tab records.\ ****

§  \ **Yes: **\ Clicking yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking the cancel button will terminate the delete
action.\ ****

.. raw:: html

   </div>

 

 

 

 

.. raw:: html

   </div>
