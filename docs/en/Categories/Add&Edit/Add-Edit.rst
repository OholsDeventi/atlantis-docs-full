.. raw:: html

   <div class="WordSection1">

**How can I add a category?**\ 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt">

By clicking the add button **(+),** the user can add a category.

.. raw:: html

   </div>

·         \ **Code: **\ Insert the code.

·         \ **Title:**\  Insert the title of the new node.

·         \ **Separation Mark: **\ Separates the code of one node from
the code of its parent.\ ****

·         \ **Earliest:**\  The user can choose the earliest possible
date.

·         \ **Latest:**\  The user can choose the latest possible
date.\ ****

·         \ **Period: **\ The assigned duration for the node.\ ****

·         \ **Does not count in coding**\ : The code of this node will
not be presented.\ ****

·         \ **Sort Number: **\ Sequencing of nodes with its
parents.\ ****

·         \ **Note: **\ Extra highlighted notes.\ ****

·         \ **Scope and content: **\ The description of scope and
content of nodes.\ ****

·         \ **Moderated: **\  Check the box if a category is moderated.
The date, time, status and user who moderated the category is
displayed.\ ****

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the following options:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

**Save Action**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

****

 

.. raw:: html

   </div>

****

 

**Close Icon**

·         \ **Close button: **\ Clicking on the **close** icon will show
the close confirmation pop-up window\ ****

§  \ **Yes: **\ Clicking on the Yes button will close the page.\ ****

§  \ **Cancel: **\ Clicking on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
