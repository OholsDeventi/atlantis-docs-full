.. raw:: html

   <div class="WordSection1">

**Click on any metadata model, this action will take the user to the
overview pane:**

****

 

·         \ **Green mark: **\ The green color right mark symbolizes
those nodes which are moderated.\ ****

·         \ **Grey mark: **\ The grey color right mark symbolizes those
nodes which are immoderate.\ ****

·         \ **Expand button +: **\ Clicking on the expand button,
expands the nodes and shows the total number of nodes. Also the user can
drag and drop the categories, behind or on top of the categories.\ ****

·         \ **Delete button: **\ Clicking on the **Trash** icon displays
the delete validation message in order to delete the category.

·         \ **Collapse pane button:**\  Click on the **left arrow**
button to collapse the pane. Once the pane collapses, the same arrow
will turn towards right. Click on the **right arrow** to expand the
pane.

·         \ **Search: **\ This field allows users to search the nodes of
selected node of model.\ ****

The user can drag the category/ies and drop them in front of, behind or
on top of another category. By placing it in front the user can change a
category into a sub category.

.. raw:: html

   </div>
