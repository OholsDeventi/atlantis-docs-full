.. raw:: html

   <div class="WordSection1">

**Categories**

The metadata objects are arranged in a hierarchy of categories per
metadata model. Categories contains the list of nodes which are shown in
a tree type structure. From this list the user can add and edit
categories and move the nodes of categories within the list. There are
various construction schemes which are available for the category,
being: naming a scheme, assigning to a metadata model, entering the
hierarchical nodes, numbering the categories accordingly by applying
code and separator properties. The user can sort the category for
ordering its siblings by providing a sort number. The user can right
click on the node in order to edit the node, add a new node, replace the
global selection with selected documents (categories), convert it to a
different type and add the selected documents (categories) to the
\ `**global
selection**\ **`[PvD1] <#_msocom_1>`__\  .** <Global%20Menu.docx>`__\ ` <>`__\ ****

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ **Collapse pane button:**\  Click the **left arrow** button
to collapse the pane. Once the pane collapses, the same arrow will turn
towards right. Click on the **right arrow** to expand the pane.

.. raw:: html

   </div>

·         \ `**Pin up button:
** <../../../../Atlantis/Again%20re-work/Helpfiles/Make%20changes/18-07-2017/Pin%20Button.docx>`__\  Like
the name suggests, the Pin Up button is used for pinning the pages. The
pinned pages are displayed under the pinned lists placed in the header
of Atlantis Dashboard.

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.25in;margin-right:0in">

·         \ **Search:**\  This field allows a user to search a desired
metadata model. Clicking on the metadata model will display the related
nodes.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>

.. raw:: html

   <div style="mso-element:comment-list">

--------------

.. raw:: html

   <div style="mso-element:comment">

.. raw:: html

   <div id="_com_1" class="msocomtxt" language="JavaScript"
   onmouseover="msoCommentShow('_anchor_1','_com_1')"
   onmouseout="msoCommentHide('_com_1')">

` <>`__
 `[PvD1] <#_msoanchor_1>`__This should be a link to the helpcontent for
the global selection.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>
