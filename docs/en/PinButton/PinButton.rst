.. raw:: html

   <div class="WordSection1">

**Pin Button**

The pin button can be found on the top of each page. The main purpose of
the pin button is to create a quick and easy access shortcut to the
pages the user wants to access frequently.

The links defined in this pinned list act as hyperlinks or as quick
access points to the screens the user has pinned. Pinned pages can be
removed from this list by navigating to the respective screens and
un-pinning them.

 

.. raw:: html

   </div>
