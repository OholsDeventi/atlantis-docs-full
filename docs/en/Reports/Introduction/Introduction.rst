.. raw:: html

   <div class="WordSection1">

**Reports**

The thumbnail of the reports can be seen on the **Atlantis**
**Dashboard**. The screen will display the list of all the available
reports. The user can select any report to perform various actions like
add (with the **+** button), delete (with the **trash** button) or edit
(with the **pencil** button). Report formats can be easily customized by
using the XSLT editor. The user can design its own set of fields and
contents. Reports should be associated with the respective metadata
models for which they are being created.

**Tool Tip: **\ For the ease of understanding of clickables the user can
hover the mouse pointer on each button which will display the name of
the button.

 

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         \ `**Pin up
button** <../../../../Atlantis/Again%20re-work/Helpfiles/Checked/Pin%20Button.docx>`__\ **:
**\ Like the name suggests, the Pin Up button is used for pinning the
pages. The pinned pages are displayed under the pinned lists which are
placed in the header of Atlantis Dashboard.\ ****

.. raw:: html

   </div>

·         \ **Collapse pane button:**\  Click on the **left arrow**
button to collapse the pane. Once the pane collapses, the same arrow
will turn towards right. Click on the **right arrow** to expand the
pane.\ ****

·         \ **Add button: **\ Click on the **+** button to add
reports.\ ****

·         \ **Search:**\  This field allows a user to search and select
a report from the list.\ ****

·         \ **Edit button: **\ Click on the **Pencil** button to edit a
report.\ ****

·         \ **XSLT button: **\ “We’re still working on XSLT”.\ ****

·         \ **Delete: **\ Clicking the **Trash** icon will delete the
reports.\ ****

§  \ **Yes: **\ Clicking on Yes will delete the record.\ ****

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;margin-left:.75in;margin-right:0in">

§  \ **Cancel**\ : Clicking on the Cancel button will terminate the
delete action.\ ****

.. raw:: html

   </div>

 

.. raw:: html

   </div>
