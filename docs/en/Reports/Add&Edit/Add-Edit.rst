.. raw:: html

   <div class="WordSection1">

**Add Button**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:solid windowtext 1.0pt;
   border-left:solid windowtext 1.0pt;border-bottom:none;border-right:none;
   mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
   padding:1.0pt 0in 0in 4.0pt;margin-left:.25in;margin-right:0in">

·         Click on the plus (+) buttons to add a report and pencil
button to edit report.

.. raw:: html

   </div>

·         \ **Description:**\  Insert the details of the reports, like
name, number(s), etc.

·         \ **Output Format:**\  The user can specify the extension of
the format in which the report is generated.

·         \ **Is Label:**\  Check this box if the report is used for
generating labels. The check mark on the box will enable more fields
such as:

§  \ **Page Format:**\  Insert the required page format, for example A4,
A6 etc.

§  \ **Label Width:**\  Insert the width of the label for the report.

§  \ **Height:**\  Insert the height of the report.

§  \ **Label Interspacing Horizontal:**\  Insert the spacing between two
labels horizontally.

§  \ **Labe Interspacing Vertical:**\  Insert the spacing between two
labels vertically.

§  \ **Left Margin:**\  Insert the margin the user needs to leave for
the left side of the report..

§  \ **Right Margin:**\  Insert the margin user needs to leave for the
right side of the report.

§  \ **Top Margin:**\  Insert the margin the user needs to leave at the
top of the report.

§  \ **Bottom Margin:**\  Insert the margin the user needs to leave at
the bottom of the report.

§  \ **Labels Per Row:**\  Insert the number of labels the user needs
per row.

§  \ **Rows Per Page:**\  Insert the number of rows the user needs on
each page.

 

·         \ **Detail Report:**\  Check this box if the report is for
reporting information of an object in detail.

·         \ **For Thesaurus:**\  Check this box if the report is used
for reporting on the thesaurus.\ ** **\ 

****

 

** Add Button**

·         Click on the **+** button to add a metadata model from the
drop down list, so that a report can be linked with respective
metadata.\ ****

·         \ **Report Standard: **\ Check the box if the user needs a
default report for the selected metadata model.\ ****

·         \ **Edit button:**\  Click on the **Pencil** button to change
the related metadata or uncheck the standard check box.\ ****

·         \ **Delete button: **\ Clicking on the **Trash** icon displays
the delete validation message in order to delete the category.

**                                   **

**Reset Button**

Clicking on the **Reset** button will display a confirmation pop-up
window.

The pop-up window contains the following options:\ ****

·         \ **Yes**\ : Clicking on the Yes button will take you back to
the last saved position.\ ****

·         \ **Cancel**\ : Clicking on the Cancel button will terminate
the current action.\ ****

****

 

**Save Button**

.. raw:: html

   <div
   style="mso-element:para-border-div;border-top:none;border-left:none;
   border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
   mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
   padding:0in 4.0pt 1.0pt 0in;background:white;margin-left:.25in;margin-right:
   0in">

·         This button saves any new or unsaved changes.\ ****

.. raw:: html

   </div>

 

**Close button**

•        Clicking on the **cancel** icon will show the close
confirmation pop up window\ ****

§  \ **Yes: A **\ click on the Yes button will close the page.\ ****

§  \ **Cancel: **\ A click on the Cancel button will keep you on the
same page.\ ****

 

.. raw:: html

   </div>
